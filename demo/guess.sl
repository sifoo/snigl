doc: _ "
A trivial number guessing game implemented using a recursive lambda and a switch.

$ snigl demo/guess.sl
Your guess: 50
Too high!
Your guess: 25
Correct!
"

use: math ('rand)

100 rand &{
  let: 'n
  
  switch: %("Your guess: " ask int) (
    (@n <) "Too low!"
    (@n >) "Too high!",
    drop "Correct!" say
  ), drop say @n recall
} call
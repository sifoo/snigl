> The majority believes that everything hard to comprehend must be very profound.<br/>
> This is incorrect. What is hard to understand is what is immature, unclear and often false.<br/>
> The highest wisdom is simple and passes through the brain directly into the heart.
>
> ~ Viktor Shauberger

### [sni`gel]
Snigl is a dynamic language aiming for functional simplicity and tight `C` integration.

```
  "abc" &(int ++ char) map str

["bcd"]
```

### Status
Snigl currently weighs in at 14 kloc including a 5 kloc standard library; and depends on `CMake`, a `C11` compiler and `pthreads`. It often runs several times faster than `Python3`, as far as the current [suite](https://gitlab.com/sifoo/snigl/tree/master/bench) of benchmarks go. The type hierarchy and standard library are works in progress, but embedding in and/or extending from `C` is trivial. Code from this document, [tests](https://gitlab.com/sifoo/snigl/blob/master/test/all.sl) and benchmarks should run clean in `Valgrind`.

### Bootstrap
Snigl may be compiled from source by issuing the following commands.

```
$ git clone https://gitlab.com/sifoo/snigl.git
$ cd snigl
$ mkdir build
$ cd build
$ cmake ..
$ sudo make install
$ snigl ../test/all.sl
$ rlwrap snigl

Snigl v0.9.12

Press Return in empty row to eval.

  1 2 +

[3]
```

### Evaluation
Expressions are evaluated left to right, pushing arguments on the stack and calling functions and macros in order of appearance.

```
  1 2 3 swap

[1 3 2]

  dup
  
[1 3 2 2]

  drop
  
[1 3 2]

  rotl
  
[2 1 3]

  rotr
  
[1 3 2]

  swap
  
[1 2 3]
```

### Expressions
Expressions consist of literals, including types; function and macros, which is anything that's not a type; and variables, like `@foo`. Sub expressions may be created by enclosing in `()` or prefixing with `,`.

```
  [3 times: 'a 'b] str

["aaab"]
```
```
  [3 times: ('a 'b)] str

["ababab"]
```
```
  [3 times:, 'a 'b] str

["ababab"]
```

### Equality & Identity
`=` may be used to check if two values are equal, while `==` makes sure they are the same value; the difference only applies to references. Both comparisons may be negated by prefixing with `!`.

```
  [1 2 3] [1 2 3] =

[t]
```
```
  [1 2 3] [1 2 3] !==

[t]
```
```
  42 42 ==

[t]
```

### Functions
Named functions support multiple dispatch with pattern matching.

```
  func: is-42? 42 Bool t
  func: is-42? Int Bool f
  1 is-42?
  42 is-42?

[f t]
```

Anonymous functions may be created by prefixing sexprs and scopes with `&`.

```
  &(42)

[(Lambda 1 0)]

  call
  
[42]
```

Scoped lambdas capture the defining environment.

```
  {42 let: 'foo &{@foo}}

[(Lambda 1 13)]

  call
  
[42]
```

Named functions may be referenced the same way.

```
  1 2 &+
  
[1 2 (Func +)]

  call

[3]
```

Function argument lists are fully evaluated at compile time.

```
  func: is-3? (1 2 +) Bool t
  func: is-3? (3 type) Bool f
  1 is-3?
  3 is-3?

[f t]
```

#### Recursion
Functions may be called tail-recursively using `recall` and `recall:`; the first variant jumps to the start of the current function, the second delegates to the specified function.

```
  func: fib-tail (0 Int Int) Int (
    rotr drop2
  )
  func: fib-tail (1 Int Int) Int (
    rotl drop2
  )
  func: fib-tail (Int Int Int) Int (
    rotr -- rotl dup rotl + recall: fib-tail
  )
  20 fib-tail

[6765]
```

#### Currying
Prebinding some or all arguments in a call may be accomplished using `curry:`.

```
  &+ curry: (35 7)

[((Func +) 35 7)]

  call

[42]
```

### Macros
Macros transform expressions into byte code at compile time.

```
  42 let: ('compiling say 'foo)

compiling
[]

  @foo

[42]
```

Macros are first class, and may be referenced like functions.

```
  &dup
  
[(Macro 1 0)]
```

Compile time arguments are captured at the reference site.

```
  &let: 'bar
  42 swap call
  @bar
  
[42]
```

### Variables
Names may be bound once per scope using `let:`.

Symbols are bound to values from the stack in LIFO order.

```
  1 2 3 let: ('a 'b 'c)
  @a @b @c

[3 2 1]
```

Runtime expressions may be injected using `%`.

```
  1 2 3 let: ('a %(40 +) 'b 'c)
  @a @b @c

[3 42 1]
```

Pairs may be used to specify values inline.

```
  2 let: ('foo :: 1 'bar 'baz :: 3)
  @foo @bar @baz

[1 2 3]
```

Using variables, the final case for `fib-tail` could be rewritten as follows.

```
func: fib-tail (Int Int Int) Int {
  let: ('b 'a 'n)
  @n -- @b dup @a + recall: fib-tail
}
```

### Numbers
Snigl supports signed 64-bit integers and decimal fixed point numbers. Fixed point literals use decimals to determine scale.

```
.25 .5 +

[.75]
```

```
  1.0 4.0 /
  
[.2]
```
```
  1.00 4.0 /
  
[.25]
```

### Characters
Character literals are prefixed with `#`.

```
  #a type

[Char]
```
While special characters use `\`.

```
  \n int

[10]
```

### References
Values may have their references captured using `&`. `get` returns the value, `set` updates it; while `get-set:` evaluates the expression with the value pushed and uses the result as new value.

```
  &41

[(Ref 41)]

  dup get-set: ++

[(Ref 42)]

  get

[42]
```

`ref` may be used instead of `&` to capture a reference after evaluation.

```
  &t

[(Macro 1 0)]
```
```
  t ref

[(Ref t)]  
```

### Strings
Strings are mutable, reference counted, fixed capacity sequences of characters.

```
  ["foo" 42 'bar] #, join

["foo,42,bar"]

  dup pop-drop dup last
  
["foo,42,ba" #a]

  drop len

[9]
```

Expressions may be injected using `%`.

```
  'bar
  42 let: 'baz
  "foo %_ %@baz"

["foo bar 42"]
```

###  Symbols
Symbols are immutable singleton strings with support for fast equality, they may be created by prefixing any identifier with `'`.

```
  'abc type

[Sym]
```

```
  'abc 'abc ==

[t]
```

### Sequences
Sequences are values that may be iterated; integers (0..N-1), pairs, strings and lists are all sequences.

#### Lists
Lists may be created by enclosing in brackets.

```
  [42 \n]
  
[[42 \n]]

  'foo push
  
[[42 \n 'foo]]

  pop
  
['foo']
```

Lists are reference counted; `dup` copies the reference, not the value.

```
  [1 2 3] dup pop-drop

[[1 2]]
```

A deep copy may be obtained by calling `clone`.

```
  [1 2 3] dup-clone dup pop-drop

[[1 2 3] [1 2]]
```

The `list` constructor allows creating a new list from any sequence.

```
  [1 2 3] dup list pop-drop

[[1 2 3]]
```

```
  7 list

[[0 1 2 3 4 5 6]]
```

Lists support deep comparisons.

```
  [1 2 2] [1 2 3] <

[t]
```

Expressions from the containing stack may be injected using `%`.

```
  41 ['foo %(++) 'bar]

[['foo 42 'bar]]
```
```
  42 ['foo [%_ 'bar %%_].. 'baz]
  
[['foo 'bar 42 'baz]]
```

`[..]` may be used to move all values on the stack to a new list.

```
  'foo 'bar 'baz [..]

[['foo 'bar 'baz]]
```

#### List Sets
`ListSet`s share part of their representation with `List`s and may be treated as either type without conversions. Note that modifying `ListSet`s as `List`s comes with the risk of messing up ordering, which may cause all sorts of heisenbugs when treated as a `ListSet` again.

```
  nil new-list-set let: 'cs
  @cs #b _add
  @cs #a _add
  @cs #c _add
  @cs
  
[[#a #b #c]]

  @cs dup pop

[[#a #b] #c]
```

The first argument to `new-list-set` is an optional function for converting values to keys; which allows using `ListSet`s as maps, indexing on `struct` fields etc. The following example uses pairs to represent map entries.

```
  &first new-list-set let: 'ps
  @ps 2 :: 'b _add
  @ps 1 :: 'a _add
  @ps 3 :: 'c _add
  @ps

[[1::'a 2::'b 3::'c]]

  2 find

[2::'b]
```

`find-iter` returns a `ListIter` to the specified key and a `Bool` indicating if it was found or not.

```
  nil new-list-set let: 'ns
  @ns 1 _add
  @ns 3 _add
  @ns

[[1 3]]

  @ns 2 find-iter

[(ListIter 0xaea968) f]

  drop 2 insert
  @ns

[[1 2 3]]
```

#### Iterators
`iter` may be called on any sequence to get a new iterator. `next` pushes the next value, or nil; while `_next` discards it and `skip` discards the specified number of values.

```
  3 iter

[(IntIter 0x3100c38)]

  dup next

[(IntIter 0x3100c38) 0]

  swap dup _next dup next swap

[0 2 (IntIter 0x3100c38)]

  next

[0 2 nil]
```
```
  "foobar" iter dup 3 skip
  
[(StrIter 0x1666318)]

  next
  
['b]
```

List iterators additionally support inserting and deleting values at the current position.

```
  [1 2 3] dup iter

[[1 2 3] (ListIter 0x2100b38)]

  dup _next dup _next dup _delete

[[1 3] (ListIter 0x2100b38)]

  42 insert

[1 42 3]
```

Iterators may be further processed using `map` and `filter`, both return new iterators.

```
  "abcabcabc" &(int ++ char) map str

["bcdbcdbcd"]
```
```
  "abcabcabc" &('b !=) filter str

["acacac"]
```

`map` skips `nil` results, which allows filtering while mapping. With that in mind, previous examples could be combined as follows:

```
  "abcabcabc" &(int ++ char dup #b = if: (drop nil)) map str

["cdcdcd"]
```

Since list literals are fully evaluated, putting a `for:`-loop with the condition inside accomplishes the mostly same thing slightly faster, in return for being less composable and requiring additional memory to store the list.

```
  ["abcabcabc" for:, int ++ char dup #b = if: drop] str
````

`for:` may be used to consume, or reduce, any sequence.

```
  7 list

[[0 1 2 3 4 5 6]]

  0 swap for: +

[21]
```

### Pairs
Pairs may be formed using `::`; queried using `first` and `last`; and split up using `..`.

```
  'foo :: 42

['foo::42]

  dup last 42 =

['foo::42 t]

  drop ..

['foo 42]
```

Pairs support deep comparisons.

```
  42 :: 'a 42 :: 'b <

[t]
```

### Loops
Besides recursion and iterators; `times:` may be used to iterate an expression a fixed number of, well, times.

```
  0 3 times: ++

[3]
```

And `while:` to repeat an expression until it returns false.

```
  0 while: (++ dup 42 <)

[42]
```

### Types
Typenames start with uppercase letters. All types, including `Nil`; are derived from the root type, `A`. All types except `Nil` are additionally derived from `T`.

```
  42 T type-sub?

[t]
```
```
  42 Nil type-sub?
  
[f]
```
```
  nil Nil type-sub?  
[t]
```

Types may be combined using `|`.

```
  42 Int|Char type-sub?

[t]
```
```
  Int|Fix Num sub?

[t]
````

`type:` creates a new type derived from specified target. Both expressions, parent types and implementation are fully evaluated at compile time.

```
  type: IntList _ List
  func: sum IntList Int (
    0 swap for: +
  )
  [1 2 3] sum

Error in row 1, col 8: Fimp not applicable: sum (IntList)

  [1 2 3] int-list sum

[6]
```

#### Traits
Traits are types without implementations, `Num` and `Seq` are two built-in examples. New traits may be created using `trait:`, parent types are evaulated at compile time.

```
  trait: Foo _
  trait: Bar Foo
  trait: Baz _ 
  type: FooInt (Bar Baz) Int
  42 foo-int Foo type-sub?

[t]
```

#### Structs
Structs contain a fixed number of named with optional types and default values. Any number of parent types may be specified as long as field names are unique.

```
  struct: Peer _ ('name Str :: "n/a")
  struct: Message _ ('from Peer 'body Str)
  struct: EMail Message ('subject Str)
```

`new` may be used to create new instances. `put:` works much like `let:`, but additionally expects a receiver on top of the stack.

```
  Peer new let: 'p
  @p
  
[(Peer "n/a")]
```
```
  @p dup-put: ('name :: "Me")

[(Peer "Me")]
```

Fields may be referenced using symbols, which are used to look up fields; or using optionally typed field specifiers that know enough to index the instance.

```
  @p .name
  
["Me"]
```
```
  @p .Peer/name
  
["Me"]
```

Uninitialized fields return `nil`.

```
  EMail new let: 'm
  m .from
  
[nil]
```

`put:` accepts any kind of field reference.

```
  "Testing 123" "Test" @p
  @m dup-put: ('.EMail/from '.subject 'body)

[(EMail (Peer "Me") "Test" "Testing 123")]
```

Instances default to comparing fields.

```
  Peer new dup-put: ('.name :: "foo") let: 'p1
  Peer new dup-put: ('.name :: "foo") let: 'p2  
  @p1 @p2 = say

[t]
```
```
  @p2 put: ('.name :: "bar")
  @p1 @p2 = say
  
[f]
```
The behavior may be customized by overriding `=`.
```
  func: = (Peer Peer) Int 42
  @p1 @p2 = say
  
[42]
```

Structs may be turned into sequences by deriving `Seq` and implementing `iter`

```
struct: Foobar Seq _
func: iter Foobar Iter (
  drop "foobar" iter
)
Foobar new #, join

["f,o,o,b,a,r"]
```

#### Enums
Enums represent fixed, ordered sets of values. Any number of parent traits may be specified.

```
  enum: PropType _ ('border-color 'border-width)
  border-color

[PropType/border-color]

  PropType type-sub?

[t]
```

Values respect definition order.

```
  border-color >= border-width

[f]
```

Conversions to `Int` and `Sym` are provided.

```
  border-color sym
  border-width int

[border-color 1]
```

Values may be iterated.

```
  PropType for: _
[PropType/border-color PropType/border-width]
```

Pattern matching may be used to piggyback additional behavior. The following example adds support for emitting `CSS` based on property type, `RGB` is a built-in type.

testprops.sl
```
struct: Prop _ (
  'type PropType
  'val  T
)

func: new-prop(PropType T) Prop (
  Prop new dup-put: ('type 'val)
)

func: prop-val-css(border-color RGB) Str {
  let: 'c
  "border-color: 'rgb(%([@c..] ", " join))';" swap drop
}

func: prop-val-css(border-width Int) Str (
  "border-width: %();" swap drop
)

func: prop-css(Prop) Str (
  dup .Prop/type swap .Prop/val prop-val-css
)

border-color 255 0 0 new-rgb new-prop prop-css say
```
```
$ snigl testprops.sl
border-color: rgb(255, 0, 0);
```

#### Conversions
Where applicable, values may be converted by calling a constructor named after the target type.

```
  "foo" list
  
[['f 'o 'o]]

  "bar" push

[['f 'o 'o "bar"]]

  str
["foobar"]
```

### Libraries
Most of Snigl's features are regular definitions that may or may not be imported into the current namespace. The default library is called `home`; and the usual suspects may be found in `abc`, which is imported into `home` by default. Definitions from external libraries may be accessed by prefixing their names with library name followed by `/`.

```
  35 7 abc/+

[42]
```

New libraries may be defined, and existing libraries extended; using `lib:`.

testlib.sl
```
lib: math (
  use: abc _
  
  func: fib Int Int (
    0 1 rotr --
    times: (dup rotl +)
    swap drop
  )
)

20 math/fib say
```
```
$ snigl testlib.sl
6765
```

### Serialization
Most values support serialization to a portable, compact, binary archive format.

```
  new-starch 42 store _load

[42]
```

References are de-duplicated and restored on load.

```
  "foo" let: 'foo
  new-starch
  @foo store @foo store
  load swap _load ==

[t]
```

Archives are sequences and may be iterated to load values.

```
  new-starch
  1 store [2 "foo"] store 3 store "bar" store
  for: _

[1 [2 "foo"] 3 "bar"]
```

Archives share part of their representation with byte buffers and may be treated as such without conversions. The following example stores three integers and pushes the resulting archive size in bytes.

```
  new-starch 1 2 3 store len

[5]
```

### Tasks
New tasks may be started using `task:`, while calling `yield` transfers control to the next. `idle:` may be used to finish all tasks, the expression is run before each round.

The following example runs two cooperative tasks pushing results to a shared list and waits for them to finish.

```
[] let: 'out
task: (2 times:, @out 'bar _push yield)
task: (2 times:, @out 'baz _push yield)
idle: (@out 'foo _push)
@out
  
[['foo 'bar 'baz 'foo 'bar 'baz 'foo 'foo]]
```

### IO
When multitasking, any operation that might block is deferred to a background thread by default while the current task yields until a result is available.

In tight loops like the following example, it still makes sense to ```yield``` manually for optimal concurrency. As of this writing; asynchronous file IO is around 5 times slower, which is still slightly faster than blocking file IO in Python3.

The following example reads the same file line by line in three concurrent tasks.

test.txt
```
foo
bar
baz
```
iotest.sl
```
  3 for: {
    let: 'n

    task: {
      "test.txt" r fopen
      [@n " fopen"] say
    
      lines for: {
        [@n " read"] say
        drop yield
      }
    }
  }

  idle: ('idle say)
```

The output will look slightly different each run because of the indeterminism added by background threads.

```
$ snigl iotest.sl
idle
1 fopen
0 fopen
2 fopen
idle
2 read
0 read
1 read
idle
1 read
idle
1 read
2 read
0 read
idle
0 read
2 read
idle
```

`sync:` may be used to force synchronous IO in an expression. Synchronizing the entire body of the previous example results in a deterministic sequence of operations.

iotest.sl
```
  3 for: {
    let: 'n

    task: { sync:,
      "test.txt" r fopen
      [@n " fopen"] say
    
      lines for: {
        [@n " read"] say
        drop yield
      }
    }
  }

  idle: ('idle say)
```
```
$ snigl iotest.sl
idle
0 fopen
0 read
1 fopen
1 read
2 fopen
2 read
idle
0 read
1 read
2 read
idle
0 read
1 read
2 read
idle
```

#### Program Arguments
`argv` returns the list of arguments passed to `snigl` when starting the script.

test.sl
```
argv for: say
```
```
$ snigl test.sl foo bar baz
foo
bar
baz
```

#### Output
`say` pretty-prints the specified value followed by newline to `stdout`; while `dump` prints the raw value without formatting. Lists may be used to output multiple values.

```
  [42 \n foo] say

42
foo
[]
```
```
  [42 \n foo] dump

[42 \n 'foo]
[]
```

Snigl buffers output per task, the buffer may be flushed using `flush-stdout` and its contents retrieved using `stdout-str`. The REPL flushes the buffer automatically each time an expression is evaluated.

```
  'foo say
  flush-stdout 'bar say stdout-str
  'baz say

foo
baz
["bar\n"]
```

### Precompilation
`pre:` may be used to evaluate code at compile time, any results are pushed each time the code is evaluated.

```
  &((pre:, 'compiling say 1 2 +) 'running say)

compiling
[(Lambda 1 0)]

  call

running
[3]
```

### Documentation
Documentation may be appended to definitions using `doc:`. The expression may contain arbitrary code, and the contents of the stack are converted to string and concatenated after evaluation. `source:` evaluates the expression and returns result followed by source code as string.

```
  func: my-add(42 Int) Int drop
  func: my-add(Int 42) Int (swap drop)
  func: my-add(Int Int) Int +

  doc: my-add (
    "Adds arguments and returns the result; except when any argument is 42,"
    "in which case it returns 42.\n\n"
    
    "Examples:\n"
    "  " source: (3 4 my-add)\n rotr
    "[%_]\n\n"
    "  " source: (42 7 my-add)\n rotr
    "[%_]"
  )
```

`docs` returns documentation for a definition as string.

```
  'my-add docs say
  
Adds arguments and returns the result; except when any argument is 42, in which case it returns 42.

Examples:
  3 4 my-add
[7]

  42 7 my-add
[42]
```

#### Comments
Expressions may be exempted from compilation and evaluation using `_:`, the code is parsed as usual and required to be syntactically correct but has no effect on the system.

```
  'foo _: ("Launching missiles" say) 'bar

['foo 'bar]
```

### Failures
When operations fail, it's often useful to unwind to a point where the failure can be dealt with properly. In those situations; `throw` may be used to unwind to the nearest `try:`, and `catch` to retrieve the thrown value. 

`try:` returns nil on success.

```
  try: _

[nil]
```

And an error containing the value otherwise.

```
  try: (42 throw)

[(Error 42)]

  catch

[42]
```

#### Resources
Code may be scheduled to run on scope exit in reverse declared order using `defer:`. The same mechanism may be used to run code on program exit by deferring from main scope.

```
  {
    'before say
    defer: ('first say) 
    defer: ('second say)
    'after say
  }

before
after
second
first
[]
```

### Plugins
`link:` may be used to pull shared libraries into the runtime. A plugin is simply a shared library containing a function with the signature `bool X_init(struct sgl *, struct sgl_pos *)`, where `X` is the plugin name (`snigl_sqlite` in the example below).

```
  link: "/usr/local/lib/libsnigl_sqlite.so"
  use: sqlite ('open-db)
  "test.dat" open-db

[(DB 0x1684798)]
```

### Tracing
Snigl traces code before running to replace as many runtime checks as possible with static guarantees. Operations that are no longer needed are replaced with `nop`.

bench/struct.sl
```
struct: Foo _ ('bar 'baz 'qux Int)

10 bench: (100000 times:,
  40 Foo new dup-put: '.Foo/bar
  dup .Foo/bar ++ swap dup-put: '.Foo/baz
  dup .Foo/baz ++ swap put: '.Foo/qux
) say
```
```
$ ./snigl compile bench/struct.sl
    0 nop             next: 9
    1 nop             next: 9
    2 nop             next: 9
    3 nop             next: 9
    4 nop             next: 9
    5 nop             next: 9
    6 nop             next: 9
    7 nop             next: 9
    8 nop             next: 9
    9 push            10 next: 10
   10 bench           next: 12
   11 nop             next: 12
   12 times           100000 next: 14
   13 nop             next: 14
   14 new             Foo next: 20
   15 nop             next: 20
   16 nop             next: 20
   17 nop             next: 20
   18 nop             next: 20
   19 nop             next: 20
   20 dup             next: 21
   21 put-field       Foo.bar 40 next: 22
   22 dup             next: 23
   23 get-field       Foo.bar next: 24
   24 fimp-call       ++(Int) next: 25
   25 swap            next: 30
   26 nop             next: 30
   27 nop             next: 30
   28 nop             next: 30
   29 nop             next: 30
   30 dupl            next: 31
   31 put-field       Foo.baz next: 32
   32 dup             next: 33
   33 get-field       Foo.baz next: 34
   34 fimp-call       ++(Int) next: 35
   35 swap            next: 39
   36 nop             next: 39
   37 nop             next: 39
   38 nop             next: 39
   39 put-field       Foo.qux next: 40
   40 count           next: 41
   41 count           next: 42
   42 dispatch        say(A) next: 43
   43 nop             next: 43

nop               24  35%
dup                3   6%
put-field          3   6%
count              2   4%
fimp-call          2   4%
get-field          2   4%
swap               2   4%
bench              1   2%
dispatch           1   2%
dupl               1   2%
new                1   2%
push               1   2%
times              1   2%

Traced 3 rounds in 0ms
```
```
$ snigl bench/struct.sl
312
```

Unless otherwise instructed, Snigl keeps tracing until it can't find anything to improve. The number of rounds may be limited by passing `--trace N` on the command line. Without tracing, this benchmark takes 22% longer to finish.

```
$ ./snigl compile --trace 0 bench/struct.sl
    0 nop             next: 1
    1 eval-beg        end: 2 next: 2
    2 eval-end        next: 3
    3 eval-beg        end: 8 next: 4
    4 push            'bar next: 5
    5 push            'baz next: 6
    6 push            'qux next: 7
    7 push            Int next: 8
    8 eval-end        next: 9
    9 push            10 next: 10
   10 bench           next: 11
   11 push            100000 next: 12
   12 times           next: 13
   13 push            Foo next: 14
   14 new             next: 15
   15 eval-beg        end: 19 next: 16
   16 push            '.Foo/bar next: 17
   17 push            40 next: 18
   18 pair            next: 19
   19 eval-end        next: 20
   20 dup             next: 21
   21 put-field       Foo.bar 40 next: 22
   22 dup             next: 23
   23 get-field       Foo.bar next: 24
   24 dispatch        ++(Int) next: 25
   25 swap            next: 26
   26 eval-beg        end: 28 next: 27
   27 push            '.Foo/baz next: 28
   28 eval-end        next: 29
   29 dup             next: 30
   30 rotl            next: 31
   31 put-field       Foo.baz next: 32
   32 dup             next: 33
   33 get-field       Foo.baz next: 34
   34 dispatch        ++(Int) next: 35
   35 swap            next: 36
   36 eval-beg        end: 38 next: 37
   37 push            '.Foo/qux next: 38
   38 eval-end        next: 39
   39 put-field       Foo.qux next: 40
   40 count           next: 41
   41 count           next: 42
   42 dispatch        say(A) next: 43
   43 nop             next: 43

push              11  20%
eval-beg           5  10%
eval-end           5  10%
dup                4   8%
dispatch           3   6%
put-field          3   6%
count              2   4%
get-field          2   4%
nop                2   4%
swap               2   4%
bench              1   2%
new                1   2%
pair               1   2%
rotl               1   2%
times              1   2%
```
```
$ snigl --trace 0 bench/struct.sl
380
```

### Native Executables
Passing `--cemit` on the command line emits the `C` code for an interpreter initialized with the result of compiling and tracing the specified script. The resulting file may be embedded in separate applications or compiled standalone with a `main` stub. Native executables start faster and embed the results of compile time expressions, which simplifies distribution further. Native executables are a work in progress, use of unmapped features triggers compile errors; the good news is that the situation is improving.

test.sl
```
func: fib-rec 0 Int _
func: fib-rec 1 Int _

func: fib-rec Int Int (
  -- dup fib-rec swap -- fib-rec +
)

20 fib-rec say
```

make_test
```
rm -f test
snigl compile --cemit test.sl > test.c
gcc test.c -lsnigl -lpthread -ldl -o test -O2 -g
```

```
$ ./make_test
$ ls -all test
-rwxrwxr-x 1 a a 1228768 Jan  3 20:12 test
$ ./test
6765
```

### Performance
Included below are fairly recent results from running the [benchmarks](https://gitlab.com/sifoo/snigl/blob/master/bench/run) on my machine.

```
catch
476
202

fib_iter
15
n/a
41
11

fib_rec
423
200

fib_tail
537
n/a
358
336

fix
733
52
234
n/a

func
117
75

io
413
n/a
79
387

loop
275
83

seq
65
200
125
191

stack
600
388

str
593
572

str_vars
543
116

struct
492
336

try
127
46

var
154
251

yield
569
48
```
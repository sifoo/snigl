from bench import bench

print(bench(10, '''
def test():
  42
''', '''
for _ in range(100000):
  test()
'''))


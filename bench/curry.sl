func: foo (Int Str) () drop2

&foo curry: (42 "abc")
10 bench: (100000 times: dup-call) say drop

&(42 "abc" foo)
10 bench: (100000 times: dup-call) say drop
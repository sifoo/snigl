from bench import bench

print(bench(10, '''
import functools

@functools.lru_cache()
def fib_mem(n):
  return n if n <= 1 else fib_mem(n-1) + fib_mem(n-2)
''', '''
for _ in range(100000):
  fib_mem(20)
'''))

func: fib-iter-ref Int Int {
  let: ('a :: &0 'b :: &1)
  -- times: (@a get-set:, @b get-set:, dup rotl +)
  @b get
}

10 bench: (1000 times:, 20 fib-iter-ref drop) say

func: fib-iter-stack Int Int (
  0 1 rotr -- times: (dup rotl +)
  swap drop
)

10 bench: (1000 times:, 20 fib-iter-stack drop) say
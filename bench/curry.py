from bench import bench

print(bench(10, '''
from functools import partial

def test(x, y): 
  pass

f = partial(test, 42, "abc")
''', '''
for _ in range(100000): 
  f()
'''))

print(bench(10, '''
def test(x, y): 
  pass

def f(*args):
  test(42, "abc", *args)
''', '''
for _ in range(100000): 
  f()
'''))

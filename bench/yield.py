from bench import bench

print(bench(10, '''
def task():
  for _ in range(100000):
    yield
''', '''
t1, t2, t3 = task(), task(), task()

for _ in range(100000):
  next(t1)
  next(t2)
  next(t3)
'''))

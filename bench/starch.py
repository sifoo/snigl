from bench import bench

print(bench(10, '''
from io import BytesIO
from pickle import Pickler, Unpickler
buf = BytesIO()
''', '''
w = Pickler(buf)

for i in range(10000):
  w.dump(i)
  w.dump("abc")
  w.dump(True)

buf.seek(0)
r = Unpickler(buf)

for _ in range(10000):
  r.load()
  r.load()
  r.load()
'''))


struct: Foo _ ('bar 'baz 'qux Int)

10 bench: (100000 times:,
  40 Foo new dup-put: '.Foo/bar
  dup .Foo/bar ++ swap dup-put: '.Foo/baz
  dup .Foo/baz ++ swap put: '.Foo/qux
) say
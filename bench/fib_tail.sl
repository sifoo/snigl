func: fib-tail (0 Int Int) Int (rotr drop2)
func: fib-tail (1 Int Int) Int (rotl drop2)

func: fib-tail (Int Int Int) Int (
  rotr -- rotl dup rotl + fib-tail
)

10 bench: (10000 times:, 20 0 1 fib-tail drop) say


func: fib-tail-re (0 Int Int) Int (rotr drop2)
func: fib-tail-re (1 Int Int) Int (rotl drop2)

func: fib-tail-re (Int Int Int) Int (
  rotr -- rotl dup rotl + recall: fib-tail-re
)

10 bench: (10000 times:, 20 0 1 fib-tail-re drop) say
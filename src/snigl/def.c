#include <assert.h>
#include <stdio.h>

#include "snigl/def.h"
#include "snigl/lib.h"
#include "snigl/sym.h"

struct sgl_def_type *sgl_def_type_init(struct sgl_def_type *type, const char *id) {
  type->id = id;
  type->free_def = NULL;
  return type;
}

struct sgl_def *sgl_def_init(struct sgl_def *d,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_lib *lib,
                             struct sgl_def_type *type,
                             struct sgl_sym *id) {
  d->lib = lib;
  d->type = type;
  d->id = id;
  d->pos = *pos;
  d->nrefs = 1;
  sgl_buf_init(&d->docs);
  return d;
}

void sgl_def_deref(struct sgl_def *d, struct sgl *sgl) {
  assert(d->nrefs > 0);

  if (!--d->nrefs) {
    sgl_buf_deinit(&d->docs);
    if (d->type->free_def) { d->type->free_def(d, sgl); }
  }
}

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include "snigl/def.h"
#include "snigl/form.h"
#include "snigl/func.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/vec.h"

struct sgl_lib *sgl_lib_init(struct sgl_lib *lib,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id) {
  lib->id = id;
  lib->init = NULL;
  lib->deinit = NULL;
  sgl_hash_init(&lib->defs);
  lib->init_ok = false;
  sgl_buf_init(&lib->docs);
  return lib;
}

struct sgl_lib *sgl_lib_deinit1(struct sgl_lib *lib, struct sgl *sgl) {
  sgl_ls_do(&lib->defs.root, struct sgl_hash_node, ls, n) {
    struct sgl_def *d = n->val;

    if (d->type != &SGL_DEF_TYPE) {
      sgl_def_deref(d, sgl);
      n->val = NULL;
    }
  }
  
  return lib;
}

struct sgl_lib *sgl_lib_deinit2(struct sgl_lib *lib, struct sgl *sgl) {
  sgl_ls_do(&lib->defs.root, struct sgl_hash_node, ls, n) {
    struct sgl_def *d = n->val;
    if (d) { sgl_def_deref(d, sgl); }
  }

  sgl_hash_deinit(&lib->defs, sgl);
  if (lib->deinit) { lib->deinit(lib, sgl); }
  sgl_buf_deinit(&lib->docs);
  return lib;
}

struct sgl_lib *sgl_lib_new(struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_sym *id) {
  return sgl_lib_init(sgl_malloc(&sgl->lib_pool), sgl, pos, id);
}

bool sgl_lib_add(struct sgl_lib *l,
                 struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_def *def) {
  struct sgl_hash *ds = &l->defs;
  bool is_empty = sgl_hash_init_slots(ds, sgl);
  sgl_uint_t hk = sgl_hash_key(ds, def->id);
  struct sgl_ls **hs = sgl_hash_slot(ds, hk);

  if (!is_empty) {
    struct sgl_hash_node *hn = sgl_hash_find(ds, hs, hk, def->id);
    
    if (hn && hn->val != def) {
      sgl_error(sgl, pos, sgl_sprintf("Dup def: %s", def->id->id));
      return false;
    }
  }

  sgl_hash_add(ds, sgl, hs, hk, def->id, def);
  return true;
}

struct sgl_pmacro *sgl_lib_add_pmacro(struct sgl_lib *l,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_sym *id,
                                      sgl_pmacro_imp_t imp) {
  struct sgl_pmacro *m = sgl_pmacro_new(sgl, pos, l, id, imp);
  sgl_lib_add(l, sgl, pos, &m->def);
  return m;
}

static struct sgl_ls *const_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_val *v = sgl_baseof(struct sgl_val, ls, macro->data.next);
  sgl_op_push_new(sgl, form, sgl_val_dup(v, sgl, NULL));
  return form->ls.next;
}

static void const_deinit(struct sgl_macro *m, struct sgl *sgl) {
  sgl_ls_do(&m->data, struct sgl_val, ls, v) { sgl_val_deinit(v, sgl); }
}

bool sgl_lib_add_const(struct sgl_lib *l,
                       struct sgl *sgl,
                       struct sgl_pos *pos,
                       struct sgl_sym *id,
                       struct sgl_val *val) {
  struct sgl_macro *m = sgl_lib_add_macro(l, sgl, pos, id, 0, const_imp);
  m->deinit = const_deinit;
  sgl_ls_push(&m->data, &val->ls);
  return true;
}

struct sgl_macro *sgl_lib_add_macro(struct sgl_lib *l,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_sym *id,
                                    sgl_int_t nargs,
                                    sgl_macro_imp_t imp) {
  struct sgl_macro *m = sgl_macro_new(sgl, pos, l, id, nargs, imp);
  sgl_lib_add(l, sgl, pos, &m->def);
  return m;
}

struct sgl_fimp *_sgl_lib_add_func(struct sgl_lib *lib,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id,
                                   sgl_int_t nargs,
                                   struct sgl_arg args[],
                                   struct sgl_type *rets[]) {
  struct sgl_def *d = sgl_lib_find(lib, sgl, pos, id);

  if (d && d->type != &SGL_DEF_FUNC) {
    sgl_error(sgl, pos,
              sgl_sprintf("Func def mismatch (%s): %s", d->type->id, id->id));
    
    return NULL;
  }
  
  struct sgl_func *func = d ? sgl_baseof(struct sgl_func, def, d) : NULL;
  
  if (func) {
    if (func->nargs != nargs) {
      sgl_error(sgl, pos,
                sgl_sprintf("Func args mismatch (%" SGL_INT "/%" SGL_INT "): %s",
                            nargs, func->nargs, id->id));
      
      return NULL;
    }
  } else {
    func = sgl_func_new(sgl, pos, lib, id, nargs);
    sgl_lib_add(lib, sgl, pos, &func->def);
  }
  
  return sgl_func_add_fimp(func, sgl, pos, args, rets);
}

struct sgl_fimp *_sgl_lib_add_cfunc(struct sgl_lib *lib,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_sym *id,
                                    sgl_int_t nargs,
                                    struct sgl_arg args[],
                                    struct sgl_type *rets[],
                                    sgl_cimp_t imp) {
  struct sgl_fimp *f = _sgl_lib_add_func(lib, sgl, pos, id, nargs, args, rets);
  if (!f) { return NULL; }
  f->cimp = imp;
  return f;
}

static struct sgl_def *new_type_union(struct sgl_lib *lib,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_sym *id) {
  char *start = id->id;
  struct sgl_vec pts;
  sgl_vec_init(&pts, sizeof(struct sgl_type *));

  for (;;) {
    char *end = strchr(start, '|');
    struct sgl_sym *pid = end ? sgl_nsym(sgl, start, end-start) : sgl_sym(sgl, start);
    struct sgl_type *pt = sgl_lib_find_type(lib, sgl, pos, pid);

    if (!pt) {
      sgl_error(sgl, pos, sgl_sprintf("Unknown type: %s", pid->id));
      sgl_vec_deinit(&pts);
      return NULL;
    }
    
    *(struct sgl_type **)sgl_vec_push(&pts) = pt;
    if (!end) { break; }
    start = end+1;
  }

  
  *(struct sgl_type **)sgl_vec_push(&pts) = NULL;
  struct sgl_type *u = sgl_type_union_new(sgl, pos, lib, id, sgl_vec_beg(&pts));
  sgl_vec_deinit(&pts);
  return &u->def;
}

struct sgl_def *sgl_lib_find(struct sgl_lib *l,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id) {
  struct sgl_hash *ds = &l->defs;
  sgl_uint_t hk = sgl_hash_key(ds, id);
  struct sgl_ls **hs = sgl_hash_slot(ds, hk);
  if (!hs) { goto missing; }
  struct sgl_hash_node *hn = sgl_hash_find(ds, hs, hk, id);
  if (!hn) { goto missing; }
  return hn->val;
 missing:
  if (isupper(*id->id)) {
    char *s = id->id;
    if (strchr(s, '|')) { return new_type_union(l, sgl, pos, id); }
    sgl_int_t sl = strlen(s);
    
    if (s[sl-1] == '?') {
      struct sgl_type *t = sgl_lib_find_type(l, sgl, pos, sgl_nsym(sgl, s, sl-1));
      return t ? &sgl_type_union(sgl, pos, t, sgl->Nil)->def : NULL;
    }
  }

  return NULL;
}

struct sgl_type *sgl_lib_find_type(struct sgl_lib *lib,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id) {     
  struct sgl_def *d = sgl_lib_find(sgl->task->lib, sgl, pos, id);
  if (!d) { return NULL; }
  assert(d->type == &SGL_DEF_TYPE);
  return sgl_baseof(struct sgl_type, def, d);
}

bool sgl_lib_use(struct sgl_lib *lib,
                 struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_def *def) {
  if (!sgl_lib_add(lib, sgl, pos, def)) { return false; }
  def->nrefs++;
  return true;
}

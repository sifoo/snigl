#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "snigl/pool.h"
#include "snigl/scope.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"

struct sgl_scope *sgl_scope_new(struct sgl *sgl,
                                struct sgl_scope *parent,
                                sgl_uint_t var_tag) {
  return sgl_scope_init(sgl_malloc(&sgl->scope_pool), sgl, parent, var_tag);
}

struct sgl_scope *sgl_scope_init(struct sgl_scope *s,
                                 struct sgl *sgl,
                                 struct sgl_scope *parent,
                                 sgl_uint_t var_tag) {
  s->parent = parent;
  if (parent) { parent->nrefs++; }
  sgl_ls_init(&s->defers);
  s->nvars = 0;
  s->nrefs = 1;
  s->var_tag = var_tag ? var_tag : sgl->var_tag + 1;
  return s;
}

struct sgl_scope *sgl_scope_deinit(struct sgl_scope *s, struct sgl *sgl) {
  sgl_ls_do(&s->defers, struct sgl_val, ls, v) { sgl_val_free(v, sgl); }
  if (s->parent) { sgl_scope_deref(s->parent, sgl); }
  sgl_int_t gap = 0;
  
  if (s->nvars) {
    struct sgl_vec *vs = &sgl->vars;

    sgl_vec_rdo(vs, struct sgl_var, v) {      
      if (v->scope == s && v->id) {
        sgl_val_free(v->val, sgl);
        
        if (gap) {
          v->id = NULL;
        } else {
          assert(v->tag == sgl->var_tag);
          sgl->var_tag--;
          vs->len--;
        }

        if (!--s->nvars) { break; }
      } else {
        gap++;
      }
    }
  }
  
  return s;
}

void sgl_scope_free(struct sgl_scope *s, struct sgl *sgl) {
  sgl_free(&sgl->scope_pool, sgl_scope_deinit(s, sgl));
}

void sgl_scope_deref(struct sgl_scope *s, struct sgl *sgl) {
  assert(s->nrefs > 0);
  if (!--s->nrefs) { sgl_scope_free(s, sgl); }
}

void sgl_scope_end(struct sgl_scope *s, struct sgl *sgl) {
  sgl_ls_rdo(&s->defers, struct sgl_op_defer, ls, op) {
    sgl_run_task(sgl, sgl->task, op->beg_pc->next, op->end_pc->next);
  }

  sgl_ls_init(&s->defers);
}

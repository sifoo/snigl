#ifndef SNIGL_LAMBDA_H
#define SNIGL_LAMBDA_H

#include "snigl/imp.h"
#include "snigl/int.h"
#include "snigl/ls.h"

struct sgl;
struct sgl_op;
struct sgl_scope;

struct sgl_lambda {
  struct sgl_ls ls;
  struct sgl_imp imp;
  struct sgl_scope *parent_scope;
  sgl_uint_t var_tag;
  sgl_uint_t nrefs;
};

struct sgl_lambda *sgl_lambda_new(struct sgl *sgl,
                                  struct sgl_op *beg_pc, struct sgl_op *end_pc,
                                  enum sgl_imp_flags flags);

void sgl_lambda_deref(struct sgl_lambda *l, struct sgl *sgl);

#endif

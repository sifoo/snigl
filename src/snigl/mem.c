#include <assert.h>

#include "snigl/mem.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"

struct sgl_mem *sgl_mem_new(struct sgl *sgl, struct sgl_func *func) {
  struct sgl_mem *m = sgl_malloc(&sgl->mem_pool);
  struct sgl_ls *s = sgl_stack(sgl), *sp = s->prev;

  for (struct sgl_val **a = m->args + func->nargs - 1;
       a >= m->args;
       a--,
       sp = sp->prev) {
    assert(sp != s);
    *a = sgl_val_clone(sgl_baseof(struct sgl_val, ls, sp), sgl, sgl_pos(sgl), NULL);
  }

  return m;
}

void sgl_mem_free(struct sgl_mem *m, struct sgl *sgl, struct sgl_func *func) {
  for (struct sgl_val **a = m->args; a < m->args + func->nargs; a++) {
    sgl_val_free(*a, sgl);
  }

  for (struct sgl_val **r = m->rets; r < m->rets + func->nrets; r++) {
    sgl_val_free(*r, sgl);
  }

  sgl_free(&sgl->mem_pool, m);
}

void sgl_mem_push_rets(struct sgl_mem *m, struct sgl *sgl, struct sgl_func *func) {
  struct sgl_ls *s = sgl_stack(sgl);
  
  for (struct sgl_val **r = m->rets; r < m->rets + func->nrets; r++) {
    sgl_val_clone(*r, sgl, sgl_pos(sgl), s);
  }
}

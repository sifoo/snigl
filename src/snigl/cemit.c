#include "snigl/cemit.h"

struct sgl_cemit *sgl_cemit_init(struct sgl_cemit *c) {
  c->indent = 0;
  c->fimp_id = c->form_id = c->func_id = c->sym_id = c->type_id = 0;
  c->pc = 0;
  c->tab = 2;
  sgl_buf_init(&c->buf);
  return c;
}

struct sgl_cemit *sgl_cemit_deinit(struct sgl_cemit *c) {
  sgl_buf_deinit(&c->buf);
  return c;
}

void sgl_cemit_incl(struct sgl_cemit *c, const char *path) {
  sgl_cemit_line(c, "#include \"%s\"", path);
}

void sgl_cemit_sincl(struct sgl_cemit *c, const char *path) {
  sgl_cemit_line(c, "#include <%s>", path);
}

void sgl_cemit_indent(struct sgl_cemit *c) {
  for (sgl_int_t i = 0; i < c->indent * c->tab; i++) { sgl_buf_putc(&c->buf, ' '); }
}

void sgl_cemit_line(struct sgl_cemit *c, const char *spec, ...) {
  if (spec) {
    sgl_cemit_indent(c);
    va_list args;
    va_start(args, spec);
    sgl_buf_vprintf(&c->buf, spec, args);
    va_end(args);
  }

  sgl_buf_putc(&c->buf, '\n');
}

#ifndef SNIGL_SYM_H
#define SNIGL_SYM_H

#include "snigl/int.h"
#include "snigl/ls.h"

struct sgl;
struct sgl_cemit;

struct sgl_sym {
  struct sgl_ls ls;
  char *id;
  sgl_int_t cemit_id, len;
};

struct sgl_sym *sgl_sym_init(struct sgl_sym *s, const char *id, sgl_int_t len);
struct sgl_sym *sgl_sym_deinit(struct sgl_sym *s);
sgl_int_t sgl_sym_cemit_id(struct sgl_sym *s, struct sgl *sgl, struct sgl_cemit *out);
struct sgl_sym *sgl_sym_lcase(struct sgl_sym *s, struct sgl *sgl);

enum sgl_cmp sgl_sym_cmp(const struct sgl_sym *lhs, const struct sgl_sym *rhs);

#endif

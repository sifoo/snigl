#ifndef SNIGL_HASH_H
#define SNIGL_HASH_H

#include "snigl/int.h"
#include "snigl/ls.h"

struct sgl;

struct sgl_hash {
  sgl_int_t nslots, len;
  struct sgl_ls root, **slots;
  void *data;
  bool full;

  sgl_uint_t (*fn)(void *x, void *data);
  bool (*is)(void *x, void *y, void *data);
};

struct sgl_hash_node {
  struct sgl_ls ls;
  sgl_uint_t hkey;
  void *key, *val;
};

struct sgl_hash *sgl_hash_init(struct sgl_hash *h);
struct sgl_hash *sgl_hash_deinit(struct sgl_hash *h, struct sgl *sgl);
bool sgl_hash_init_slots(struct sgl_hash *h, struct sgl *sgl);
sgl_uint_t sgl_hash_key(struct sgl_hash *h, void *key);
struct sgl_ls **sgl_hash_slot(struct sgl_hash *h, sgl_uint_t hkey);

struct sgl_hash_node *sgl_hash_find(struct sgl_hash *h,
                                    struct sgl_ls **slot,
                                    sgl_uint_t hkey,
                                    void *key);

struct sgl_hash_node *sgl_hash_add(struct sgl_hash *h,
                                   struct sgl *sgl,
                                   struct sgl_ls **slot,
                                   sgl_uint_t hkey,
                                   void *key,
                                   void *val);

#endif

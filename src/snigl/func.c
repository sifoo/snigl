#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "snigl/cemit.h"
#include "snigl/fimp.h"
#include "snigl/func.h"
#include "snigl/lib.h"
#include "snigl/mem.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"

struct sgl_def_type SGL_DEF_FUNC;

static const void *fimps_key(void *v) {
  struct sgl_fimp **f = v;
  return  &(*f)->id;
}

static enum sgl_cmp fimps_cmp(const void *lhs, const void *rhs, void *_) {
  struct sgl_sym *const *ls = lhs, *const *rs = rhs;
  return sgl_sym_cmp(*ls, *rs);
}

static const void *fimp_queue_key(void *v) {
  struct sgl_fimp **f = v;
  return  &(*f)->weight;
}

static enum sgl_cmp fimp_queue_cmp(const void *lhs, const void *rhs, void *_) {
  const sgl_int_t *lw = lhs, *rw = rhs;
  return sgl_int_cmp(*lw, *rw);
}

static sgl_uint_t mem_fn(void *x, void *data) {
  struct sgl_val **as = x;
  struct sgl_func *f = data;
  sgl_uint_t h = 0;
  for (struct sgl_val **a = as; a < as + f->nargs; a++) { h ^= sgl_val_hash(*a); }
  return h;
}

static bool mem_is(void *x, void *y, void *data) {
  struct sgl_val **xs = x, **ys = y;
  struct sgl_func *f = data;

  for (struct sgl_val **x = xs, **y = ys;
       x < xs + f->nargs;
       x++, y++) {
    if (!sgl_val_is(*x, *y)) { return false; }
  }

  return true;
}

struct sgl_func *sgl_func_init(struct sgl_func *func,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_lib *lib,
                               struct sgl_sym *id,
                               sgl_int_t nargs) {
  sgl_def_init(&func->def, sgl, pos, lib, &SGL_DEF_FUNC, id);
  sgl_vset_init(&func->fimps, sizeof(struct sgl_fimp *), fimps_cmp, 0);
  func->fimps.key_fn = fimps_key;
  sgl_vset_init(&func->fimp_queue, sizeof(struct sgl_fimp *), fimp_queue_cmp, 0);
  func->fimp_queue.key_fn = fimp_queue_key;
  func->cemit_id = -1;
  func->nfimps = 0;
  func->nargs = nargs;
  func->nrets = -1;
  memset(func->rets, 0, sizeof(func->rets));
  sgl_hash_init(&func->mem);
  func->mem.fn = mem_fn;
  func->mem.is = mem_is;
  func->mem.data = func;
  return func;
}

struct sgl_func *sgl_func_deinit(struct sgl_func *func, struct sgl *sgl) {
  sgl_vset_do(&func->fimps, struct sgl_fimp *, f) {
    sgl_free(&sgl->fimp_pool, sgl_fimp_deinit(*f, sgl));
  }

  sgl_vset_deinit(&func->fimps);
  sgl_vset_deinit(&func->fimp_queue);

  sgl_ls_do(&func->mem.root, struct sgl_hash_node, ls, n) {
    sgl_mem_free(n->val, sgl, func);
  }

  sgl_hash_deinit(&func->mem, sgl);
  return func;
}
                               
struct sgl_func *sgl_func_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_lib *lib,
                              struct sgl_sym *id,
                              sgl_int_t nargs) {
  struct sgl_ref_type *t = sgl_baseof(struct sgl_ref_type, type, sgl->Func);
  return sgl_func_init(sgl_malloc(&t->pool), sgl, pos, lib, id, nargs);
}

sgl_int_t sgl_func_cemit_id(struct sgl_func *f,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_cemit *out) {
  if (f->cemit_id != -1) { return f->cemit_id; }
  sgl_int_t sym_id = sgl_sym_cemit_id(f->def.id, sgl, out);
  if (sym_id == -1) { return -1; }
  f->cemit_id = out->func_id++;

  sgl_cemit_line(out,
                 "struct sgl_func *func%" SGL_INT " = "
                 "sgl_get_func(sgl, sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), "
                 "sym%" SGL_INT ");",
                 f->cemit_id, pos->row, pos->col, sym_id);

  sgl_cemit_line(out, "if (!func%" SGL_INT ") { return false; }", f->cemit_id);
  return f->cemit_id;
}

void sgl_func_push_fimp(struct sgl_func *f, struct sgl_fimp *fimp, sgl_int_t i) {
  *(struct sgl_fimp **)sgl_vec_insert(&f->fimps.vals, i) = fimp;
  sgl_int_t j = sgl_vset_find(&f->fimp_queue, &fimp->weight, NULL, NULL);
  *(struct sgl_fimp **)sgl_vec_insert(&f->fimp_queue.vals, j) = fimp;
  f->nfimps++;
}

struct sgl_fimp *sgl_func_add_fimp(struct sgl_func *func,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_arg args[],
                                   struct sgl_type *rets[]) { 
  struct sgl_sym *id = sgl_fimp_id(sgl, args, func->nargs);
  
  bool ok = false;
  sgl_int_t i = sgl_vset_find(&func->fimps, &id, NULL, &ok);
  
  if (ok) {
    sgl_error(sgl, pos,
              sgl_sprintf("Dup fimp: %s(%s)", func->def.id->id, id->id));
    
    return NULL;
  }

  struct sgl_fimp *fimp = sgl_fimp_new(sgl, pos, func, args, rets);
  
  if (func->nfimps) {
    func->nrets = sgl_min(func->nrets, fimp->nrets);

    for (struct sgl_type **src = fimp->rets, **dst = func->rets;
         src < fimp->rets + func->nrets;
         src++, dst++) { *dst = sgl_type_root(*dst, *src); }
  } else {
    func->nrets = fimp->nrets;

    for (struct sgl_type **src = fimp->rets, **dst = func->rets;
         src < fimp->rets + func->nrets;
         src++, dst++) { *dst = *src; }
  }
  
  sgl_func_push_fimp(func, fimp, i);
  return fimp;
}

struct sgl_fimp *sgl_func_get_fimp(struct sgl_func *func,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_sym *id) {
  bool ok = false;
  sgl_int_t i = sgl_vset_find(&func->fimps, &id, NULL, &ok);

  if (!ok) {
    sgl_error(sgl, pos,
              sgl_sprintf("Unknown fimp: %s(%s)", func->def.id->id, id->id));
    
    return NULL;
  }

  return ok ? *(struct sgl_fimp **)sgl_vec_get(&func->fimps.vals, i) : NULL;
}
                               
struct sgl_fimp *sgl_func_fimp(struct sgl_func *func) {
  struct sgl_vec *fs = &func->fimps.vals;
  return (fs->len == 1) ? *(struct sgl_fimp **)sgl_vec_beg(fs) : NULL;
}

void sgl_func_mem_add(struct sgl_func *func, struct sgl *sgl, struct sgl_mem *mem) {
  struct sgl_hash *m = &func->mem;
  sgl_hash_init_slots(m, sgl);
  struct sgl_val **k = mem->args;
  sgl_uint_t hk = sgl_hash_key(m, k);
  struct sgl_ls **hs = sgl_hash_slot(m, hk);
  sgl_hash_add(m, sgl, hs, hk, k, mem);
  struct sgl_ls *s = sgl_stack(sgl), *sp = s->prev;
  struct sgl_val **rs = mem->rets;
  
  for (struct sgl_val **r = rs + func->nrets - 1;
       r >= rs;
       r--,
       sp = sp->prev) {
    assert(sp != s);
    *r = sgl_val_clone(sgl_baseof(struct sgl_val, ls, sp), sgl, sgl_pos(sgl), NULL);
  }
}

struct sgl_mem *sgl_func_mem_get(struct sgl_func *func, struct sgl *sgl) {
  struct sgl_val *k[SGL_MAX_ARGS];
  struct sgl_ls *s = sgl_stack(sgl), *sp = s->prev;

  for (struct sgl_val **kv = k + func->nargs - 1;
       kv >= k;
       kv--, sp = sp->prev) {
    if (sp == s) { return NULL; }
    *kv = sgl_baseof(struct sgl_val, ls, sp);
  }

  struct sgl_hash *m = &func->mem;
  sgl_uint_t hk = sgl_hash_key(m, k);
  struct sgl_ls **hs = sgl_hash_slot(m, hk);

  if (hs) {
    struct sgl_hash_node *hn = sgl_hash_find(m, hs, hk, k);
    if (hn) { return hn->val; }
  }

  return NULL;
}

struct sgl_fimp *sgl_func_dispatch(struct sgl_func *func,
                                   struct sgl *sgl,
                                   struct sgl_ls *stack) {
  sgl_vset_do(&func->fimp_queue, struct sgl_fimp *, f) {
    if (sgl_fimp_match(*f, sgl, stack)) { return *f; }
  }

  return NULL;
}

struct sgl_fimp *sgl_func_dispatch_types(struct sgl_func *func,
                                         struct sgl *sgl,
                                         struct sgl_ls *stack) {
  sgl_vset_do(&func->fimp_queue, struct sgl_fimp *, f) {  
    struct sgl_arg *a = (*f)->args + func->nargs - 1;
    bool ok = true;
    
    for (struct sgl_ls *s = stack->prev;
         a >= (*f)->args && s != stack;
         a--, s = s->prev) {
      struct sgl_val *av = a->val, *sv = sgl_baseof(struct sgl_val, ls, s);
      
      if (av) {
        if (sv->type == sgl->Undef) { return NULL; }

        if (!sgl_val_eq(av, sgl, sv)) {
          ok = false;
          break;
        }
        
        continue;      
      }
      
      struct sgl_type
        *at = a->type,
        *st = (sv->type == sgl->Undef) ? sv->as_type : sv->type;
      
      if (!sgl_derived(st, at)) {
        if (!sgl_derived(at, st)) {
          ok = false;
          break;
        }
        
        return NULL;
      }
    }

    if (ok) { return *f; }
  }

  return NULL;  
}

struct sgl_op *_sgl_func_call(struct sgl_func *f,
                              struct sgl *sgl,
                              bool now,
                              struct sgl_val *args[]) {
  struct sgl_op *call_pc = sgl->pc;
  struct sgl_ls *s = sgl_stack(sgl);
  for (struct sgl_val **a = args; *a; a++) { sgl_val_dup(*a, sgl, s); }
  struct sgl_fimp *found = sgl_func_dispatch(f, sgl, s);

  if (!found) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Func not applicable: %s", f->def.id->id));
    
    return sgl->end_pc;
  }

  struct sgl_op *pc = sgl_fimp_call(found, sgl, sgl_pos(sgl), call_pc);
  if (!pc) { return sgl->end_pc; }
  if (!now || found->cimp) { return pc; }

  return sgl_run_task(sgl, sgl->task, pc, call_pc->next)
    ? call_pc->next
    : sgl->end_pc;
}

static void free_def(struct sgl_def *def, struct sgl *sgl) {
  struct sgl_ref_type *t = sgl_baseof(struct sgl_ref_type, type, sgl->Func);
  sgl_free(&t->pool, sgl_func_deinit(sgl_baseof(struct sgl_func, def, def), sgl));
}

void sgl_setup_funcs() {
  sgl_def_type_init(&SGL_DEF_FUNC, "func");
  SGL_DEF_FUNC.free_def = free_def;
}

#ifndef SNIGL_PAIR_H
#define SNIGL_PAIR_H

struct sgl;
struct sgl_val;

struct sgl_pair {
  struct sgl_val *first, *last;
};

struct sgl_pair *sgl_pair_init(struct sgl_pair *p,
                               struct sgl_val *first, struct sgl_val *last);

struct sgl_pair *sgl_pair_deinit(struct sgl_pair *p, struct sgl *sgl);

#endif

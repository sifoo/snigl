#include <string.h>

#include "snigl/cmp.h"

enum sgl_cmp sgl_char_cmp(char lhs, char rhs) {
  if (lhs < rhs) { return SGL_LT; }
  return (lhs > rhs) ? SGL_GT : SGL_EQ;
}

enum sgl_cmp sgl_cs_cmp(const char *lhs, const char *rhs) {
  int res = strcmp(lhs, rhs);
  if (res < 0) { return SGL_LT; }
  return (res > 0) ? SGL_GT : SGL_EQ;
}

enum sgl_cmp sgl_ptr_cmp(const void *lhs, const void *rhs) {
  if (lhs < rhs) { return SGL_LT; }
  return (lhs > rhs) ? SGL_GT : SGL_EQ;
}

#ifndef SNIGL_REF_TYPE_H
#define SNIGL_REF_TYPE_H

#include "snigl/type.h"
#include "snigl/pool.h"

struct sgl_ref_type {
  struct sgl_type type;
  struct sgl_pool pool;
};

struct sgl_ref_type *sgl_ref_type_init(struct sgl_ref_type *t,
                                       struct sgl *sgl,
                                       struct sgl_pos *pos,
                                       struct sgl_lib *lib,
                                       struct sgl_sym *id,
                                       struct sgl_type *parents[],
                                       sgl_int_t val_size,
                                       sgl_int_t ls_offs);

struct sgl_ref_type *sgl_ref_type_deinit(struct sgl_ref_type *t);

struct sgl_type *sgl_ref_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[],
                                  sgl_int_t val_size,
                                  sgl_int_t ls_offs);

#endif

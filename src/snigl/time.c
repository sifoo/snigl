#include "snigl/time.h"

enum sgl_cmp sgl_time_cmp(time_t lhs, time_t rhs) {
  if (lhs < rhs) { return SGL_LT; }
  return (lhs > rhs) ? SGL_GT : SGL_EQ;
}

time_t sgl_now() { return time(NULL); }

#ifndef SNIGL_PMACRO_H
#define SNIGL_PMACRO_H

#include "snigl/def.h"
#include "snigl/ls.h"

struct sgl_pmacro;

typedef const char *(*sgl_pmacro_imp_t)(struct sgl_pmacro *m,
                                        const char *in,
                                        struct sgl *sgl,
                                        struct sgl_pos *pos,
                                        char end_char,
                                        struct sgl_ls *out);

struct sgl_pmacro {
  struct sgl_def def;
  struct sgl_ls ls;
  sgl_pmacro_imp_t imp;
};

extern struct sgl_def_type SGL_DEF_PMACRO;

struct sgl_pmacro *sgl_pmacro_init(struct sgl_pmacro *m,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   sgl_pmacro_imp_t imp);

struct sgl_pmacro *sgl_pmacro_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  sgl_pmacro_imp_t imp);

const char *sgl_pmacro_call(struct sgl_pmacro *m,
                            const char *in,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            char end_char,
                            struct sgl_ls *out);

void sgl_setup_pmacros();

#endif

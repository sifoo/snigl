#ifndef SNIGL_MEM_H
#define SNIGL_MEM_H

#include "snigl/config.h"
#include "snigl/ls.h"

struct sgl;
struct sgl_func;
struct sgl_val;

struct sgl_mem {
  struct sgl_ls ls;
  struct sgl_val *args[SGL_MAX_ARGS], *rets[SGL_MAX_RETS];
};

struct sgl_mem *sgl_mem_new(struct sgl *sgl, struct sgl_func *func);
void sgl_mem_free(struct sgl_mem *m, struct sgl *sgl, struct sgl_func *func);
void sgl_mem_push_rets(struct sgl_mem *m, struct sgl *sgl, struct sgl_func *func);

#endif

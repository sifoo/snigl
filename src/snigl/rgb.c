#include "snigl/rgb.h"

struct sgl_rgb *sgl_rgb_init(struct sgl_rgb *rgb, int32_t r, int32_t g, int32_t b) {
  rgb->r = r;
  rgb->g = g;
  rgb->b = b;
  return rgb;
}

#include <assert.h>

#include "snigl/hash_map.h"
#include "snigl/pool.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/val.h"
#include "snigl/util.h"

static sgl_uint_t fn_imp(void *x, void *data) {
  return sgl_val_hash(x);
}

static bool is_imp(void *x, void *y, void *data) {
  return sgl_val_is(x, y);
}

struct sgl_hash_map *sgl_hash_map_new(struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->HashMap);
  struct sgl_hash_map *m = sgl_malloc(&rt->pool);
  struct sgl_hash *h = &m->hash;
  sgl_hash_init(h);
  h->fn = fn_imp;
  h->is = is_imp;
  m->nrefs = 1;
  return m;
}

void sgl_hash_map_deref(struct sgl_hash_map *m, struct sgl *sgl) {
  assert(m->nrefs > 0);

  if (!--m->nrefs) {
    sgl_ls_do(&m->hash.root, struct sgl_hash_node, ls, n) {
      sgl_val_free(n->key, sgl);
      sgl_val_free(n->val, sgl);
    }
    
    sgl_hash_deinit(&m->hash, sgl);
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->HashMap);
    sgl_free(&rt->pool, m);
  }
}

struct sgl_val *sgl_hash_map_at(struct sgl_hash_map *m,
                                struct sgl *sgl,
                                struct sgl_val *key) {
  struct sgl_hash *h = &m->hash;
  sgl_uint_t hk = sgl_hash_key(h, key);
  struct sgl_ls **hs = sgl_hash_slot(h, hk);
  if (!hs) { return NULL; }
  struct sgl_hash_node *hn = sgl_hash_find(h, hs, hk, key);
  return hn ? hn->val : NULL;
}

bool sgl_hash_map_put(struct sgl_hash_map *m,
                      struct sgl *sgl,
                      struct sgl_val *key,
                      struct sgl_val *val) {
  struct sgl_hash *h = &m->hash;
  bool is_empty = sgl_hash_init_slots(h, sgl);
  sgl_uint_t hk = sgl_hash_key(h, key);
  struct sgl_ls **hs = sgl_hash_slot(h, hk);

  if (!is_empty) {
    struct sgl_hash_node *hn = sgl_hash_find(h, hs, hk, key);

    if (hn) {
      sgl_val_free(key, sgl);
      sgl_val_free(hn->val, sgl);
      hn->val = val;
      return false;
    }
  }

  sgl_hash_add(h, sgl, hs, hk, key, val);
  return true;
}

static struct sgl_val *iter_next_val(struct sgl_iter *i,
                                     struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_ls *root) {
  struct sgl_hash_map_iter *mi = sgl_baseof(struct sgl_hash_map_iter, iter, i);
  if ((mi->pos = mi->pos->next)  == mi->root) { return NULL; }  
  struct sgl_hash_node *n = sgl_baseof(struct sgl_hash_node, ls, mi->pos);
  struct sgl_val *p = sgl_val_new(sgl, sgl->Pair, root);

  sgl_pair_init(&p->as_pair,
                sgl_val_dup(n->key, sgl, NULL),
                sgl_val_dup(n->val, sgl, NULL));

  return p;
}

static bool iter_skip_vals(struct sgl_iter *i,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           sgl_int_t nvals) {
  struct sgl_hash_map_iter *mi = sgl_baseof(struct sgl_hash_map_iter, iter, i);
  
  for (mi->pos = mi->pos->next;
       mi->pos != mi->root && --nvals;
       mi->pos = mi->pos->next);

  if (nvals) {
    sgl_error(sgl, pos, sgl_strdup("Nothing to skip"));
    return false;
  }

  return true;
}

static void iter_free(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_hash_map_iter *mi = sgl_baseof(struct sgl_hash_map_iter, iter, i);
  sgl_hash_map_deref(mi->map, sgl);
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->HashMapIter);
  sgl_free(&rt->pool, mi);
}

struct sgl_hash_map_iter *sgl_hash_map_iter_new(struct sgl *sgl,
                                        struct sgl_hash_map *map,
                                        struct sgl_ls *pos) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->HashMapIter);
  struct sgl_hash_map_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = iter_next_val;
  i->iter.skip_vals = iter_skip_vals;
  i->iter.free = iter_free;
  i->map = map;
  map->nrefs++;
  i->root = &map->hash.root;
  i->pos = pos ? pos : i->root;
  return i;
}

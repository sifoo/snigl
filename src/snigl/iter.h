#ifndef SNIGL_ITER_H
#define SNIGL_ITER_H

#include "snigl/int.h"
#include "snigl/ls.h"

struct sgl;
struct sgl_pos;
struct sgl_val;

struct sgl_iter {
  struct sgl_ls ls;

  struct sgl_iter *(*clone)(struct sgl_iter *, struct sgl *);
  bool (*eq)(struct sgl_iter *, struct sgl *, struct sgl_iter *);
  void (*free)(struct sgl_iter *, struct sgl *);
  
  struct sgl_val *(*next_val)(struct sgl_iter *i,
                              struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_ls *root);
  
  bool (*skip_vals)(struct sgl_iter *i,
                    struct sgl *sgl,
                    struct sgl_pos *pos,
                    sgl_int_t nvals);
    
  sgl_int_t nrefs;
};

struct sgl_iter *sgl_iter_init(struct sgl_iter *i);
void sgl_iter_deref(struct sgl_iter *i, struct sgl *sgl);

struct sgl_val *sgl_iter_next_val(struct sgl_iter *i,
                                  struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root);

bool sgl_iter_skip_vals(struct sgl_iter *i,
                        struct sgl *sgl,
                        struct sgl_pos *pos,
                        sgl_int_t nvals);

#endif

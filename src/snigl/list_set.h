#ifndef SNIGL_LIST_SET_H
#define SNIGL_LIST_SET_H

#include "snigl/cmp.h"
#include "snigl/list.h"

struct sgl;

struct sgl_list_set {
  struct sgl *sgl;
  struct sgl_list list;
  struct sgl_val *key;
  sgl_ls_cmp_t cmp;
};

struct sgl_list_set *sgl_list_set_new(struct sgl *sgl, struct sgl_val *key);
void sgl_list_set_deref(struct sgl_list_set *s);

struct sgl_val *sgl_list_set_key(struct sgl_list_set *s, struct sgl_val *val);

struct sgl_ls *sgl_list_set_find(struct sgl_list_set *s,
                                 struct sgl_val *key,
                                 bool *ok);

#endif

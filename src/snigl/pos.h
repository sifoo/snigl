#ifndef SNIGL_POS_H
#define SNIGL_POS_H

#include "snigl/int.h"

struct sgl_pos {
  const char *load_path;
  sgl_int_t row, col;
};

extern struct sgl_pos SGL_NULL_POS;
extern struct sgl_pos SGL_START_POS;

struct sgl_pos *sgl_pos_init(struct sgl_pos *pos,
                             const char *load_path,
                             sgl_int_t row, sgl_int_t col);

void sgl_setup_pos();

#endif

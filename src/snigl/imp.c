#include <stddef.h>

#include "snigl/form.h"
#include "snigl/imp.h"

struct sgl_imp *sgl_imp_init(struct sgl_imp *imp,
                             struct sgl_form *body,
                             enum sgl_imp_flags flags) {
  imp->body = body;
  body->nrefs++;
  imp->beg_pc = imp->end_pc = imp->rec_pc = NULL;
  imp->flags = flags;
  return imp;
}

struct sgl_imp *sgl_imp_deinit(struct sgl_imp *imp, struct sgl *sgl) {
  sgl_form_deref(imp->body, sgl);
  return imp;
}

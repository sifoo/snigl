#ifndef SNIGL_ARG_H
#define SNIGL_ARG_H

#include <stdbool.h>

#include "snigl/int.h"

#define sgl_rets(...)                           \
  (struct sgl_type *[]){__VA_ARGS__, NULL}      \

struct sgl;
struct sgl_buf;
struct sgl_cemit;
struct sgl_pos;
struct sgl_type;
struct sgl_val;

struct sgl_arg {
  struct sgl_type *type;
  struct sgl_val *val;
};

struct sgl_arg *sgl_arg_init(struct sgl_arg *a,
                             struct sgl_type *type,
                             struct sgl_val *val);

struct sgl_arg *sgl_arg_deinit(struct sgl_arg *a, struct sgl *sgl);
sgl_int_t sgl_arg_weight(struct sgl_arg *a);
void sgl_arg_dump(struct sgl_arg *a, struct sgl_buf *out);
bool sgl_arg_cemit(struct sgl_arg *a,
                   struct sgl *sgl,
                   struct sgl_pos *pos,
                   sgl_int_t id, sgl_int_t i,
                   struct sgl_cemit *out);

struct sgl_arg sgl_arg(struct sgl_type *type);

#endif

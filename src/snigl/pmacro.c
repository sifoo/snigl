#include "snigl/pmacro.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"

struct sgl_def_type SGL_DEF_PMACRO;

struct sgl_pmacro *sgl_pmacro_init(struct sgl_pmacro *m,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   sgl_pmacro_imp_t imp) {
  sgl_def_init(&m->def, sgl, pos, lib, &SGL_DEF_PMACRO, id);
  m->imp = imp;
  return m;
}

struct sgl_pmacro *sgl_pmacro_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  sgl_pmacro_imp_t imp) {
  return sgl_pmacro_init(sgl_malloc(&sgl->pmacro_pool), sgl, pos, lib, id, imp);
}

const char *sgl_pmacro_call(struct sgl_pmacro *m,
                            const char *in,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            char end_char,
                            struct sgl_ls *out) {
  return m->imp(m, in, sgl, pos, end_char, out);
}

static void free_def(struct sgl_def *def, struct sgl *sgl) {
  struct sgl_pmacro *m = sgl_baseof(struct sgl_pmacro, def, def);
  sgl_free(&sgl->pmacro_pool, m);
}

void sgl_setup_pmacros() {
  sgl_def_type_init(&SGL_DEF_PMACRO, "pmacro");
  SGL_DEF_PMACRO.free_def = free_def;
}

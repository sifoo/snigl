#include <assert.h>
#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/starch.h"
#include "snigl/sym.h"

enum sgl_store_id sgl_load_id(struct sgl_buf *in) {
  sgl_store_id_t *p = sgl_buf_get(in, sizeof(sgl_store_id_t));
  return p ? *p : SGL_STORE_INVALID;
}

void sgl_store_id(enum sgl_store_id id, struct sgl_buf *out) {
  sgl_buf_grow(out, out->len + 1);
  sgl_store_id_t *v = (sgl_store_id_t *)out->data + out->len++;
  *v = id;
}

bool sgl_load_short(struct sgl_buf *in, uint8_t *out) {
  uint8_t *v = sgl_buf_get(in, 1);
  if (!v) { return false; }
  *out = *v;
  return true;
}

void sgl_store_short(sgl_int_t v, struct sgl_buf *out) {    
  uint8_t *p = out->data + out->len++;
  *p = v;
}

bool sgl_load_int(enum sgl_store_id id,
                  struct sgl_buf *in,
                  struct sgl *sgl,
                  sgl_int_t *out) {
  switch(id) {
  case SGL_STORE_INT_ZERO:
    *out = 0;
    break;
  case SGL_STORE_INT_ONE:
    *out = 1;
    break;
  case SGL_STORE_INT_SHORT: {
    uint8_t *v = sgl_buf_get(in, 1);
    if (!v) { return false; }
    *out = *v;
    break;
  }
  case SGL_STORE_INT: {
    uint8_t *lp = sgl_buf_get(in, 1);
    if (!lp) { return false; }
    
    uint8_t l = *lp;
    assert(l);
    
    uint8_t *ip = sgl_buf_get(in, l);
    if (!ip) { return false; }
    sgl_int_t i = 0, m = 1;
    
    while (l--) {
      i = i + (*ip++ * m);
      m *= 256;
    }
    
    *out = i;
    break;
  }
  default:
    return false;
  }

  return true;
}

void sgl_store_int(sgl_int_t v, struct sgl_buf *out) {    
  switch(v) {
  case 0:
    sgl_store_id(SGL_STORE_INT_ZERO, out);
    break;
  case 1:
    sgl_store_id(SGL_STORE_INT_ONE, out);
    break;
  default: {
    sgl_buf_grow(out, out->len + 2);

    if (v < 256) {
      sgl_store_id(SGL_STORE_INT_SHORT, out);
      uint8_t *p = out->data + out->len++;
      *p = v;
    } else {
      sgl_store_id(SGL_STORE_INT, out);    
      sgl_int_t offs = out->len++;
      
      while (v) {
        uint8_t *p = out->data + out->len++;
        *p = v % 256;
        v /= 256;
        
        if (v) { sgl_buf_grow(out, out->len + 1); }
      }
      
      uint8_t *p = out->data + offs;
      *p = out->len - offs - 1;
    }
  }}
}

static enum sgl_cmp store_vars_cmp(const void *lhs, const void *rhs, void *sgl) {
  return sgl_val_cmp(*(struct sgl_val **)lhs, sgl, *(struct sgl_val **)rhs);
}

struct sgl_starch *sgl_starch_new(struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->Starch);
  struct sgl_starch *s = sgl_malloc(&rt->pool);
  
  sgl_buf_init(&s->buf);
  sgl_vec_init(&s->load_vals, sizeof(struct sgl_val *));
  
  sgl_vset_init(&s->store_vars,
                sizeof(struct sgl_store_var),
                store_vars_cmp,
                offsetof(struct sgl_store_var, val));

  
  return s;
}

struct sgl_starch *sgl_starch_ref(struct sgl_starch *s) {
  s->buf.nrefs++;
  return s;
}

void sgl_starch_deref(struct sgl_starch *s, struct sgl *sgl) {
  assert(s->buf.nrefs > 0);
  
  if (!--s->buf.nrefs) {
    sgl_buf_deinit(&s->buf);
    sgl_vec_do(&s->load_vals, struct sgl_val *, v) { sgl_val_free(*v, sgl); }
    sgl_vec_deinit(&s->load_vals);

    sgl_vset_do(&s->store_vars, struct sgl_store_var, v) {
      sgl_val_free(v->val, sgl);
    }

    sgl_vset_deinit(&s->store_vars);
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->Starch);
    sgl_free(&rt->pool, s);
  }
}

struct sgl_val *sgl_starch_load(struct sgl_starch *s,
                                struct sgl *sgl,
                                struct sgl_ls *root) {
  struct sgl_buf *sb = &s->buf;
  enum sgl_store_id id = sgl_load_id(sb);
  if (id == SGL_STORE_INVALID) { return NULL; }
  sgl_load_t l = sgl->loaders[id];
  
  if (!l) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Not loadable: %d", id));
    return NULL;
  }

  struct sgl_val *v = l(s, sgl, id, root);

  switch (id) {
  case SGL_STORE_DUP:
  case SGL_STORE_DUP_SHORT:
  case SGL_STORE_CLONE:
  case SGL_STORE_CLONE_SHORT:
    break;
  default:
    if (v && v->type->is_ref) {
      *(struct sgl_val **)sgl_vec_push(&s->load_vals) = sgl_val_dup(v, sgl, NULL);
    }
  }
  
  return v;
}

bool sgl_starch_store(struct sgl_starch *s,
                      struct sgl *sgl,
                      struct sgl_val *val) {
  struct sgl_type *t = val->type;
  
  if (!t->store_val) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Not storable: %s", sgl_type_id(t)->id));
    return NULL;
  }

  if (t->is_ref) {
    struct sgl_vset *vs = &s->store_vars;
    bool ok = false;

    sgl_int_t i = sgl_vset_find(vs, &val, sgl, &ok);

    if (ok) {
      struct sgl_store_var *v = sgl_vec_get(&vs->vals, i);
      struct sgl_buf *sb = &s->buf;
      bool clone = !sgl_val_is(val, v->val);

      if (v->i < 256) {
        sgl_store_id(clone ? SGL_STORE_CLONE_SHORT : SGL_STORE_DUP_SHORT, sb);
        sgl_store_short(v->i, sb);
      } else {
        sgl_store_id(clone ? SGL_STORE_CLONE : SGL_STORE_DUP, sb);
        sgl_store_int(v->i, sb);
      }
      
      return true;
    }
    
    struct sgl_store_var *v = sgl_vec_insert(&vs->vals, i);
    v->i = vs->vals.len - 1;
    v->val = sgl_val_dup(val, sgl, NULL);
  }
  
  return val->type->store_val(val, sgl, s);
}

static struct sgl_val *iter_next_val(struct sgl_iter *i,
                                     struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_ls *root) {
  struct sgl_starch_iter *si = sgl_baseof(struct sgl_starch_iter, iter, i);
  return sgl_starch_load(si->src, sgl, root);
}

static bool iter_skip_vals(struct sgl_iter *i,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           sgl_int_t nvals) {
  struct sgl_starch_iter *si = sgl_baseof(struct sgl_starch_iter, iter, i);
  struct sgl_starch *s = si->src;
  
  while (nvals--) {
    struct sgl_val *v = sgl_starch_load(s, sgl, NULL);
    if (!v) { break; }
    sgl_val_free(v, sgl);
  }

  if (nvals) {
    sgl_error(sgl, pos, sgl_strdup("Nothing to skip"));
    return false;
  }

  return true;
}

static void iter_free(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_starch_iter *si = sgl_baseof(struct sgl_starch_iter, iter, i);
  sgl_starch_deref(si->src, sgl);
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->StarchIter);
  sgl_free(&rt->pool, si);
}

struct sgl_starch_iter *sgl_starch_iter_new(struct sgl_starch *src, struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->StarchIter);
  struct sgl_starch_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = iter_next_val;
  i->iter.skip_vals = iter_skip_vals;
  i->iter.free = iter_free;
  i->src = sgl_starch_ref(src);
  return i;
}

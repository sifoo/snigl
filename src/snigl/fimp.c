#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/cemit.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/fimp.h"
#include "snigl/form.h"
#include "snigl/val.h"

sgl_int_t sgl_fimp_weight(struct sgl_arg args[], sgl_int_t nargs) {
  sgl_int_t w = 0;
  for (struct sgl_arg *a = args; nargs; a++, nargs--) { w += sgl_arg_weight(a); }
  return SGL_INT_MAX - w;
}

struct sgl_sym *sgl_fimp_id(struct sgl *sgl, struct sgl_arg args[], sgl_int_t nargs) {
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  char sep = 0;
  
  for (struct sgl_arg *a = args; nargs; a++, nargs--) {
    if (sep) { sgl_buf_putc(&buf, sep); }
    sgl_arg_dump(a, &buf);
    sep = ' ';
  }

  struct sgl_sym *id = sgl_sym(sgl, sgl_buf_cs(&buf));
  sgl_buf_deinit(&buf);
  return id;
}

struct sgl_fimp *sgl_fimp_init(struct sgl_fimp *fimp,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_func *func,
                               struct sgl_arg args[],
                               struct sgl_type *rets[]) {
  fimp->func = func;
  fimp->id = func->nargs ? sgl_fimp_id(sgl, args, func->nargs) : sgl_sym(sgl, "");
  fimp->weight = sgl_fimp_weight(args, func->nargs);
  memcpy(fimp->args, args, sizeof(struct sgl_arg) * func->nargs);
  fimp->nrets = -1;
  
  if (rets) {
    fimp->nrets = 0;

    for (struct sgl_type **src = rets, **dst = fimp->rets; *src; src++, dst++) {
      *dst = *src;
      fimp->nrets++;
    }
  }
  
  fimp->cimp = NULL;
  fimp->data = NULL;
  fimp->trace_depth = 0;
  fimp->cemit_id = -1;
  return fimp;
}

struct sgl_fimp *sgl_fimp_deinit(struct sgl_fimp *fimp, struct sgl *sgl) {
  if (!fimp->cimp) { sgl_imp_deinit(&fimp->imp, sgl); }

  for (sgl_int_t i = 0; i < fimp->func->nargs; i++) {
    sgl_arg_deinit(fimp->args+i, sgl);
  }
  
  return fimp;
}

struct sgl_fimp *sgl_fimp_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_func *func,
                              struct sgl_arg args[],
                              struct sgl_type *rets[]) {
  return sgl_fimp_init(sgl_malloc(&sgl->fimp_pool), sgl, pos, func, args,rets);
}

sgl_int_t sgl_fimp_cemit_id(struct sgl_fimp *f,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_cemit *out) {
  if (f->cemit_id != -1) { return f->cemit_id; }
  sgl_int_t sym_id = sgl_sym_cemit_id(f->id, sgl, out);
  sgl_int_t fid = f->cemit_id = out->fimp_id++;
  
  if (f->cimp) {
    sgl_int_t func_id = sgl_func_cemit_id(f->func, sgl, pos, out);
    if (func_id == -1) { return -1; }

    sgl_cemit_line(out,
                   "struct sgl_fimp *fimp%" SGL_INT " = "
                   "sgl_func_get_fimp(func%" SGL_INT ", sgl, "
                   "sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), sym%" SGL_INT ");",
                   fid, func_id, pos->row, pos->col, sym_id);

    sgl_cemit_line(out, "if (!fimp%" SGL_INT ") { return false; }", fid);
  } else {
    sgl_cemit_line(out,
                   "struct sgl_arg args%" SGL_INT "[%" SGL_INT "];",
                   fid, f->func->nargs);

    for (sgl_int_t i = 0; i < f->func->nargs; i++) {
      sgl_arg_cemit(f->args + i, sgl, pos, fid, i, out); 
    }
    
    if (f->nrets < 1) {
      sgl_cemit_line(out, "struct sgl_type **rets%" SGL_INT " = NULL;", fid);
    } else {
      sgl_cemit_line(out,
                     "struct sgl_type *rets%" SGL_INT "[%" SGL_INT "] = {0};",
                     fid, f->nrets+1);

      for (sgl_int_t i = 0; i < f->nrets; i++) {
        sgl_int_t type_id = sgl_type_cemit_id(f->rets[i], sgl, pos, out);
        if (type_id == -1) { return -1; }
        
        sgl_cemit_line(out,
                       "rets%" SGL_INT "[%" SGL_INT "] = type%" SGL_INT ";",
                       fid, i, type_id);
      }
    }

    sym_id = sgl_sym_cemit_id(f->func->def.id, sgl, out);

    sgl_cemit_line(out,
                   "struct sgl_fimp *fimp%" SGL_INT " = "
                   "_sgl_lib_add_func(lib, sgl, "
                   "sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), "
                   "sym%" SGL_INT ", "
                   "%" SGL_INT ", "
                   "args%" SGL_INT ", "
                   "rets%" SGL_INT ");",
                   fid, pos->row, pos->col, sym_id, f->func->nargs, fid, fid);
    
    sgl_int_t body_id = sgl_form_cemit_id(f->imp.body, sgl, out);
    if (body_id == -1) { return -1; }
    
    sgl_cemit_line(out,
                   "sgl_imp_init(&fimp%" SGL_INT "->imp, f%" SGL_INT ");",
                   fid, body_id);
  }
  
  return fid;
}

bool sgl_fimp_match(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_ls *stack) {
  struct sgl_type *A = sgl->A;
  struct sgl_arg *a = fimp->args + fimp->func->nargs - 1;

  for (struct sgl_ls *s = stack->prev; a >= fimp->args; a--, s = s->prev) {
    if (s == stack) { return false; }
    struct sgl_type *at = a->type;
    struct sgl_val *av = a->val;    
    if (!av && at == A) { continue; }
    struct sgl_val *sv = sgl_baseof(struct sgl_val, ls, s);
    
    if (av) {
      if (!sgl_val_eq(av, sgl, sv)) { return false; }
      continue;
    }
    
    struct sgl_type *st = sv->type;
    if (at == st) { continue; }    

    if (!*(struct sgl_type **)((unsigned char *)st + at->parent_offs)) {
      return false;
    }
  }
  
  return true;
}

bool sgl_fimp_match_types(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_ls *stack) {
  struct sgl_arg *a = fimp->args + fimp->func->nargs - 1;
  
  for (struct sgl_ls *s = stack->prev;
       a >= fimp->args && s != stack;
       a--, s = s->prev) {
    struct sgl_val *av = a->val, *sv = sgl_baseof(struct sgl_val, ls, s);

    if (av) {
      if (sv->type == sgl->Undef || !sgl_val_eq(av, sgl, sv)) { return false; }
      continue;
    }
  
    struct sgl_type
      *at = a->type,
      *st = (sv->type == sgl->Undef) ? sv->as_type : sv->type;
    
    if (!sgl_derived(st, at)) { return false; }
  }

  return true;
}

bool sgl_fimp_compile(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_pos *pos) {
  if (fimp->cimp || fimp->imp.beg_pc) { return true; }
  
  struct sgl_imp *i = &fimp->imp;
  i->beg_pc = sgl_op_fimp_new(sgl, i->body, fimp);

  if (!sgl_form_compile(i->body, sgl)) {
    sgl_error(sgl, &i->body->pos,
              sgl_sprintf("Failed compiling fimp: %s(%s)",
                          fimp->func->def.id->id, fimp->id->id));
    
    return false;
  }
         
  if (sgl->end_pc->prev == i->beg_pc && fimp->nrets <= 0) {
    i->end_pc = i->beg_pc;
  } else {
    i->rec_pc = i->beg_pc;

    for (struct sgl_op *op = i->beg_pc->next; op != sgl->end_pc; op = op->next) {
      if (op->type == &SGL_OP_RECALL) {
        i->flags |= SGL_IMP_RECALL;
        break;
      }
    }

    if (i->beg_pc->next->type == &SGL_OP_SCOPE_BEG) {
      i->flags |= SGL_IMP_SCOPE;
      struct sgl_op *beg = i->beg_pc->next, *end = beg->as_scope_beg.end_pc;
      sgl_nop(beg, sgl);
      sgl_nop(end, sgl);
    }
    
    if (fimp->nrets > 0) { sgl_op_fimp_rets_new(sgl, i->body, fimp); }
    i->end_pc = sgl_op_return_new(sgl, i->body, i->beg_pc);
  }
  
  return true;
}

struct sgl_op *sgl_fimp_call(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_op *return_pc) {
  if (fimp->cimp) {
    struct sgl_fimp *prev_fimp = sgl->cfimp;
    sgl->cfimp = fimp;
    struct sgl_op *rpc = return_pc->next;
    struct sgl_ls *errs = &sgl->errors;
    if (!fimp->cimp(fimp, sgl, &rpc) || errs->next != errs) { rpc = NULL; }
    sgl->cfimp = prev_fimp;
    return rpc;
  }

  struct sgl_imp *i = &fimp->imp;
  if (!i->beg_pc && !sgl_fimp_compile(fimp, sgl, pos)) { return sgl->end_pc; }
  if (i->end_pc == i->beg_pc) { return return_pc->next; }
  if (i->flags & SGL_IMP_SCOPE) { sgl_beg_scope(sgl, NULL, 0); }
  sgl_beg_call(sgl, i, return_pc);
  return i->beg_pc->next;
}

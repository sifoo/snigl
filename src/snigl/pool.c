#include <stdlib.h>

#include "snigl/config.h"
#include "snigl/pool.h"
#include "snigl/util.h"

struct sgl_pool *sgl_pool_init(struct sgl_pool *pool,
                               sgl_int_t slot_size,
                               sgl_int_t ls_offs) {
  pool->slot_size = slot_size;

#ifdef SGL_USE_POOL
  pool->slab_size = SGL_MIN_SLAB * slot_size;
  pool->ls_offs = ls_offs;
  sgl_ls_init(&pool->slabs);
  sgl_ls_init(&pool->free_slots);
#endif
  
  return pool;
}

struct sgl_pool *sgl_pool_deinit(struct sgl_pool *pool) {
#ifdef SGL_USE_POOL
  sgl_ls_do(&pool->slabs, struct sgl_slab, ls, s) { free(s); }
#endif
  return pool;
}

void *sgl_malloc(struct sgl_pool *pool) {
#ifdef SGL_USE_POOL
  struct sgl_ls *fs = &pool->free_slots;
  
  if (fs->next != fs) {
    void *ptr = (unsigned char *)sgl_ls_pop(&pool->free_slots) - pool->ls_offs;
    return ptr;
  }

  struct sgl_ls *sl = sgl_ls_peek(&pool->slabs);
  struct sgl_slab *s = sgl_baseof(struct sgl_slab, ls, sl);
  
  if (!sl || pool->offs == s->size) {
    s = malloc(sizeof(struct sgl_slab)+pool->slab_size);
    s->size = pool->slab_size;
    pool->slab_size *= 2;
    sgl_ls_push(&pool->slabs, &s->ls);
    pool->offs = 0;
  }

  void *ptr = s->slots + pool->offs;
  pool->offs += pool->slot_size;
  return ptr;
#endif

  return malloc(pool->slot_size);
}

void sgl_free(struct sgl_pool *pool, void *ptr) {
#ifdef SGL_USE_POOL
  void *s = (unsigned char *)ptr + pool->ls_offs;
  sgl_ls_push(&pool->free_slots, s);
#else
  free(ptr);
#endif 
}



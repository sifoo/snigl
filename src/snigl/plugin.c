#include "snigl/plugin.h"

#ifdef SGL_USE_DL

#include <dlfcn.h>
#include <string.h>

#include "snigl/sgl.h"

static bool sgl_plugin_init(void *h, struct sgl *sgl,
                            struct sgl_pos *pos,
                            const char *path) {
  const char *beg = strrchr(path, SGL_PATH_SEP);
  if (beg) { beg++; } else { beg = path; }
  for (sgl_int_t i = 0; *beg && i < 3; beg++, i++);
  const char *end = strchr(beg, '.');

  sgl_int_t len = end - beg; 
  char id[len+6];
  strncpy(id, beg, len);
  strcpy(id+len, "_init");
  void *found = dlsym(h, id);
  bool ok = true;
  
  if (found) {
    bool (*init)(struct sgl *, struct sgl_pos *);
    *(void **) (&init) = found;
    ok = init(sgl, pos);
  }

  return ok;
}

bool sgl_link(struct sgl *sgl, struct sgl_pos *pos, const char *path) {
  dlerror();
  void *h = dlopen(path, RTLD_LAZY | RTLD_GLOBAL);

  if (!h) {
    sgl_error(sgl, pos, sgl_sprintf("Failed linking: %s\n", dlerror()));
    return false;
  }

  if (!sgl_plugin_init(h, sgl, pos, path)) {
    dlclose(h);
    return false;
  }
  
  *(void **)sgl_vec_push(&sgl->links) = h;
  return true;
}

void sgl_deinit_links(struct sgl *sgl) {
  sgl_vec_do(&sgl->links, void *, h) { dlclose(*h); }
}

#endif

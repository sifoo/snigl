#include <assert.h>

#include "snigl/lambda.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/util.h"

static struct sgl_pool *lambda_pool(struct sgl *sgl) {
  return &sgl_baseof(struct sgl_ref_type, type, sgl->Lambda)->pool;
}

struct sgl_lambda *sgl_lambda_new(struct sgl *sgl,
                                  struct sgl_op *beg_pc, struct sgl_op *end_pc,
                                  enum sgl_imp_flags flags) {
  struct sgl_lambda *l = sgl_malloc(lambda_pool(sgl));
  struct sgl_imp *i = &l->imp;
  sgl_imp_init(i, beg_pc->form, flags);
  i->beg_pc = beg_pc;
  i->end_pc = end_pc;
  i->rec_pc = (flags & SGL_IMP_RECALL) ? i->beg_pc->next : NULL;
  l->var_tag = 0;

  if (flags & SGL_IMP_SCOPE) {
    struct sgl_scope *ps = l->parent_scope = sgl_scope(sgl);
    ps->nrefs++;
  } else {
    l->parent_scope = NULL;
  }
  
  l->nrefs = 1;
  return l;
}

void sgl_lambda_deref(struct sgl_lambda *l, struct sgl *sgl) {
  assert(l->nrefs);
  
  if (!--l->nrefs) {
    sgl_imp_deinit(&l->imp, sgl);
    if (l->parent_scope) { sgl_scope_deref(l->parent_scope, sgl); }
    sgl_free(lambda_pool(sgl), l);
  }
}

#include <assert.h>

#include "snigl/curry.h"
#include "snigl/pool.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/util.h"

struct sgl_curry *sgl_curry_new(struct sgl *sgl,
                                struct sgl_val *target,
                                struct sgl_list *args) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->Curry);
  struct sgl_curry *c = sgl_malloc(&rt->pool);
  c->target = target;
  c->args = args;
  c->nrefs = 1;
  return c;
}

void sgl_curry_deref(struct sgl_curry *c, struct sgl *sgl) {
  assert(c->nrefs > 0);
  
  if (!--c->nrefs) {
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->Curry);
    sgl_val_free(c->target, sgl);
    sgl_list_deref(c->args, sgl);
    sgl_free(&rt->pool, c);
  }
}

#include "snigl/enum.h"

struct sgl_enum *sgl_enum_init(struct sgl_enum *e, sgl_int_t i, struct sgl_sym *id) {
  e->i = i;
  e->id = id;
  return e;
}

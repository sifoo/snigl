#include "snigl/call.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"

struct sgl_call *sgl_call_init(struct sgl_call *c,
                               struct sgl *sgl,
                               struct sgl_imp *imp,
                               struct sgl_op *ret_pc) {
  c->imp = imp;
  c->ret_pc = ret_pc;
  if (imp->flags & SGL_IMP_RECALL) { sgl_state_init(&c->state, sgl); }
  return c;
}


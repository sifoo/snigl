#ifndef SNIGL_STR_H
#define SNIGL_STR_H

#include "snigl/config.h"
#include "snigl/int.h"
#include "snigl/iter.h"
#include "snigl/ls.h"

struct sgl_str {
  struct sgl_ls ls;
  sgl_int_t cap, len, nrefs;
  
  union {
    char as_short[SGL_MAX_STR_LEN];
    char *as_long;
  };
};

enum sgl_cmp sgl_cs_ncmp(const char *lhs, const char *rhs, sgl_int_t len);

struct sgl_str *sgl_str_new(struct sgl *sgl, const char *cs, sgl_int_t len);
void sgl_str_deref(struct sgl_str *s, struct sgl *sgl);
struct sgl_str *sgl_str_malloc(struct sgl *sgl);
void sgl_str_free(struct sgl_str *s, struct sgl *sgl);
struct sgl_str *sgl_str_init(struct sgl_str *s, const char *cs, sgl_int_t len);
void sgl_str_deinit(struct sgl_str *s);
void sgl_str_assign(struct sgl_str *s, char *in, sgl_int_t len);
char *sgl_str_cs(struct sgl_str *s);

struct sgl_str_iter {
  struct sgl_iter iter;
  struct sgl_str *str;
  char *beg, *pos;
};

struct sgl_str_iter *sgl_str_iter_new(struct sgl *sgl, struct sgl_str *s);

#endif

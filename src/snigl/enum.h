#ifndef SGL_ENUM_H
#define SGL_ENUM_H

#include "snigl/int.h"

struct sgl_enum {
  sgl_int_t i;
  struct sgl_sym *id;
};

struct sgl_enum *sgl_enum_init(struct sgl_enum *e, sgl_int_t i, struct sgl_sym *id);

#endif

#ifndef SNIGL_BUF_H
#define SNIGL_BUF_H

#include <stdarg.h>

#include "snigl/int.h"
#include "snigl/ls.h"

struct sgl;

struct sgl_buf {
  struct sgl_ls ls;
  sgl_int_t cap, len, nrefs, offs;
  unsigned char *data;
};

struct sgl_buf *sgl_buf_new(struct sgl *sgl);
void sgl_buf_deref(struct sgl_buf *buf, struct sgl *sgl);
struct sgl_buf *sgl_buf_init(struct sgl_buf *buf);
struct sgl_buf *sgl_buf_deinit(struct sgl_buf *buf);
void sgl_buf_grow(struct sgl_buf *buf, sgl_int_t len);
void sgl_buf_putc(struct sgl_buf *buf, unsigned char val);
void sgl_buf_put_int(struct sgl_buf *buf, sgl_int_t val);
void sgl_buf_nputcs(struct sgl_buf *buf, const char *val, sgl_int_t len);
void sgl_buf_putcs(struct sgl_buf *buf, const char *val);
void sgl_buf_printf(struct sgl_buf *buf, const char *spec, ...);
void sgl_buf_vprintf(struct sgl_buf *buf, const char *spec, va_list args1);
void sgl_buf_append(struct sgl_buf *dst, struct sgl_buf *src);
const char *sgl_buf_cs(struct sgl_buf *buf);
struct sgl_str *sgl_buf_str(struct sgl_buf *buf, struct sgl *sgl);

void *sgl_buf_get(struct sgl_buf *b, sgl_int_t len);
                  
#endif

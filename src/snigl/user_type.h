#ifndef SGL_USER_TYPE_H
#define SGL_USER_TYPE_H

#include <stdbool.h>

struct sgl;
struct sgl_op;
struct sgl_pos;
struct sgl_type;
struct sgl_val;

struct sgl_op *sgl_user_type_call_val(struct sgl_val *val,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_op *return_pc,
                                      bool now);

bool sgl_user_type_clone_val(struct sgl_val *src,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_val *dst);

struct sgl_iter *sgl_user_type_iter_val(struct sgl_val *val,
                                        struct sgl *sgl,
                                        struct sgl_type **type);

#endif

#include <assert.h>

#include "snigl/ref.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/sym.h"
#include "snigl/util.h"

struct sgl_ref *sgl_ref_new(struct sgl *sgl, struct sgl_val *val) {
  struct sgl_ref_type *rl = sgl_baseof(struct sgl_ref_type, type, sgl->Ref);
  struct sgl_ref *r = sgl_malloc(&rl->pool);
  r->val = val;
  r->nrefs = 1;
  return r;
}

void sgl_ref_deref(struct sgl_ref *r, struct sgl *sgl) {
  assert(r->nrefs > 0);
  
  if (!--r->nrefs) {
    if (r->val) { sgl_val_free(r->val, sgl); }
    struct sgl_ref_type *rl = sgl_baseof(struct sgl_ref_type, type, sgl->Ref);
    sgl_free(&rl->pool, r);
  }
}

struct sgl_val *sgl_ref_get(struct sgl_ref *r, struct sgl *sgl) {
  return r->val;
}

bool sgl_ref_set(struct sgl_ref *r, struct sgl *sgl, struct sgl_val *val) {
  if (r->val) { sgl_val_free(r->val, sgl); }
  r->val = val;  
  return true;
}

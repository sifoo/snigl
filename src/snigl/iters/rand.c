#include "snigl/iters/rand.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/util.h"
#include "snigl/val.h"

static struct sgl_iter *clone_imp(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_rand_iter
    *ri = sgl_baseof(struct sgl_rand_iter, iter, i),
    *rj = sgl_rand_iter_new(sgl, ri->max);

  rj->gen = ri->gen;
  return &rj->iter;
}

static bool eq_imp(struct sgl_iter *lhs, struct sgl *sgl, struct sgl_iter *rhs) {
  struct sgl_rand_iter
    *l = sgl_baseof(struct sgl_rand_iter, iter, lhs),
    *r = sgl_baseof(struct sgl_rand_iter, iter, rhs);
  
  return l->max == r->max && sgl_prng_eq(&l->gen, &r->gen);
}

static void free_imp(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_ref_type *rt =
    sgl_baseof(struct sgl_ref_type, type, sgl->math_lib.RandIter);
  
  sgl_free(&rt->pool, sgl_baseof(struct sgl_rand_iter, iter, i));
}

static struct sgl_val *next_val_imp(struct sgl_iter *i,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root) {
  struct sgl_rand_iter *ri = sgl_baseof(struct sgl_rand_iter, iter, i);
  struct sgl_val *v = sgl_val_new(sgl, sgl->Int, root);
  sgl_uint_t n = sgl_prng_next(&ri->gen);
  v->as_int = sgl_abs(ri->max ? n % ri->max : n);
  return v;
}

static bool skip_vals_imp(struct sgl_iter *i,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          sgl_int_t nvals) {
  struct sgl_rand_iter *ri = sgl_baseof(struct sgl_rand_iter, iter, i);
  while (nvals--) { sgl_prng_next(&ri->gen); }
  return true;
}

struct sgl_rand_iter *sgl_rand_iter_new(struct sgl *sgl, sgl_uint_t max) {
  struct sgl_ref_type *rt =
    sgl_baseof(struct sgl_ref_type, type, sgl->math_lib.RandIter);
  
  struct sgl_rand_iter *ri = sgl_malloc(&rt->pool);
  ri->max = max;
  
  struct sgl_iter *i = sgl_iter_init(&ri->iter);
  i->clone = clone_imp;
  i->eq = eq_imp;
  i->free = free_imp;
  i->next_val = next_val_imp;
  i->skip_vals = skip_vals_imp;
  return ri;
}

#ifndef SNIGL_ITERS_RAND_H
#define SNIGL_ITERS_RAND_H

#include "snigl/iter.h"
#include "snigl/prng.h"

struct sgl_rand_iter {
  struct sgl_iter iter;
  struct sgl_prng gen;
  sgl_uint_t max;
};

struct sgl_rand_iter *sgl_rand_iter_new(struct sgl *sgl, sgl_uint_t max);

#endif

#include <ctype.h>

#include "snigl/iters/phrase.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/util.h"
#include "snigl/val.h"

static bool is_break(char c) {
  return c == ',' || c == ';' || c == '.' || c == '!' || c == '?';
}

static struct sgl_val *next_val_imp(struct sgl_iter *i,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root) {
  struct sgl_phrase_iter *wi = sgl_baseof(struct sgl_phrase_iter, iter, i);

  char c = 0;
  while ((c = *wi->pos) && (isspace(c) || ispunct(c))) { wi->pos++; }
  if (!c) { return NULL; }
  char *beg = wi->pos;
  
  while ((c = *wi->pos)) {
    if (is_break(c)) { break; }
    wi->pos++;
  }

  if (wi->pos == beg) { return NULL; }
  struct sgl_val *v = sgl_val_new(sgl, sgl->Str, root);
  v->as_str = sgl_str_new(sgl, beg, wi->pos - beg);
  return v;
}

static bool skip_vals_imp(struct sgl_iter *i,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          sgl_int_t nvals) {
  struct sgl_phrase_iter *wi = sgl_baseof(struct sgl_phrase_iter, iter, i);

  while (nvals && *wi->pos) {
    char c = *wi->pos;

    if (is_break(c)) {
      do { c = *(++wi->pos); } while (isspace(c) || ispunct(c));
      nvals--;
    } else {
      wi->pos++;
    }
  }

  if (nvals) {
    sgl_error(sgl, pos, sgl_strdup("Nothing to skip"));
    return false;
  }

  return true;
}

static void free_imp(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_phrase_iter *wi = sgl_baseof(struct sgl_phrase_iter, iter, i);
  sgl_str_deref(wi->in, sgl);
  
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->PhraseIter);
  sgl_free(&rt->pool, wi);
}

struct sgl_phrase_iter *sgl_phrase_iter_new(struct sgl *sgl, struct sgl_str *in) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->PhraseIter);
  struct sgl_phrase_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = next_val_imp;
  i->iter.skip_vals = skip_vals_imp;
  i->iter.free = free_imp;
  i->in = in;
  in->nrefs++;
  i->pos = sgl_str_cs(in);
  return i;
}

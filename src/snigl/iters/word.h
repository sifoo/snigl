#ifndef SNIGL_ITERS_WORD_H
#define SNIGL_ITERS_WORD_H

#include "snigl/iter.h"

struct sgl_word_iter {
  struct sgl_iter iter;
  struct sgl_str *in;
  char *pos;
};

struct sgl_word_iter *sgl_word_iter_new(struct sgl *sgl, struct sgl_str *in);

#endif

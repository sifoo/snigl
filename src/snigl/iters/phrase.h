#ifndef SNIGL_ITERS_PHRASE_H
#define SNIGL_ITERS_PHRASE_H

#include "snigl/iter.h"

struct sgl_phrase_iter {
  struct sgl_iter iter;
  struct sgl_str *in;
  char *pos;
};

struct sgl_phrase_iter *sgl_phrase_iter_new(struct sgl *sgl, struct sgl_str *in);

#endif

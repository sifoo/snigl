#include <stdio.h>

#include "snigl/iters/filter.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/util.h"
#include "snigl/val.h"

static struct sgl_val *next_val_imp(struct sgl_iter *i,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root) {
  struct sgl_filter_iter *fi = sgl_baseof(struct sgl_filter_iter, iter, i);
  struct sgl_ls *stack = sgl_stack(sgl);
  struct sgl_op *pc = sgl->pc;
  
  for (;;) {
    struct sgl_val *v = sgl_iter_next_val(fi->in, sgl, pos, stack);
    if (!v) { break; }
    v = sgl_val_dup(v, sgl, NULL);
    sgl_val_call(fi->fn, sgl, pos, pc, true);
    struct sgl_val *ok = sgl_pop(sgl);
    
    if (!ok) {
      sgl_val_free(v, sgl);
      break;
    }
    
    if (sgl_val_bool(ok, sgl)) {
      if (root) { sgl_ls_push(root, &v->ls); }
      sgl->pc = pc;
      return v;
    }
    
    sgl_val_free(v, sgl);
  }

  sgl->pc = pc;
  return NULL;
}

static bool skip_vals_imp(struct sgl_iter *i,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          sgl_int_t nvals) {
  struct sgl_filter_iter *fi = sgl_baseof(struct sgl_filter_iter, iter, i);

  while (nvals) {
    if (!sgl_iter_next_val(fi->in, sgl, pos, sgl_stack(sgl))) { return false; }
    sgl_val_call(fi->fn, sgl, pos, sgl->end_pc->prev, true);
    struct sgl_val *ok = sgl_pop(sgl);
    if (sgl_val_bool(ok, sgl)) { nvals--; }
    sgl_val_free(ok, sgl);
  }
  
  return true;
}

static void free_imp(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->FilterIter);
  struct sgl_filter_iter *fi = sgl_baseof(struct sgl_filter_iter, iter, i);
  sgl_iter_deref(fi->in, sgl);
  sgl_val_free(fi->fn, sgl);
  sgl_free(&rt->pool, fi);
}

struct sgl_filter_iter *sgl_filter_iter_new(struct sgl *sgl,
                                      struct sgl_iter *in,
                                      struct sgl_val *fn) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->FilterIter);
  struct sgl_filter_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = next_val_imp;
  i->iter.skip_vals = skip_vals_imp;
  i->iter.free = free_imp;
  i->in = in;
  i->fn = fn;
  return i;
}

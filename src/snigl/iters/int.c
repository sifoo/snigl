#include "snigl/iters/int.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/util.h"
#include "snigl/val.h"

static struct sgl_val *next_val_imp(struct sgl_iter *i,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root) {
  struct sgl_int_iter *ii = sgl_baseof(struct sgl_int_iter, iter, i);
  if (++ii->pos == ii->max) { return NULL; }
  struct sgl_val *v = sgl_val_new(sgl, sgl->Int, root);
  v->as_int = ii->pos;
  return v;
}

static bool skip_vals_imp(struct sgl_iter *i,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          sgl_int_t nvals) {
  struct sgl_int_iter *ii = sgl_baseof(struct sgl_int_iter, iter, i);

  if (ii->max - ii->pos < nvals) {
    sgl_error(sgl, pos, sgl_strdup("Iter eof"));
    return false;
  }
  
  ii->pos += nvals;
  return true;
}

static void free_imp(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->IntIter);
  sgl_free(&rt->pool, sgl_baseof(struct sgl_int_iter, iter, i));
}

struct sgl_int_iter *sgl_int_iter_new(struct sgl *sgl, sgl_int_t max) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->IntIter);
  struct sgl_int_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = next_val_imp;
  i->iter.skip_vals = skip_vals_imp;
  i->iter.free = free_imp;
  i->max = max;
  i->pos = -1;
  return i;
}

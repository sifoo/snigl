#ifndef SNIGL_MACRO_H
#define SNIGL_MACRO_H

#include "snigl/def.h"
#include "snigl/ls.h"

struct sgl_macro;

typedef struct sgl_ls *(*sgl_macro_imp_t)(struct sgl_macro *m,
                                          struct sgl_form *form,
                                          struct sgl_ls *root,
                                          struct sgl *sgl);

struct sgl_macro {
  struct sgl_def def;
  struct sgl_ls ls;
  sgl_int_t nargs;
  sgl_macro_imp_t imp;
  struct sgl_ls data;
  void (*deinit)(struct sgl_macro *, struct sgl *);
};

extern struct sgl_def_type SGL_DEF_MACRO;

struct sgl_macro *sgl_macro_init(struct sgl_macro *macro,
                                 struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_lib *lib,
                                 struct sgl_sym *id,
                                 sgl_int_t nargs,
                                 sgl_macro_imp_t imp);

struct sgl_macro *sgl_macro_new(struct sgl *sgl,
                                struct sgl_pos *pos,
                                struct sgl_lib *lib,
                                struct sgl_sym *id,
                                sgl_int_t nargs,
                                sgl_macro_imp_t imp);

struct sgl_ls *sgl_macro_call(struct sgl_macro *m,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl);

void sgl_setup_macros();

#endif

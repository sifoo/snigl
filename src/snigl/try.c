#include "snigl/pool.h"
#include "snigl/sgl.h"
#include "snigl/try.h"

struct sgl_try *sgl_try_init(struct sgl_try *t,
                             struct sgl *sgl,
                             struct sgl_op *end_pc) {
  t->end_pc = end_pc;
  sgl_state_init(&t->state, sgl);
  t->state.tries_len--;
  return t;
}

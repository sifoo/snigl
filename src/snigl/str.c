#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/pool.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/util.h"

enum sgl_cmp sgl_cs_ncmp(const char *lhs, const char *rhs, sgl_int_t len) {
  int res = strncmp(lhs, rhs, len);
  if (res < 0) { return SGL_LT; }
  return (res > 0) ? SGL_GT : SGL_EQ;
}

struct sgl_str *sgl_str_new(struct sgl *sgl, const char *cs, sgl_int_t len) {
  return sgl_str_init(sgl_str_malloc(sgl), cs, len);
}

void sgl_str_deref(struct sgl_str *s, struct sgl *sgl) {
  assert(s->nrefs > 0);
  
  if (!--s->nrefs) {
    if (s->cap >= SGL_MAX_STR_LEN) { free(s->as_long); }
    sgl_str_free(s, sgl);
  }
}

struct sgl_str *sgl_str_malloc(struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->Str);
  return sgl_malloc(&rt->pool);
}

void sgl_str_free(struct sgl_str *s, struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->Str);
  sgl_free(&rt->pool, s);  
}

struct sgl_str *sgl_str_init(struct sgl_str *s, const char *cs, sgl_int_t len) {
  s->cap = s->len = len;
  if (cs) { sgl_str_assign(s, sgl_strndup(cs, len+1), len); }
  s->nrefs = 1;
  return s;
}

void sgl_str_deinit(struct sgl_str *s) {
  if (s->cap >= SGL_MAX_STR_LEN) { free(s->as_long); }
}

void sgl_str_assign(struct sgl_str *s, char *in, sgl_int_t len) {
  if (len < SGL_MAX_STR_LEN) {
    if (len) { strncpy(s->as_short, in, len); }
    s->as_short[len] = 0;
    free(in);
  } else {
    s->as_long = in;
    s->as_long[len] = 0;
  }
  
  s->len = len;  
}

char *sgl_str_cs(struct sgl_str *s) {
  return (s->cap < SGL_MAX_STR_LEN) ? s->as_short : s->as_long;
}

static struct sgl_val *iter_next_val(struct sgl_iter *i,
                                     struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_ls *root) {
  struct sgl_str_iter *si = sgl_baseof(struct sgl_str_iter, iter, i);
  if (++si->pos == si->beg + si->str->len + 1) { return NULL; }
  struct sgl_val *v = sgl_val_new(sgl, sgl->Char, root);
  v->as_char = *si->pos;
  return v;
}

static bool iter_skip_vals(struct sgl_iter *i,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           sgl_int_t nvals) {
  struct sgl_str_iter *si = sgl_baseof(struct sgl_str_iter, iter, i);

  if (si->str->len - (si->pos - si->beg) < nvals) {
    sgl_error(sgl, pos, sgl_strdup("Iter eof"));
    return false;
  }
  
  si->pos += nvals;
  return true;
}

static void iter_free(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_str_iter *si = sgl_baseof(struct sgl_str_iter, iter, i);
  sgl_str_deref(si->str, sgl);
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->StrIter);
  sgl_free(&rt->pool, si);
}

struct sgl_str_iter *sgl_str_iter_new(struct sgl *sgl, struct sgl_str *s) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->StrIter);
  struct sgl_str_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = iter_next_val;
  i->iter.skip_vals = iter_skip_vals;
  i->iter.free = iter_free;
  i->str = s;
  s->nrefs++;
  i->pos = i->beg = sgl_str_cs(s)-1;
  return i;
}


#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/cemit.h"
#include "snigl/form.h"
#include "snigl/list.h"
#include "snigl/macro.h"
#include "snigl/types/struct.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/struct.h"
#include "snigl/sym.h"
#include "snigl/util.h"
#include "snigl/val.h"
#include "snigl/vset.h"

struct sgl_form_type SGL_FORM_EXPR,
  SGL_FORM_ID, SGL_FORM_INDEX,
  SGL_FORM_LIFT, SGL_FORM_LIST, SGL_FORM_LIT,
  SGL_FORM_NOP,
  SGL_FORM_REF,
  SGL_FORM_SOURCE, SGL_FORM_SCOPE, SGL_FORM_STR;

struct sgl_form_type *sgl_form_type_init(struct sgl_form_type *type, const char *id) {
  type->id = id;
  type->cemit_form = NULL;
  type->deinit_form = NULL;
  return type;
}

struct sgl_form *sgl_form_init(struct sgl_form *form,
                               struct sgl_pos *pos,
                               struct sgl_form_type *type,
                               struct sgl_ls *root) {
  form->type = type;
  form->pos = *pos;
  form->cemit_id = -1;
  form->nrefs = 1;
  form->compile = NULL;

  if (root) {
    sgl_ls_push(root, &form->ls);
  } else {
    sgl_ls_init(&form->ls);
  }

  return form;
}

struct sgl_form *sgl_form_deinit(struct sgl_form *form, struct sgl *sgl) {
  if (form->type->deinit_form) { form->type->deinit_form(form, sgl); }
  return form;
}

struct sgl_form *sgl_form_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_form_type *type,
                              struct sgl_ls *root) {
  return sgl_form_init(sgl_malloc(&sgl->form_pool), pos, type, root);
}

void sgl_form_deref(struct sgl_form *form, struct sgl *sgl) {
  assert(form->nrefs > 0);
  if (!--form->nrefs) { sgl_free(&sgl->form_pool, sgl_form_deinit(form, sgl)); }
}

bool sgl_form_compile(struct sgl_form *f, struct sgl *sgl) {
  return f->compile(f, sgl, f->ls.next);
}

struct sgl_list *sgl_form_eval(struct sgl_form *f, struct sgl *sgl) {
  struct sgl_op *eval = sgl_op_eval_beg_new(sgl, f);
  if (!sgl_form_compile(f, sgl)) { return NULL; }
  eval->as_eval_beg.end_pc = sgl_op_eval_end_new(sgl, f);
  struct sgl_list *out = sgl_beg_stack(sgl);
  bool ok = sgl_run_task(sgl, sgl->task, eval->next, sgl->end_pc);
  sgl_end_stack(sgl);

  if (!ok) {
    sgl_list_deref(out, sgl);
    return NULL;
  }
  
  return out;
}

sgl_int_t sgl_form_cemit_id(struct sgl_form *f,
                            struct sgl *sgl,
                            struct sgl_cemit *out) {
  return sgl_form_cemit(f, sgl, "&forms", out) ? f->cemit_id : -1;
}

bool sgl_form_cemit(struct sgl_form *f,
                    struct sgl *sgl,
                    const char *root,
                    struct sgl_cemit *out) {
  if (f->cemit_id != -1) { return true; }

  if (!f->type->cemit_form) {
    sgl_error(sgl, &f->pos,
              sgl_sprintf("cemit not implemented by form: %s", f->type->id));

    return false;
  }

  f->cemit_id = out->form_id++;
  if (!f->type->cemit_form(f, sgl, root, out)) { return false; }
  return true;
}

static void body_deinit(struct sgl_form *form, struct sgl *sgl) {
  sgl_ls_do(&form->as_body.forms, struct sgl_form, ls, f) {
    sgl_form_deref(f, sgl);
  }
}

static bool expr_cemit(struct sgl_form *f,
                       struct sgl *sgl,
                       const char *root,
                       struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "struct sgl_form *form%" SGL_INT " = "
                 "sgl_form_expr_new(sgl, "
                 "sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), "
                 "%s);",
                 f->cemit_id, f->pos.row, f->pos.col, root);

  char *child_root = sgl_sprintf("&form%" SGL_INT "->as_body.forms", f->cemit_id);
  
  sgl_ls_do(&f->as_body.forms, struct sgl_form, ls, cf) {
    if (!sgl_form_cemit(cf, sgl, child_root, out)) {
      free(child_root);
      return false;
    }
  }
  
  free(child_root);
  return true;
}

static struct sgl_ls *expr_compile(struct sgl_form *form,
                                   struct sgl *sgl,
                                   struct sgl_ls *root) {
  if (!sgl_compile(sgl, &form->as_body.forms)) { return NULL; }
  return form->ls.next;
}

struct sgl_form *sgl_form_expr_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_ls *root) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_EXPR, root);
  f->compile = expr_compile;
  sgl_ls_init(&f->as_body.forms);
  return f;
}

static bool id_cemit(struct sgl_form *f,
                     struct sgl *sgl,
                     const char *root,
                     struct sgl_cemit *out) {
  sgl_int_t sym_id = sgl_sym_cemit_id(f->as_id.val, sgl, out);
  
  sgl_cemit_line(out,
                 "struct sgl_form *form%" SGL_INT " = "
                 "sgl_form_id_new(sgl, "
                 "sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), "
                 "%s, "
                 "sym%" SGL_INT ");",
                 f->cemit_id, f->pos.row, f->pos.col, root, sym_id);
  
  return true;
}

static struct sgl_ls *id_compile(struct sgl_form *form,
                                 struct sgl *sgl,
                                 struct sgl_ls *root) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_sym *id = form->as_id.val;
  struct sgl_lib *lib = sgl_lib(sgl);
  char *ids = id->id;
    
  if (ids[0] == '@') {
    sgl_op_get_var_new(sgl, form, sgl_sym(sgl, ids+1));
  } else {
    char *p = NULL;
    
    if (*ids != '.' && (p = strrchr(ids, '/')) && *(p+1)) {
      struct sgl_sym *lib_id = sgl_nsym(sgl, ids, p - ids);
      lib = sgl_find_lib(sgl, pos, lib_id);
      
      if (!lib) {
        sgl_error(sgl, pos, sgl_sprintf("Unknown lib: %s", lib_id->id));
        return NULL;
      }
      
      ids = p+1;
      id = sgl_sym(sgl, ids);
    }

    if (ids[0] == '.' && ids[1] != '.') {
      if (!sgl_op_get_field_new(sgl, form, sgl_sym(sgl, ids+1))) { return NULL; }
    } else {
      struct sgl_def *d = sgl_lib_find(lib, sgl, pos, id);
      
      if (!d) {
        sgl_error(sgl, pos, sgl_sprintf("Unknown def: %s", ids));
        return NULL;
      }
      
      if (d->type == &SGL_DEF_MACRO) {
        return sgl_macro_call(sgl_baseof(struct sgl_macro, def, d), form, root, sgl);
      } else if (d->type == &SGL_DEF_TYPE) {
        struct sgl_type *t = sgl_baseof(struct sgl_type, def, d);
        struct sgl_val *v = sgl_val_new(sgl, sgl_type_meta(t, sgl), NULL);
        v->as_type = t;
        sgl_op_push_new(sgl, form, v);
      } else if (d->type == &SGL_DEF_FUNC) {
        struct sgl_func *func = sgl_baseof(struct sgl_func, def, d);
        
        if (func->nargs) {
          sgl_op_dispatch_new(sgl, form, func, sgl_func_fimp(func));
        } else {
          struct sgl_fimp *fimp = sgl_func_fimp(func);
          if (!sgl_fimp_compile(fimp, sgl, pos)) { return NULL; }
          sgl_op_fimp_call_new(sgl, form, fimp);
        }
      } else {
        sgl_error(sgl, pos, sgl_sprintf("Unknown def: %s", d->id->id));
        return NULL;
      }
    }
  }

  return form->ls.next;
}

struct sgl_form *sgl_form_id_new(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_ls *root,
                                 struct sgl_sym *val) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_ID, root);
  f->compile = id_compile;
  struct sgl_form_id *id = &f->as_id;
  id->val = val;
  return f;
}

static void index_deinit(struct sgl_form *form, struct sgl *sgl) {
  struct sgl_form_index *f = &form->as_index;
  sgl_form_deref(f->target, sgl);
  sgl_ls_do(&f->vals, struct sgl_val, ls, v) { sgl_val_free(v, sgl); }
}

static struct sgl_ls *index_compile(struct sgl_form *form,
                                    struct sgl *sgl,
                                    struct sgl_ls *root) {
  return form->ls.next;
}

struct sgl_form *sgl_form_index_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_INDEX, root);
  f->compile = index_compile;
  sgl_ls_init(&f->as_index.vals);
  return f;
}

static struct sgl_ls *lift_compile(struct sgl_form *form,
                                   struct sgl *sgl,
                                   struct sgl_ls *root) {
  if (!sgl->lift_compile) {
    sgl_error(sgl, &form->pos, sgl_strdup("Missing lift source"));
    return NULL;
  }

  return sgl->lift_compile(form, sgl, root);
}

struct sgl_form *sgl_form_lift_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_ls *root) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_LIFT, root);
  f->compile = lift_compile;
  sgl_ls_init(&f->as_body.forms);
  return f;
}

static struct sgl_ls *list_lift(struct sgl_form *form,
                                  struct sgl *sgl,
                                  struct sgl_ls *root) {
  sgl_op_stack_switch_new(sgl, form, -1, false);
  if (!sgl_compile(sgl, &form->as_body.forms)) { return NULL; }
  sgl_op_stack_switch_new(sgl, form, 1, true);
  return form->ls.next;
}

static struct sgl_ls *list_compile(struct sgl_form *form,
                                   struct sgl *sgl,
                                   struct sgl_ls *root) {
  struct sgl_ls *fs = &form->as_body.forms;

  if (fs->next != fs && fs->next->next == fs) {
    struct sgl_form *vf = sgl_baseof(struct sgl_form, ls, fs->next);
    if (vf->type == &SGL_FORM_ID && vf->as_id.val == sgl_sym(sgl, "..")) {
      sgl_op_stack_list_new(sgl, form);
      return form->ls.next;
    }
  }
  
  sgl_op_list_beg_new(sgl, form);
  bool ok = true;
  sgl_lift(sgl, list_lift) { ok = sgl_compile(sgl, fs); }
  if (!ok) { return NULL; }
  sgl_op_list_end_new(sgl, form);
  return form->ls.next;
}

struct sgl_form *sgl_form_list_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_ls *root) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_LIST, root);
  f->compile = list_compile;
  sgl_ls_init(&f->as_body.forms);
  return f;
}

static bool lit_cemit(struct sgl_form *f,
                      struct sgl *sgl,
                      const char *root,
                      struct sgl_cemit *out) {
  struct sgl_val *v = f->as_lit.val;
  if (!sgl_val_cemit(v, sgl, &f->pos, "v", NULL, out)) { return false; }
  
  sgl_cemit_line(out,
                 "struct sgl_form *form%" SGL_INT " = "
                 "sgl_form_lit_new(sgl, "
                 "sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), "
                 "%s, "
                 "v);",
                 f->cemit_id, f->pos.row, f->pos.col, root);
  
  return true;
}

static void lit_deinit(struct sgl_form *form, struct sgl *sgl) {
  struct sgl_form_lit *f = &form->as_lit;
  sgl_val_free(f->val, sgl);
}

static struct sgl_ls *lit_compile(struct sgl_form *form,
                                  struct sgl *sgl,
                                  struct sgl_ls *root) {
  sgl_op_push_new(sgl, form, sgl_val_dup(form->as_lit.val, sgl, NULL));
  return form->ls.next;
}

struct sgl_form *sgl_form_lit_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root,
                                  struct sgl_val *val) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_LIT, root);
  f->compile = lit_compile;
  f->as_lit.val = val;
  return f;
}

static bool nop_cemit(struct sgl_form *f,
                     struct sgl *sgl,
                     const char *root,
                     struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "struct sgl_form *form%" SGL_INT " = "
                 "sgl_form_nop_new(sgl, "
                 "sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), "
                 "%s);",
                 f->cemit_id, f->pos.row, f->pos.col, root);
  
  return true;
}

static struct sgl_ls *nop_compile(struct sgl_form *form,
                                  struct sgl *sgl,
                                  struct sgl_ls *root) { return form->ls.next; }

struct sgl_form *sgl_form_nop_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_NOP, root);
  f->compile = nop_compile;
  return f;
}

static void ref_deinit(struct sgl_form *form, struct sgl *sgl) {
  sgl_form_deref(form->as_ref.target, sgl);
}

static struct sgl_ls *ref_compile(struct sgl_form *form,
                                  struct sgl *sgl,
                                  struct sgl_ls *root) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *target = form->as_ref.target;

  if (target->type == &SGL_FORM_NOP) {
    struct sgl_op *op = sgl_op_lambda_new(sgl, form);
    op->as_lambda.end_pc = op;
  } else if (target->type == &SGL_FORM_ID) {
    struct sgl_sym *id = target->as_id.val;
    char *ids = id->id;
        
    struct sgl_def *d = sgl_find_def(sgl, pos, id);

    if (!d) {
      sgl_error(sgl, pos, sgl_sprintf("Unknown def: %s", ids));
      return NULL;
    }
    
    if (d->type == &SGL_DEF_MACRO) {
      struct sgl_op *op = sgl_op_lambda_new(sgl, form);
      op->as_lambda.type = sgl->Macro;
      
      struct sgl_ls *res =
        sgl_macro_call(sgl_baseof(struct sgl_macro, def, d), form, root, sgl);
      
      if (!res) { return NULL; }
      
      if (sgl->end_pc->prev == op) {
        op->as_lambda.end_pc = op;
      } else {
        op->as_lambda.end_pc = sgl_op_return_new(sgl, form, op);
      }

      return res;
    } else if (d->type == &SGL_DEF_FUNC) {
      struct sgl_func *f = sgl_baseof(struct sgl_func, def, d);
      struct sgl_val *v = sgl_val_new(sgl, sgl->Func, NULL);
      v->as_func = f;
      sgl_op_push_new(sgl, form, v);
    } else {
      if (!sgl_form_compile(target, sgl)) { return NULL; }
      sgl_op_ref_new(sgl, form);
    }    
  } else if (target->type == &SGL_FORM_EXPR || target->type == &SGL_FORM_SCOPE) {
    struct sgl_ls *body = &target->as_body.forms;

    if (body->next == body) {
      struct sgl_op *op = sgl_op_lambda_new(sgl, form);
      op->as_lambda.end_pc = op;
    } else {
      struct sgl_op *op = sgl_op_lambda_new(sgl, form);
      if (!sgl_form_compile(target, sgl)) { return NULL; }
      op->as_lambda.end_pc = sgl_op_return_new(sgl, form, op);
    }
  } else {
    if (!sgl_form_compile(target, sgl)) { return NULL; }
    sgl_op_ref_new(sgl, form);
  }

  return form->ls.next;
}

struct sgl_form *sgl_form_ref_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root,
                                  struct sgl_form *target) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_REF, root);
  f->compile = ref_compile;
  f->as_ref.target = target;
  return f;
}

static struct sgl_ls *scope_compile(struct sgl_form *form,
                                    struct sgl *sgl,
                                    struct sgl_ls *root) {
  struct sgl_op *beg = sgl_op_scope_beg_new(sgl, form);
  if (!sgl_compile(sgl, &form->as_body.forms)) { return NULL; }
  beg->as_scope_beg.end_pc = sgl_op_scope_end_new(sgl, form, beg);
  return form->ls.next;
}

struct sgl_form *sgl_form_scope_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_SCOPE, root);
  f->compile = scope_compile;
  sgl_ls_init(&f->as_body.forms);
  return f;
}

static struct sgl_ls *source_compile(struct sgl_form *form,
                                    struct sgl *sgl,
                                    struct sgl_ls *root) {
  struct sgl_form_source *sf = &form->as_source;
  if (!sgl_form_compile(sf->body, sgl)) { return NULL; }
  struct sgl_val *v = sgl_val_new(sgl, sgl->Str, NULL);
  v->as_str = sgl_str_new(sgl, sf->str, strlen(sf->str));
  sgl_op_push_new(sgl, form, v);
  return form->ls.next;
}

static void source_deinit(struct sgl_form *form, struct sgl *sgl) {
  struct sgl_form_source *sf = &form->as_source;
  sgl_form_deref(sf->body, sgl);
  free(sf->str);
}

struct sgl_form *sgl_form_source_new(struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_ls *root,
                                     struct sgl_form *body,
                                     char *str) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_SOURCE, root);
  f->compile = source_compile;
  f->as_source = (struct sgl_form_source){.body = body, .str = str};
  return f;
}


static struct sgl_ls *str_compile(struct sgl_form *form,
                                  struct sgl *sgl,
                                  struct sgl_ls *root) {
  sgl_op_str_beg_new(sgl, form);

  sgl_ls_do(&form->as_body.forms, struct sgl_form, ls, f) {
    if (!sgl_form_compile(f, sgl)) { return NULL; }
    sgl_op_str_put_new(sgl, form);
  }
  
  sgl_op_str_end_new(sgl, form);
  return form->ls.next;
}

struct sgl_form *sgl_form_str_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_ls *root) {
  struct sgl_form *f = sgl_form_new(sgl, pos, &SGL_FORM_STR, root);
  f->compile = str_compile;
  sgl_ls_init(&f->as_body.forms);
  return f;
}

struct sgl_ls *sgl_forms_deref(struct sgl_ls *root, struct sgl *sgl) {
  sgl_ls_do(root, struct sgl_form, ls, f) { sgl_form_deref(f, sgl); }
  return root;
}

void sgl_setup_forms() {
  sgl_form_type_init(&SGL_FORM_EXPR, "expr");
  SGL_FORM_EXPR.cemit_form = expr_cemit;
  SGL_FORM_EXPR.deinit_form = body_deinit;

  sgl_form_type_init(&SGL_FORM_ID, "id");
  SGL_FORM_ID.cemit_form = id_cemit;

  sgl_form_type_init(&SGL_FORM_INDEX, "index");
  SGL_FORM_INDEX.deinit_form = index_deinit;

  sgl_form_type_init(&SGL_FORM_LIFT, "lift");
  SGL_FORM_LIFT.deinit_form = body_deinit;

  sgl_form_type_init(&SGL_FORM_LIST, "list");
  SGL_FORM_LIST.deinit_form = body_deinit;

  sgl_form_type_init(&SGL_FORM_LIT, "lit");
  SGL_FORM_LIT.cemit_form = lit_cemit;
  SGL_FORM_LIT.deinit_form = lit_deinit;

  sgl_form_type_init(&SGL_FORM_NOP, "nop");
  SGL_FORM_NOP.cemit_form = nop_cemit;

  sgl_form_type_init(&SGL_FORM_REF, "ref");
  SGL_FORM_REF.deinit_form = ref_deinit;

  sgl_form_type_init(&SGL_FORM_SOURCE, "source");
  SGL_FORM_SOURCE.deinit_form = source_deinit;

  sgl_form_type_init(&SGL_FORM_SCOPE, "scope");
  SGL_FORM_SCOPE.deinit_form = body_deinit;

  sgl_form_type_init(&SGL_FORM_STR, "str");
  SGL_FORM_STR.deinit_form = body_deinit;
}

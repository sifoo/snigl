#ifndef SNIGL_CALL_H
#define SNIGL_CALL_H

#include <stdbool.h>

#include "snigl/pos.h"
#include "snigl/state.h"

struct sgl;

struct sgl_call {
  struct sgl_imp *imp;
  struct sgl_op *ret_pc;
  struct sgl_state state;
};

struct sgl_call *sgl_call_init(struct sgl_call *c,
                               struct sgl *sgl,
                               struct sgl_imp *imp,
                               struct sgl_op *ret_pc);

#endif

#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/user_type.h"

struct sgl_op *sgl_user_type_call_val(struct sgl_val *val,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_op *return_pc,
                                      bool now) {
  if (sgl->cfimp == sgl->call_fimp) {
    sgl_val_dup(val, sgl, sgl_stack(sgl));
    return return_pc->next;
  }

  return sgl_func_call(sgl->call_fimp->func, sgl, true, NULL);
}
  
bool sgl_user_type_clone_val(struct sgl_val *src,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_val *dst) {
  if (sgl->cfimp != sgl->clone_fimp &&
      sgl_func_call(sgl->clone_fimp->func, sgl, true, src)) {
    struct sgl_val *dv = sgl_pop(sgl);

    if (!dv) {
      sgl_error(sgl, pos, sgl_strdup("Missing cloned value"));
      return false;
    }
    
    sgl_val_deinit(dst, sgl);
    *dst = *dv;
    return true;
  }

  return false;
}

struct sgl_iter *sgl_user_type_iter_val(struct sgl_val *val,
                                        struct sgl *sgl,
                                        struct sgl_type **type) {
  if (sgl->cfimp != sgl->iter_fimp &&
      sgl_func_call(sgl->iter_fimp->func, sgl, true, val)) {
    struct sgl_val *i = sgl_pop(sgl);
    if (!i) { return NULL; }
    
    if (!sgl_derived(i->type, sgl->Iter)) {
      sgl_error(sgl, sgl_pos(sgl),
                sgl_sprintf("Expected Iter, was: %s", i->type->def.id->id));
      
      return NULL;
    }
    
    if (type) { *type = i->type; }
    struct sgl_iter *it = i->as_iter;
    it->nrefs++;
    sgl_val_free(i, sgl);
    return it;
  }

  sgl_error(sgl, sgl_pos(sgl),
            sgl_sprintf("Missing iter: %s", val->type->def.id->id));
  
  return NULL;
}

#include <stddef.h>

#include "snigl/ls.h"

struct sgl_ls *sgl_ls_init(struct sgl_ls *ls) {
  ls->prev = ls->next = ls;
  return ls;
}

bool sgl_ls_null(struct sgl_ls *ls) { return ls->next == ls; }

sgl_int_t sgl_ls_len(struct sgl_ls *ls) {
  sgl_int_t l = 0;
  for (struct sgl_ls *i = ls->next; i != ls; i = i->next, l++);
  return l;
}

void sgl_ls_assign(struct sgl_ls *ls, struct sgl_ls *src) {
  ls->next = (src->next == src) ? ls : src->next;
  ls->next->prev = ls;
  ls->prev = (src->prev == src) ? ls : src->prev;
  ls->prev->next = ls;
}

void sgl_ls_append(struct sgl_ls *ls, struct sgl_ls *next) {
  next->prev = ls;
  next->next = ls->next;
  ls->next = next->next->prev = next;
}

void sgl_ls_insert(struct sgl_ls *ls, struct sgl_ls *prev) {
  prev->next = ls;
  prev->prev = ls->prev;
  ls->prev = prev->prev->next = prev;
}

void sgl_ls_delete(struct sgl_ls *ls) {
  ls->prev->next = ls->next;
  ls->next->prev = ls->prev;
}

void sgl_ls_move(struct sgl_ls *dst, struct sgl_ls *src) {
  src->prev->next = src->next;
  src->next->prev = src->prev;
  dst->prev->next = src;
  src->prev = dst->prev;
  src->next = dst;
  dst->prev = src;
}

void sgl_ls_swap(struct sgl_ls *ls) {
  struct sgl_ls *prev = ls->prev;
  prev->prev->next = ls;
  ls->next->prev = prev;
  ls->prev = prev->prev;
  prev->prev = ls;
  prev->next = ls->next;
  ls->next = prev;
}

struct sgl_ls *sgl_ls_peek(struct sgl_ls *root) {
  struct sgl_ls *top = root->prev;
  return (top == root) ? NULL : top;
}

struct sgl_ls *sgl_ls_pop(struct sgl_ls *root) {
  struct sgl_ls *top = root->prev;
  if (top == root) { return NULL; }
  sgl_ls_delete(top);
  return top;
}

struct sgl_ls *sgl_ls_find(struct sgl_ls *root,
                           sgl_ls_cmp_t cmp,
                           const void *key,
                           void *data,
                           bool *ok) {
  for (struct sgl_ls *i = root->next; i != root; i = i->next) {
    enum sgl_cmp res = cmp(i, key, data);
    if (res == SGL_CMP_FAIL) { return NULL; }
      
    if (res != SGL_LT) {
      if (ok && res == SGL_EQ) { *ok = true; }
      return i;
    }
  }

  return root;
}

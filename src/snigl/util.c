#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/util.h"

bool sgl_is_little_endian() {
  uint16_t x = 1;
  return *(uint8_t *)&x;
}

sgl_int_t sgl_abs(sgl_int_t x) { return (x < 0) ? -x : x; }
sgl_int_t sgl_min(sgl_int_t x, sgl_int_t y) { return (x <= y) ? x : y; }
sgl_int_t sgl_max(sgl_int_t x, sgl_int_t y) { return (x >= y) ? x : y; }

sgl_int_t sgl_pow(sgl_int_t x, sgl_int_t exp) {
  int64_t y = 1;
  while (exp--) { y *= x; }
  return y;
}                    

char *sgl_strndup(const char *in, sgl_int_t len) {
  char *out = malloc(len+1);
  out[len] = 0;
  return strncpy(out, in, len);
}

char *sgl_strdup(const char *in) { return sgl_strndup(in, strlen(in)); }

sgl_uint_t sgl_hash_cs(const char *in) {
  sgl_uint_t h = 0;
  const char *src = in;
  char *dst = (char *)&h, *dst_beg = dst, *dst_end = dst + sizeof(h);
  
  while (*src) {
    *dst++ ^= *src++;
    if (dst == dst_end) { dst = dst_beg; }
  }

  return h;
}

sgl_uint_t sgl_hash_ncs(const char *in, sgl_int_t len) {
  sgl_uint_t h = 0;
  const char *src = in;
  char *dst = (char *)&h, *dst_beg = dst, *dst_end = dst + sizeof(h);
  
  while (len--) {
    *dst++ ^= *src++;
    if (dst == dst_end) { dst = dst_beg; }
  }

  return h;
}

char *sgl_sprintf(const char *spec, ...) {
  va_list args1, args2;
  va_start(args1, spec);
  va_copy(args2, args1);

  sgl_int_t len = vsnprintf(NULL, 0, spec, args1);
  va_end(args1);
  
  char *out = malloc(len+1);
  out[len] = 0;
  vsnprintf(out, len+1, spec, args2);
  va_end(args2);
  return out;
}

char *sgl_readln(FILE *in, sgl_int_t *len) {
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  char c = 0;  
  while ((c = fgetc(in)) && c != '\n') { sgl_buf_putc(&buf, c); }
  if (len) { *len = buf.len; }
  char *out = buf.len ? (char *)buf.data : malloc(1);
  out[buf.len] = 0;
  return out;
}

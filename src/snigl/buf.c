#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/util.h"

struct sgl_buf *sgl_buf_new(struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->Buf);
  return sgl_buf_init(sgl_malloc(&rt->pool));
}

void sgl_buf_deref(struct sgl_buf *buf, struct sgl *sgl) {
  assert(buf->nrefs > 0);
  
  if (!--buf->nrefs) {
    sgl_buf_deinit(buf);
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->Buf);
    sgl_free(&rt->pool, buf);
  }
}

struct sgl_buf *sgl_buf_init(struct sgl_buf *buf) {
  buf->cap = buf->len = buf->offs = 0;
  buf->data = NULL;
  buf->nrefs = 1;
  return buf;
}

struct sgl_buf *sgl_buf_deinit(struct sgl_buf *buf) {
  if (buf->data) { free(buf->data); }
  return buf;
}

void sgl_buf_grow(struct sgl_buf *buf, sgl_int_t len) {
  if (buf->cap < len+1) {
    if (buf->cap) {
      while (buf->cap < len+1) { buf->cap *= 2; }
    } else {
      buf->cap = len+1;
    }
    
    buf->data = realloc(buf->data, buf->cap);
  }
}

void sgl_buf_putc(struct sgl_buf *buf, unsigned char val) {
  sgl_buf_grow(buf, buf->len+1);
  buf->data[buf->len++] = val;
}

void sgl_buf_put_int(struct sgl_buf *buf, sgl_int_t val) {
  char out[SGL_INT_MAX_LEN];
  sgl_int_t len = sgl_int_str(val, out, 10);
  sgl_buf_grow(buf, buf->len+len);
  strcpy((char *)buf->data+buf->len, out);
  buf->len += len;
}

void sgl_buf_nputcs(struct sgl_buf *buf, const char *val, sgl_int_t len) {
  sgl_buf_grow(buf, buf->len+len);
  strncpy((char *)buf->data+buf->len, val, len);
  buf->len += len;
}

void sgl_buf_putcs(struct sgl_buf *buf, const char *val) {
  sgl_buf_nputcs(buf, val, strlen(val));
}

const char *sgl_buf_cs(struct sgl_buf *buf) {
  if (!buf->len) { return ""; }
  buf->data[buf->len] = 0;
  return (const char *)buf->data;
}

void sgl_buf_printf(struct sgl_buf *buf, const char *spec, ...) {
  va_list args;
  va_start(args, spec);
  sgl_buf_vprintf(buf, spec, args);
  va_end(args);
}

void sgl_buf_vprintf(struct sgl_buf *buf, const char *spec, va_list args1) {
  va_list args2;
  va_copy(args2, args1);
  sgl_buf_grow(buf, buf->len + strlen(spec));
  
  sgl_int_t
    buf_len = buf->cap - buf->len,
    len = vsnprintf((char *)buf->data+buf->len, buf_len, spec, args1);

  if (len >= buf_len) {
    sgl_buf_grow(buf, buf->len + len);
    assert(vsnprintf((char *)buf->data+buf->len, len + 1, spec, args2) == len);
  }

  va_end(args2);
  buf->len += len;
}

void sgl_buf_append(struct sgl_buf *dst, struct sgl_buf *src) {
  sgl_buf_grow(dst, dst->len + src->len);
  memcpy(dst->data + dst->len, src->data, src->len);
  dst->len += src->len;
  src->len = 0;
}

struct sgl_str *sgl_buf_str(struct sgl_buf *b, struct sgl *sgl) {
  return sgl_str_new(sgl, sgl_buf_cs(b), b->len);
}

void *sgl_buf_get(struct sgl_buf *b, sgl_int_t len) {
  if (b->offs + len > b->len) { return NULL; }
  void *p = b->data + b->offs;
  b->offs += len;
  return p;
}

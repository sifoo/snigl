#ifndef SNIGL_RGB_H
#define SNIGL_RGB_H

#include <stdint.h>

struct sgl_rgb {
  int32_t r, g, b;
};

struct sgl_rgb *sgl_rgb_init(struct sgl_rgb *rgb, int32_t r, int32_t g, int32_t b);

#endif

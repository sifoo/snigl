#ifndef SNIGL_VEC_H
#define SNIGL_VEC_H

#include "snigl/int.h"

#define _sgl_vec_do(mv, mtype, mvar, mi)                            \
  sgl_int_t mi = 0;                                                 \
  for (mtype *mvar = (void *)mv->vals; mi < mv->len; mi++, mvar++)  \

#define sgl_vec_do(mv, mtype, mvar)             \
  _sgl_vec_do((mv), mtype, mvar, SGL_CUID(mi))  \

#define _sgl_vec_rdo(mv, mtype, mvar)                                   \
  for (mtype *mvar = mv->vals                                           \
         ? (mtype *)(mv->vals + mv->val_size * (mv->len - 1))           \
         : NULL;                                                        \
       mv->vals && mvar >= (mtype *)mv->vals;                           \
       mvar = (mtype *)((uint8_t *)mvar - mv->val_size))                \
    
#define sgl_vec_rdo(mv, mtype, mvar)            \
  _sgl_vec_rdo((mv), mtype, mvar)               \
  
struct sgl_vec {
  sgl_int_t val_size, cap, len;
  unsigned char *vals;
};

struct sgl_vec *sgl_vec_init(struct sgl_vec *v, sgl_int_t val_size);
struct sgl_vec *sgl_vec_deinit(struct sgl_vec *v);
void sgl_vec_grow(struct sgl_vec *v, sgl_int_t len);
void *sgl_vec_beg(struct sgl_vec *v);
void *sgl_vec_end(struct sgl_vec *v);
void *sgl_vec_get(struct sgl_vec *v, sgl_int_t i);
void *sgl_vec_push(struct sgl_vec *v);
void *sgl_vec_peek(struct sgl_vec *v);
void *sgl_vec_pop(struct sgl_vec *v);
void *sgl_vec_insert(struct sgl_vec *v, sgl_int_t i);
void sgl_vec_delete(struct sgl_vec *v, sgl_int_t i);

#endif

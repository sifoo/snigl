#ifndef SNIGL_PRNG_H
#define SNIGL_PRNG_H

#include <stdbool.h>
#include "snigl/int.h"

struct sgl_prng {
  uint64_t state[2];
};

sgl_uint_t sgl_prng_seed();
struct sgl_prng *sgl_prng_init(struct sgl_prng *p, sgl_uint_t seed);
bool sgl_prng_eq(struct sgl_prng *p1, struct sgl_prng *p2);
sgl_uint_t sgl_prng_next(struct sgl_prng *p);

#endif

#include <assert.h>
#include <ctype.h>

#include <errno.h>
#include <stdatomic.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include "snigl/config.h"

#include "snigl/call.h"
#include "snigl/cemit.h"
#include "snigl/error.h"
#include "snigl/file.h"
#include "snigl/form.h"
#include "snigl/func.h"
#include "snigl/iters/filter.h"
#include "snigl/iters/int.h"
#include "snigl/iters/map.h"
#include "snigl/io.h"
#include "snigl/io_thread.h"
#include "snigl/libs/abc.h"
#include "snigl/libs/enum.h"
#include "snigl/libs/hash_map.h"
#include "snigl/libs/list.h"
#include "snigl/libs/str.h"
#include "snigl/list.h"
#include "snigl/macro.h"
#include "snigl/mem.h"
#include "snigl/parse.h"
#include "snigl/plugin.h"
#include "snigl/pmacro.h"
#include "snigl/pos.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/str.h"
#include "snigl/struct.h"
#include "snigl/sym.h"
#include "snigl/trace.h"
#include "snigl/types/enum.h"
#include "snigl/types/struct.h"
#include "snigl/try.h"
#include "snigl/val.h"
#include "snigl/util.h"
#include "snigl/vec.h"

const sgl_int_t SGL_VERSION[3] = {0, 9, 12};

void sgl_setup() {
  sgl_setup_forms();
  sgl_setup_funcs();
  sgl_setup_macros();
  sgl_setup_ops();
  sgl_setup_pmacros();
  sgl_setup_pos();
  sgl_setup_types();
}

static enum sgl_cmp libs_cmp(struct sgl_ls *lhs, const void *rhs, void *_) {
  struct sgl_sym *lsym = sgl_baseof(struct sgl_lib, ls, lhs)->id;
  return sgl_sym_cmp(lsym, rhs);
}

static sgl_uint_t syms_fn(void *x, void *data) { return sgl_hash_cs(x); }
static bool syms_is(void *x, void *y, void *data) { return strcmp(x, y) == 0; }

struct sgl *sgl_init(struct sgl *sgl) {
  sgl->lift_compile = NULL;
  sgl->debug = false;
  sgl->trace = -1;

  sgl_pool_init(&sgl->enum_type_pool,
                sizeof(struct sgl_enum_type),
                offsetof(struct sgl_enum_type, type) +
                offsetof(struct sgl_type, ls));

  sgl_pool_init(&sgl->error_pool,
                sizeof(struct sgl_error),
                offsetof(struct sgl_error, ls));

  sgl_pool_init(&sgl->field_pool,
                sizeof(struct sgl_field),
                offsetof(struct sgl_field, ls));

  sgl_pool_init(&sgl->fimp_pool,
                sizeof(struct sgl_fimp),
                offsetof(struct sgl_fimp, ls));

  sgl_pool_init(&sgl->form_pool,
                sizeof(struct sgl_form),
                offsetof(struct sgl_form, ls));

  sgl_pool_init(&sgl->hash_node_pool,
                sizeof(struct sgl_hash_node),
                offsetof(struct sgl_hash_node, ls));

  sgl_pool_init(&sgl->lib_pool,
                sizeof(struct sgl_lib),
                offsetof(struct sgl_lib, ls));

  sgl_pool_init(&sgl->macro_pool,
                sizeof(struct sgl_macro),
                offsetof(struct sgl_macro, ls));

  sgl_pool_init(&sgl->mem_pool,
                sizeof(struct sgl_mem),
                offsetof(struct sgl_mem, ls));

  sgl_pool_init(&sgl->pmacro_pool,
                sizeof(struct sgl_pmacro),
                offsetof(struct sgl_pmacro, ls));

  sgl_pool_init(&sgl->op_pool,
                sizeof(struct sgl_op),
                offsetof(struct sgl_op, ls));

  sgl_pool_init(&sgl->ref_type_pool,
                sizeof(struct sgl_ref_type),
                offsetof(struct sgl_ref_type, type) +
                offsetof(struct sgl_type, ls));

  sgl_pool_init(&sgl->scope_pool,
                sizeof(struct sgl_scope),
                offsetof(struct sgl_scope, ls));

  sgl_pool_init(&sgl->struct_type_pool,
                sizeof(struct sgl_struct_type),
                offsetof(struct sgl_struct_type, ref_type) +
                offsetof(struct sgl_ref_type, type) +
                offsetof(struct sgl_type, ls));

  sgl_pool_init(&sgl->sym_pool,
                sizeof(struct sgl_sym),
                offsetof(struct sgl_sym, ls));

  sgl_pool_init(&sgl->task_pool,
                sizeof(struct sgl_task),
                offsetof(struct sgl_task, ls));

  sgl_pool_init(&sgl->trace_pool,
                sizeof(struct sgl_trace),
                offsetof(struct sgl_trace, ls));

  sgl_pool_init(&sgl->type_pool,
                sizeof(struct sgl_type),
                offsetof(struct sgl_type, ls));

  sgl_pool_init(&sgl->val_pool,
                sizeof(struct sgl_val),
                offsetof(struct sgl_val, ls));

  struct sgl_pos *pos = &SGL_NULL_POS;
  sgl_hash_init(&sgl->syms);
  sgl->syms.fn = syms_fn;
  sgl->syms.is = syms_is;
  sgl->nsyms = 0;
  
  sgl_ls_init(&sgl->errors);
  sgl_ls_init(&sgl->ops);
  
  sgl->end_pc = sgl_nop_new(sgl, NULL);
  sgl->end_pc->next = sgl->end_pc;
  sgl->beg_pc = sgl_nop_new(sgl, NULL);
  sgl->beg_pc->prev = sgl->beg_pc;
  sgl->pc = NULL;
  sgl->cfimp = NULL;
  sgl->ntasks = 0;
  sgl->type_tag = sgl->var_tag = 0;

  sgl_vec_init(&sgl->load_paths, sizeof(char *));
  sgl->load_path = sgl_strdup("?");
  *(char **)sgl_vec_push(&sgl->load_paths) = sgl->load_path;
  
  sgl_lset_init(&sgl->libs, &libs_cmp);
  sgl_lib_init(&sgl->home_lib, sgl, pos, sgl_sym(sgl, "home"));
  sgl_add_lib(sgl, pos, &sgl->home_lib);

  sgl_ls_init(&sgl->tasks);
  
  sgl->task =
    sgl_task_init(&sgl->main_task, sgl, &sgl->home_lib, NULL, NULL, sgl->end_pc);

  sgl_scope_init(&sgl->main_scope, sgl, NULL, 0);
  sgl->main_scope.nrefs++;
  sgl_ls_push(&sgl->main_task.scopes, &sgl->main_scope.ls);
  sgl_buf_init(&sgl->stdout);
  memset(sgl->loaders, 0, sizeof(sgl->loaders));
  
  sgl->abc_lib = sgl_abc_lib_new(sgl, pos);
  sgl_add_lib(sgl, pos, sgl->abc_lib);

  sgl->enum_lib = sgl_enum_lib_new(sgl, pos);
  sgl_add_lib(sgl, pos, sgl->enum_lib);

  sgl->hash_map_lib = sgl_hash_map_lib_new(sgl, pos);
  sgl_add_lib(sgl, pos, sgl->hash_map_lib);

  sgl->list_lib = sgl_list_lib_new(sgl, pos);
  sgl_add_lib(sgl, pos, sgl->list_lib);

  sgl_add_lib(sgl, pos, sgl_math_lib_init(&sgl->math_lib, sgl));

  sgl->str_lib = sgl_str_lib_new(sgl, pos);
  sgl_add_lib(sgl, pos, sgl->str_lib);

  sgl_vec_init(&sgl->vars, sizeof(struct sgl_var));
  
#ifdef SGL_USE_DL
  sgl_vec_init(&sgl->links, sizeof(void *));
#endif
  
#ifdef SGL_USE_ASYNC
  sgl_pool_init(&sgl->io_pool,
                sizeof(struct sgl_io),
                offsetof(struct sgl_io, ls));
  
  sgl_pool_init(&sgl->io_thread_pool,
                sizeof(struct sgl_io_thread),
                offsetof(struct sgl_io_thread, ls));  

  sgl->io_depth = sgl->sync_depth = 0;
  sgl->nio_done = 0;
  sgl->max_io_threads = 4;
  sgl->nio_threads = 0;
  sgl->nio_idle_threads = 0;

  sgl_ls_init(&sgl->io_threads);
  sgl_ls_init(&sgl->io_queue);
  
  pthread_cond_init(&sgl->io_done, NULL);
  pthread_mutex_init(&sgl->io_lock, NULL);
#endif

  return sgl;
}

#ifdef SGL_USE_ASYNC

void sgl_stop_io_threads(struct sgl *sgl) {
  pthread_mutex_lock(&sgl->io_lock);
  
  sgl_ls_do(&sgl->io_threads, struct sgl_io_thread, ls, t) {
    pthread_mutex_unlock(&sgl->io_lock);
    sgl_io_thread_stop(t);
    pthread_mutex_lock(&sgl->io_lock);
    sgl_io_thread_free(t, sgl);
  }

  sgl->nio_threads = 0;
  sgl->max_io_threads = 0;
  sgl_ls_init(&sgl->io_threads);
  pthread_mutex_unlock(&sgl->io_lock);
}

#endif

void sgl_init_args(struct sgl *sgl, sgl_int_t argc, const char *argv[]) {
  struct sgl_ls *root = &sgl->argv.as_list->root;
  
  for (const char **a = argv; argc; argc--, a++) {
    sgl_val_new(sgl, sgl->Str, root)->as_str = sgl_str_new(sgl, *a, strlen(*a));
  }
}

struct sgl *sgl_deinit(struct sgl *sgl) {
#ifdef SGL_USE_ASYNC
  sgl_stop_io_threads(sgl);
#endif
  
  sgl_ls_do(&sgl->ops, struct sgl_op, ls, o) {
    sgl_free(&sgl->op_pool, sgl_op_deinit(o, sgl));
  }

  sgl_scope_deinit(&sgl->main_scope, sgl);
  sgl_ls_do(&sgl->tasks, struct sgl_task, ls, t) { sgl_task_free(t, sgl); }
  
  if (sgl->stdout.len) { fwrite(sgl->stdout.data, sgl->stdout.len, 1, stdout); }
  sgl_buf_deinit(&sgl->stdout);
  
  sgl_ls_do(&sgl->errors, struct sgl_error, ls, e) { sgl_error_free(e, sgl); }
  sgl_ls_do(&sgl->libs.root, struct sgl_lib, ls, l) { sgl_lib_deinit1(l, sgl); }
  sgl_ls_do(&sgl->libs.root, struct sgl_lib, ls, l) { sgl_lib_deinit2(l, sgl); }

  sgl_ls_do(&sgl->syms.root, struct sgl_hash_node, ls, n) {
    sgl_free(&sgl->sym_pool, sgl_sym_deinit(n->val));
  }

  sgl_vec_do(&sgl->vars, struct sgl_var, v) {
    if (v->id) { sgl_val_free(v->val, sgl); }
  }
  
  sgl_vec_deinit(&sgl->vars);
  sgl_hash_deinit(&sgl->syms, sgl);

  sgl_pool_deinit(&sgl->enum_type_pool);
  sgl_pool_deinit(&sgl->error_pool);
  sgl_pool_deinit(&sgl->field_pool);
  sgl_pool_deinit(&sgl->fimp_pool);
  sgl_pool_deinit(&sgl->form_pool);
  sgl_pool_deinit(&sgl->hash_node_pool);
  sgl_pool_deinit(&sgl->lib_pool);
  sgl_pool_deinit(&sgl->macro_pool);
  sgl_pool_deinit(&sgl->mem_pool);
  sgl_pool_deinit(&sgl->op_pool);
  sgl_pool_deinit(&sgl->pmacro_pool);
  sgl_pool_deinit(&sgl->ref_type_pool);
  sgl_pool_deinit(&sgl->scope_pool);
  sgl_pool_deinit(&sgl->struct_type_pool);
  sgl_pool_deinit(&sgl->sym_pool);
  sgl_pool_deinit(&sgl->task_pool);
  sgl_pool_deinit(&sgl->trace_pool);
  sgl_pool_deinit(&sgl->type_pool);
  sgl_pool_deinit(&sgl->val_pool);

  sgl_vec_do(&sgl->load_paths, char *, p) { free(*p); }
  sgl_vec_deinit(&sgl->load_paths);
  
#ifdef SGL_USE_DL
  sgl_deinit_links(sgl);
  sgl_vec_deinit(&sgl->links);
#endif
  
#ifdef SGL_USE_ASYNC
  sgl_pool_deinit(&sgl->io_pool);
  sgl_pool_deinit(&sgl->io_thread_pool);
  pthread_cond_destroy(&sgl->io_done);
  pthread_mutex_destroy(&sgl->io_lock);
#endif

  return sgl;
}

struct sgl_error *sgl_error(struct sgl *sgl,
                            struct sgl_pos *pos,
                            char *msg) {
  struct sgl_error *e = sgl_error_init(sgl_malloc(&sgl->error_pool),
                                       pos, msg, NULL, &sgl->errors);

  if (sgl->debug) {
    struct sgl_buf buf;
    sgl_buf_init(&buf);
    sgl_error_dump(e, &buf);
    fputs(sgl_buf_cs(&buf), stderr);
    abort();
  }
    
  return e;
}

struct sgl_op *sgl_throw(struct sgl *sgl,
                         struct sgl_pos *pos,
                         char *msg,
                         struct sgl_val *val) {
  struct sgl_error *e = sgl_error_init(sgl_malloc(&sgl->error_pool),
                                       pos, msg, val, NULL);

  return sgl_throw_error(sgl, e);
}

struct sgl_op *sgl_throw_error(struct sgl *sgl, struct sgl_error *e) {
  struct sgl_try *t = sgl_vec_pop(&sgl->task->tries);
  
  if (!t) {
    sgl_ls_push(&sgl->errors, &e->ls);
    return sgl->end_pc;
  }
  
  sgl_state_restore(&t->state, sgl);
  sgl_state_restore_calls(&t->state, sgl);
  sgl_push(sgl, sgl->Error)->as_error = e;
  struct sgl_op *end_pc = t->end_pc->next;
  return end_pc;
}

struct sgl_op *sgl_throw_errors(struct sgl *sgl) {
  struct sgl_ls *e = sgl_ls_pop(&sgl->errors);
  if (!e) { return NULL; }
  return sgl_throw_error(sgl, sgl_baseof(struct sgl_error, ls, e));
}

struct sgl_sym *sgl_nsym(struct sgl *sgl, const char *id, sgl_int_t len) {
  char ids[len+1];
  strncpy(ids, id, len);
  ids[len] = 0;
  return sgl_sym(sgl, ids);
}

struct sgl_sym *sgl_sym(struct sgl *sgl, const char *id) {
  struct sgl_hash *ss = &sgl->syms;
  bool is_empty = sgl_hash_init_slots(ss, sgl);
  char id_copy[strlen(id)+1];
  strcpy(id_copy, id);
  sgl_uint_t hk = sgl_hash_key(ss, id_copy);
  struct sgl_ls **hs = sgl_hash_slot(ss, hk);

  if (!is_empty) {
    struct sgl_hash_node *hn = sgl_hash_find(ss, hs, hk, id_copy);
    if (hn) { return hn->val; }
  }

  struct sgl_sym *s = sgl_sym_init(sgl_malloc(&sgl->sym_pool), id_copy, -1);
  sgl_hash_add(ss, sgl, hs, hk, s->id, s);
  sgl->nsyms++;
  return s;
}

struct sgl_sym *sgl_gsym(struct sgl *sgl) {
  char buf[SGL_INT_MAX_LEN+1] = {0};
  buf[0] = 'g';
  return sgl_nsym(sgl, buf, sgl_int_str(sgl->nsyms, buf + 1, 10));
}

bool sgl_add_lib(struct sgl *sgl, struct sgl_pos *pos, struct sgl_lib *lib) {
  bool ok = false;
  struct sgl_ls *i = sgl_lset_find(&sgl->libs, lib->id, NULL, &ok);
  
  if (ok) {
    sgl_error(sgl, pos, sgl_sprintf("Dup lib: %s", lib->id->id));
    return false;
  }

  sgl_ls_insert(i, &lib->ls);
  return true;
}


struct sgl_lib *sgl_find_lib(struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id) {
  bool ok = false;
  struct sgl_ls *i = sgl_lset_find(&sgl->libs, id, NULL, &ok);
  return ok ? sgl_baseof(struct sgl_lib, ls, i) : NULL;
}

struct sgl_lib *sgl_lib(struct sgl *sgl) { return sgl->task->lib; }

void sgl_beg_lib(struct sgl *sgl, struct sgl_lib *lib) {
  struct sgl_task *t = sgl->task;
  *(struct sgl_lib **)sgl_vec_push(&t->libs) = lib;
  t->lib = lib;
}
                                                         
struct sgl_lib *sgl_end_lib(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  assert(t->libs.len > 1);
  struct sgl_lib **l = sgl_vec_pop(&t->libs);
  t->lib = *(struct sgl_lib **)sgl_vec_peek(&t->libs);
  return *l;
}

static bool use_def(struct sgl *sgl,
                    struct sgl_pos *pos,
                    struct sgl_def *sd,
                    struct sgl_lib *dst) {
  if (sd->type == &SGL_DEF_FUNC) {
    struct sgl_def *dd = sgl_lib_find(dst, sgl, pos, sd->id);

    if (dd) {
      if (dd->type != &SGL_DEF_FUNC) {
        sgl_error(sgl, pos, sgl_sprintf("Expected func, was: %s", dd->type->id)); 
        return false;
      }
      
      struct sgl_func
        *sf = sgl_baseof(struct sgl_func, def, sd),
        *df = sgl_baseof(struct sgl_func, def, dd);

      if (sf->nargs != df->nargs) {
        sgl_error(sgl, pos,
                  sgl_sprintf("Func args mismatch: %" SGL_INT "/%" SGL_INT,
                              sf->nargs, df->nargs));
        
        return false;
      }

      sgl_vset_do(&sf->fimps, struct sgl_fimp *, f) {
        bool ok = false;
        sgl_int_t i = sgl_vset_find(&df->fimps, &(*f)->id, NULL, &ok);
          
        if (ok) {
          sgl_error(sgl, pos,
                    sgl_sprintf("Dup fimp: %s(%s)", sf->def.id->id, (*f)->id->id));
            
          return false;
        }

        sgl_func_push_fimp(df, (*f), i);
      }
        
      return true;
    }
  }
  
  return sgl_lib_use(dst, sgl, pos, sd);
}


bool _sgl_use(struct sgl *sgl,
              struct sgl_pos *pos,
              struct sgl_lib *src,
              struct sgl_sym *defs[]) {
  struct sgl_lib *dst = sgl_lib(sgl);

  if (!src->init_ok) {
    sgl_beg_lib(sgl, src);
    bool ok = src->init(src, sgl, pos);
    assert(sgl_end_lib(sgl) == src);
    if (!ok) { return false; }
    src->init_ok = true;
  }
    
  if (defs[0]) {
    for (struct sgl_sym **did = defs; *did; did++) {
      struct sgl_def *sd = sgl_lib_find(src, sgl, pos, *did);

      if (!sd) {
        sgl_error(sgl, pos, sgl_sprintf("Unknown def: %s", (*did)->id));
        return false;
      }

      if (!use_def(sgl, pos, sd, dst)) { return false; }
    }
  } else {
    sgl_ls_do(&src->defs.root, struct sgl_hash_node, ls, n) {
      if (!use_def(sgl, pos, n->val, dst)) { return false; }
    }
  }
  
  return true;
}

bool sgl_add_const(struct sgl *sgl,
                   struct sgl_pos *pos,
                   struct sgl_sym *id,
                   struct sgl_val *val) {
  return sgl_lib_add_const(sgl_lib(sgl), sgl, pos, id, val);
}

struct sgl_def *sgl_find_def(struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id) {
  return sgl_lib_find(sgl->task->lib, sgl, pos, id);
}

struct sgl_type *sgl_find_type(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_sym *id) {     
  return sgl_lib_find_type(sgl_lib(sgl), sgl, pos, id);
}

struct sgl_type *sgl_get_type(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_sym *id) {     
  struct sgl_type *t = sgl_find_type(sgl, pos, id);

  if (!t) {
    sgl_error(sgl, pos, sgl_sprintf("Unknown type: %s", id->id));
    return NULL;
  }

  return t;
}

struct sgl_pmacro *sgl_find_pmacro(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id) {
  struct sgl_def *d = sgl_lib_find(sgl->task->lib, sgl, pos, id);
  if (!d || d->type != &SGL_DEF_PMACRO) { return NULL; }
  return sgl_baseof(struct sgl_pmacro, def, d);
}

struct sgl_macro *sgl_find_macro(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_sym *id) {
  struct sgl_def *d = sgl_lib_find(sgl->task->lib, sgl, pos, id);
  if (!d || d->type != &SGL_DEF_MACRO) { return NULL; }
  return sgl_baseof(struct sgl_macro, def, d);
}

struct sgl_func *sgl_find_func(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_sym *id) {
  struct sgl_def *d = sgl_lib_find(sgl->task->lib, sgl, pos, id);
  if (!d || d->type != &SGL_DEF_FUNC) { return NULL; }
  return sgl_baseof(struct sgl_func, def, d);
}

struct sgl_func *sgl_get_func(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_sym *id) {
  struct sgl_func *f = sgl_find_func(sgl, pos, id);

  if (!f) {
    sgl_error(sgl, pos, sgl_sprintf("Unknown func: %s", id->id));
    return NULL;
  }

  return f;
}

struct sgl_scope *sgl_scope(struct sgl *sgl) {
  struct sgl_ls *ss = &sgl->task->scopes;
    
  return (ss->next == ss)
    ? &sgl->main_scope
    : sgl_baseof(struct sgl_scope, ls, sgl_ls_peek(&sgl->task->scopes));
}

void sgl_beg_scope(struct sgl *sgl, struct sgl_scope *parent, sgl_uint_t var_tag) {
  struct sgl_scope *s =
    sgl_scope_init(sgl_malloc(&sgl->scope_pool), sgl, parent, var_tag);
  
  sgl_ls_push(&sgl->task->scopes, &s->ls);
}

void sgl_end_scope(struct sgl *sgl) {
  struct sgl_scope *s =
    sgl_baseof(struct sgl_scope, ls, sgl_ls_pop(&sgl->task->scopes));
  sgl_scope_end(s, sgl);
  sgl_scope_deref(s, sgl);
}

struct sgl_call *sgl_beg_call(struct sgl *sgl,
                              struct sgl_imp *imp,
                              struct sgl_op *ret_pc) {
  return sgl_call_init(sgl_vec_push(&sgl->task->calls), sgl, imp, ret_pc);
}

struct sgl_call *sgl_end_call(struct sgl *sgl, struct sgl_pos *pos) {
  struct sgl_call *c = sgl_vec_pop(&sgl->task->calls);
  if (!c) { sgl_error(sgl, pos, sgl_strdup("No calls in progress")); }
  return c;
}

struct sgl_call *sgl_call(struct sgl *sgl) {
  struct sgl_vec *cs = &sgl->task->calls;
  return cs->len ? sgl_vec_peek(cs) : NULL;
}

void sgl_beg_stdout(struct sgl *sgl, struct sgl_buf *buf) {
  struct sgl_task *t = sgl->task;
  sgl_ls_push(&t->stdouts, &buf->ls);
  buf->nrefs++;
}

struct sgl_buf *sgl_end_stdout(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  struct sgl_ls *bl = sgl_ls_peek(&t->stdouts);
  assert(bl);

  struct sgl_buf *b = sgl_baseof(struct sgl_buf, ls, bl);
  sgl_ls_delete(bl);
  return b;
}

struct sgl_buf *sgl_stdout(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  struct sgl_ls *bl = sgl_ls_peek(&t->stdouts);
  if (bl) { return sgl_baseof(struct sgl_buf, ls, bl); }
  return &sgl->stdout;
}

bool sgl_flush_stdout(struct sgl *sgl) {
  struct sgl_buf *b = sgl_stdout(sgl);
  if (!b->len) { return true; }

  if (b == &sgl->stdout) {
    if (fwrite(b->data, b->len, 1, stdout) == 1) { b->len = 0; }
  } else {
    struct sgl_task *t = sgl->task;
    struct sgl_ls *pl = b->ls.prev;

    struct sgl_buf *p = (pl == &t->stdouts)
      ? &sgl->stdout
      : sgl_baseof(struct sgl_buf, ls, pl);

    sgl_buf_append(p, b);
  }

  return true;
}

bool sgl_is_async(struct sgl *sgl) {
#ifdef SGL_USE_ASYNC
  return sgl->ntasks > 1 && !sgl->sync_depth;
#endif
  
  return false;
}

#ifdef SGL_USE_ASYNC

struct sgl_io_thread *sgl_io_thread(struct sgl *sgl) {
  if (!sgl->max_io_threads) { return NULL; }
  pthread_mutex_lock(&sgl->io_lock);
    
  if (sgl->nio_threads == sgl->max_io_threads ||
      atomic_load(&sgl->nio_idle_threads)) {
    struct sgl_io_thread *t =
      sgl_baseof(struct sgl_io_thread, ls, sgl_ls_pop(&sgl->io_threads));
    sgl_ls_append(&sgl->io_threads, &t->ls);
    pthread_mutex_unlock(&sgl->io_lock);
    return t;
  }
  
  sgl->nio_threads++;
  struct sgl_io_thread *t = sgl_io_thread_new(sgl);
  pthread_mutex_unlock(&sgl->io_lock);
  sgl_io_thread_start(t);
  return t;
}

bool sgl_run_io(struct sgl *sgl, struct sgl_io *io) {
  sgl->io_depth++;
  
  struct sgl_task *task = sgl->task;
  struct sgl_op *pc = sgl_io_yield(io, sgl, sgl_io_thread(sgl))->next;

  while (sgl->task != task && pc != sgl->end_pc && sgl->max_io_threads) {
    pc = pc->run((sgl->pc = pc), sgl);
  }

  sgl->io_depth--;  

  if (sgl->errors.next != &sgl->errors) {
    sgl_stop_io_threads(sgl);
    return false;
  }

  return true;
}

static struct sgl_op *yield_io(struct sgl *sgl) {
  sgl_ls_rdo(&sgl->io_queue, struct sgl_io, ls, io) {
    if (io->depth == -1 || io->depth == sgl->io_depth) {
      sgl_ls_delete(&io->ls);
      pthread_mutex_unlock(&sgl->io_lock);
      atomic_fetch_sub(&sgl->nio_done, 1);
      sgl_ls_append(&sgl->tasks, &io->task->ls);
      struct sgl_task *t = sgl->task = io->task;
      if (io->free) { io->free(io, sgl); }
      return (sgl->errors.next == &sgl->errors) ? t->pc : sgl->end_pc;
    }
  }

  return NULL;
}

#endif

struct sgl_op *sgl_yield(struct sgl *sgl) {
  if (sgl->errors.next != &sgl->errors) { return sgl->end_pc; }
  
  struct sgl_task *t = sgl->task;
  t->pc = sgl->pc;
  struct sgl_ls *nt = (t->ls.next == &sgl->tasks) ? sgl->tasks.next : t->ls.next;
  
#ifdef SGL_USE_ASYNC
  if (nt == &sgl->tasks || (nt == &t->ls && sgl->ntasks > 1)) {
    pthread_mutex_lock(&sgl->io_lock);

    for (;;) {
      struct sgl_op *pc = yield_io(sgl);
      if (pc) { return pc; }      

      struct timeval tv;
      gettimeofday(&tv, NULL);
      struct timespec ts;
      
      ts.tv_sec = tv.tv_sec;
      ts.tv_nsec = tv.tv_usec * 1000 + 1000;

      //timespec_get (C11) is not available on macOS
      //timespec_get(&ts, TIME_UTC);
      //ts.tv_nsec += 1000;
      
      if (pthread_cond_timedwait(&sgl->io_done, &sgl->io_lock, &ts) == ETIMEDOUT &&
          nt != &sgl->tasks) { break; }
    }
    
    pthread_mutex_unlock(&sgl->io_lock);
  } else if (atomic_load(&sgl->nio_done)) {
    pthread_mutex_lock(&sgl->io_lock);
    struct sgl_op *pc = yield_io(sgl);
    if (pc) { return pc; }
    pthread_mutex_unlock(&sgl->io_lock);
  }
#endif
  
  t = sgl->task = sgl_baseof(struct sgl_task, ls, nt);
  return t->pc;
}

struct sgl_vec *sgl_reg_stack(struct sgl *sgl) { return &sgl->task->reg_stack; }

void sgl_push_reg(struct sgl *sgl, void *val) {
  *(void **)sgl_vec_push(&sgl->task->reg_stack) = val;
}

struct sgl_val *sgl_push_reg_val(struct sgl *sgl, struct sgl_type *type) {
  struct sgl_val *v = sgl_val_new(sgl, type, NULL);
  sgl_push_reg(sgl, v);
  return v;
}

struct sgl_val *sgl_peek_reg_val(struct sgl *sgl, bool dup, struct sgl_ls *root) {
  struct sgl_vec *rs = &sgl->task->reg_stack;
  
  if (!rs->len) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Reg stack is empty"));
    return NULL;
  }

  struct sgl_val *v = *(struct sgl_val **)sgl_vec_peek(rs);
  return dup ? sgl_val_dup(v, sgl, root) : v;
}

void *sgl_peek_reg(struct sgl *sgl) {
  struct sgl_vec *rs = &sgl->task->reg_stack;
  
  if (!rs->len) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Reg stack is empty"));
    return NULL;
  }

  return *(void **)sgl_vec_peek(rs);
}

void *sgl_pop_reg(struct sgl *sgl) {
  struct sgl_vec *rs = &sgl->task->reg_stack;
  
  if (!rs->len) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Reg stack is empty"));
    return NULL;
  }

  return *(void **)sgl_vec_pop(rs);
}

struct sgl_ls *sgl_stack(struct sgl *sgl) { return &sgl->task->stack->root; }

struct sgl_val *sgl_push(struct sgl *sgl, struct sgl_type *type) {
  return sgl_val_new(sgl, type, sgl_stack(sgl));
}

struct sgl_val *sgl_peek(struct sgl *sgl) {
  struct sgl_ls *v = sgl_ls_peek(sgl_stack(sgl));
  return v ? sgl_baseof(struct sgl_val, ls, v) : NULL;
}

struct sgl_val *sgl_pop(struct sgl *sgl) {
  struct sgl_ls *v = sgl_ls_pop(sgl_stack(sgl));
  
  if (!v) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Stack is empty"));
    return NULL;
  }

  return sgl_baseof(struct sgl_val, ls, v);
}

void sgl_reset_stack(struct sgl *sgl) {
  struct sgl_ls *s = sgl_stack(sgl);
  sgl_ls_do(s, struct sgl_val, ls, v) { sgl_val_free(v, sgl); }
  sgl_ls_init(s);
}

struct sgl_list *sgl_beg_stack(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  struct sgl_list *ps = t->stack, *s = t->stack = sgl_list_new(sgl);
  sgl_ls_append(&ps->ls, &s->ls);
  return s;
}

struct sgl_list *sgl_end_stack(struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  struct sgl_list *s = t->stack;
  struct sgl_ls *next = s->ls.next;
  sgl_ls_delete(&s->ls);
  if (next == &t->stacks) { next = t->stacks.prev; }
  assert(next != &t->stacks);
  t->stack = sgl_baseof(struct sgl_list, ls, next);
  return s;
}

struct sgl_list *sgl_switch_stack(struct sgl *sgl, sgl_int_t n) {
  struct sgl_task *t = sgl->task;
  struct sgl_ls *i = &t->stack->ls;

  if (n > 0) {
    for (; n && i != &t->stacks; i = i->next, n--);
  } else {
    for (; n && i != &t->stacks; i = i->prev, n++);
  }

  if (n) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Failed switching stack: %" SGL_INT, n));
    return false;
  }

  t->stack = sgl_baseof(struct sgl_list, ls, i);
  return t->stack;
}

struct sgl_val *sgl_get_var(struct sgl *sgl, struct sgl_sym *id) {
  struct sgl_scope *scope = sgl_scope(sgl);
  struct sgl_vec *vs = &sgl->vars;
  sgl_int_t gap = 0;

  sgl_vec_rdo(vs, struct sgl_var, v) {
    if (v->id) {
      if (v->id == id && (v->scope == scope || v->tag < scope->var_tag)) {
        return v->val;
      }
      
      gap++;
    } else if (!gap) {
      vs->len--;
    }
  }
  
  return NULL;
}

bool sgl_let_var(struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_sym *id,
                 struct sgl_val *val) {
  struct sgl_scope *scope = sgl_scope(sgl);
  sgl_int_t n = scope->nvars;

  if (n) {
    sgl_vec_rdo(&sgl->vars, struct sgl_var, v) {
      if (v->scope == scope) {
        if (v->id == id) {
          sgl_error(sgl, pos, sgl_sprintf("Dup var: %s", id->id));
          return false;
        }
        
        if (!--n) { break; }
      }
    }
  }

  struct sgl_var *v = sgl_vec_push(&sgl->vars);
  v->tag = ++sgl->var_tag;
  v->scope = scope;
  v->id = id;
  v->val = val;
  scope->nvars++;
  return true;
}

bool sgl_compile(struct sgl *sgl, struct sgl_ls *in) {
  struct sgl_ls *i = in->next;
  
  while (i && i != in) {
    struct sgl_form *f = sgl_baseof(struct sgl_form, ls, i);
    i = f->compile(f, sgl, in);
  }
  
  return sgl->errors.next == &sgl->errors;
}

void sgl_dump_ops(struct sgl *sgl, FILE *out) {
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  sgl_ls_do(&sgl->ops, struct sgl_op, ls, op) { sgl_op_dump(op, &buf); }
  fputs(sgl_buf_cs(&buf), out);
  sgl_buf_deinit(&buf);
}

bool sgl_cemit(struct sgl *sgl, struct sgl_op *pc, struct sgl_cemit *out) {
  sgl_cemit_sincl(out, "stddef.h");
  sgl_cemit_sincl(out, "stdio.h");
  sgl_cemit_line(out, NULL);

  sgl_cemit_incl(out, "snigl/form.h");
  sgl_cemit_incl(out, "snigl/ls.h");
  sgl_cemit_incl(out, "snigl/pos.h");
  sgl_cemit_incl(out, "snigl/sgl.h");
  sgl_cemit_incl(out, "snigl/str.h");
  sgl_cemit_incl(out, "snigl/val.h");
  sgl_cemit_line(out, NULL);
  
  sgl_cemit_line(out, "static struct sgl_ls forms;");
  sgl_cemit_line(out, NULL);
  
  sgl_cemit_scope(out, "static bool init(struct sgl *sgl)") {
    sgl_cemit_line(out, "static bool init_ok = false;");
    sgl_cemit_line(out, "if (init_ok) { return false; }");
    sgl_cemit_line(out, "init_ok = true;\n");
    sgl_cemit_line(out, "sgl_ls_init(&forms);");
    sgl_cemit_line(out, "struct sgl_lib *lib = sgl_lib(sgl);");
    sgl_cemit_line(out, "struct sgl_pos p = SGL_NULL_POS;");
    sgl_cemit_line(out, "struct sgl_sym *id = NULL;");
    sgl_cemit_line(out, "struct sgl_val *v = NULL;");
    sgl_cemit_line(out, NULL);
    
    for (struct sgl_op *ipc = pc;
         ipc != sgl->end_pc;
         ipc = sgl_op_cinit(ipc, sgl, out));

    if (sgl->errors.next != &sgl->errors) { return false; }
    sgl_cemit_line(out, "return sgl->errors.next == &sgl->errors;");
  }

  sgl_cemit_line(out, NULL);
  
  sgl_cemit_scope(out, "static bool eval(struct sgl *sgl)") {
    sgl_cemit_line(out, "if (!sgl_run(sgl, sgl->beg_pc->next)) { return false; }");
    while (pc != sgl->end_pc) { pc = sgl_op_cemit(pc, sgl, out); }
    if (sgl->errors.next != &sgl->errors) { return false; }
    sgl_cemit_line(out, "return true;");
  }

  sgl_cemit_line(out, NULL);

  sgl_cemit_scope(out, "int main(int argc, const char *argv[])") {
    sgl_cemit_line(out, "sgl_setup();");
    sgl_cemit_line(out, "struct sgl sgl;");
    sgl_cemit_line(out, "sgl_init(&sgl);");
    if (sgl->debug) { sgl_cemit_line(out, "sgl.debug = true;"); }
    sgl_cemit_line(out, NULL);
      
    sgl_cemit_scope(out, "if (!sgl_use(&sgl, &SGL_NULL_POS, sgl.abc_lib, NULL))") {
      sgl_cemit_line(out, "sgl_dump_errors(&sgl, stdout);");
      sgl_cemit_line(out, "return -1;");
    }

    sgl_cemit_line(out, NULL);

    sgl_cemit_scope(out, "if (!init(&sgl))") {
      sgl_cemit_line(out, "sgl_dump_errors(&sgl, stdout);");
      sgl_cemit_line(out, "return -1;");
    }
  
    sgl_cemit_line(out, "sgl_init_args(&sgl, argc - 1, argv + 1);");
    //sgl_cemit_line(out, "sgl_dump_ops(&sgl, stdout);");
    sgl_cemit_line(out, NULL);
    
    sgl_cemit_scope(out, "if (!eval(&sgl))") {
      sgl_cemit_line(out, "sgl_dump_errors(&sgl, stdout);");
      sgl_cemit_line(out, "return -1;");
    }

    sgl_cemit_line(out, NULL);
    sgl_cemit_line(out, "sgl_deinit(&sgl);");
    sgl_cemit_line(out, "return 0;");
  }
  
  return sgl->errors.next == &sgl->errors;
}

char *sgl_load_path(struct sgl *sgl, const char *in) {
  if (in[0] == SGL_PATH_SEP) { return sgl_strdup(in); }
  const char *end = strrchr(sgl->load_path, SGL_PATH_SEP);
  if (!end) { return sgl_strdup(in); }
  sgl_int_t i = end - sgl->load_path;
  char *out = malloc(i + strlen(in) + 2);
  strncpy(out, sgl->load_path, i);
  out[i++] = SGL_PATH_SEP;
  strcpy(out+i, in);
  return out;
}

bool sgl_load(struct sgl *sgl, struct sgl_pos *pos, char *path) {
  FILE *f = fopen(path, "r");

  if (!f) {
    sgl_error(sgl, pos, sgl_sprintf("Failed opening file: %s", path));
    return false;
  }
  
  fseek(f, 0, SEEK_END);
  sgl_int_t len = ftell(f);
  fseek(f, 0, SEEK_SET);
  char buf[len+1];
  buf[len] = 0;
  sgl_int_t rlen = fread(buf, 1, len, f);
  fclose(f);
  
  if (rlen != len) { return false; }

  struct sgl_ls fs;
  bool ok = false;
  char *prev_path = sgl->load_path;
  *(char **)sgl_vec_push(&sgl->load_paths) = path;
  sgl->load_path = path;
  if (!sgl_parse(buf, sgl, sgl_ls_init(&fs))) { goto exit; }
  if (!sgl_compile(sgl, &fs)) { goto exit; }
  sgl_forms_deref(&fs, sgl);
  ok = true;
 exit:
  sgl->load_path = prev_path;
  return ok;
}

sgl_int_t sgl_trace(struct sgl *sgl, struct sgl_op *pc) {
  struct sgl_trace *t = sgl_trace_new(sgl);
  struct sgl_op *beg_pc = pc;
  bool changed = false;
  sgl_int_t nrounds = 0;

  for (pc = beg_pc;
       pc != sgl->end_pc &&
         sgl->errors.next == &sgl->errors &&
         (sgl->trace == -1 || nrounds < sgl->trace);
       pc = pc->next) {
    changed |= sgl_op_trace(pc, sgl, t);
    
    if (changed && (!t->len || pc == sgl->end_pc || pc->next == sgl->end_pc)) {
      sgl_trace_clear(t, sgl);
      pc = beg_pc;
      changed = false;
      nrounds++;
    }
  }

  sgl_trace_free(t, sgl);
  return (sgl->errors.next == &sgl->errors) ? nrounds : -1;
}

struct sgl_pos *sgl_pos(struct sgl *sgl) {
  return sgl->pc ? &sgl->pc->form->pos : &SGL_NULL_POS;
}

bool sgl_run(struct sgl *sgl, struct sgl_op *pc) {
  while ((pc = pc->run((sgl->pc = pc), sgl)) != sgl->end_pc);
  return sgl->errors.next == &sgl->errors;
}

bool sgl_run_task(struct sgl *sgl,
                  struct sgl_task *task,
                  struct sgl_op *pc, struct sgl_op *end_pc) {
  while ((sgl->task != task || pc != end_pc) && pc != sgl->end_pc) {
    pc = pc->run((sgl->pc = pc), sgl);
  }

  return sgl->errors.next == &sgl->errors;
}

void sgl_defer(struct sgl *sgl, struct sgl_op_defer *op) {
  sgl_ls_push(&sgl_scope(sgl)->defers, &op->ls);
}

void sgl_dump_errors(struct sgl *sgl, FILE *out) {
  sgl_ls_do(&sgl->tasks, struct sgl_task, ls, t) { sgl_task_end_scopes(t, sgl); }
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  
  sgl_ls_do(&sgl->errors, struct sgl_error, ls, e) {
    if (buf.len) { sgl_buf_putc(&buf, '\n'); }
    sgl_error_dump(e, &buf);
    sgl_buf_putc(&buf, '\n');
    fputs(sgl_buf_cs(&buf), out);
    buf.len = 0;
    sgl_error_deref(e, sgl);
  }

  sgl_buf_deinit(&buf);
}

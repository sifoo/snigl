#include <stddef.h>

#include "snigl/lset.h"

struct sgl_lset *sgl_lset_init(struct sgl_lset *s, sgl_ls_cmp_t cmp) {
  s->cmp = cmp;
  sgl_ls_init(&s->root);
  return s;
}

struct sgl_ls *sgl_lset_find(struct sgl_lset *s,
                             const void *key,
                             void *data,
                             bool *ok) {
  return sgl_ls_find(&s->root, s->cmp, key, data, ok);
}

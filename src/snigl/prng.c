#include "snigl/prng.h"
#include "snigl/time.h"

sgl_uint_t sgl_prng_seed() { return sgl_now() + (sgl_uint_t)sgl_prng_seed; }

struct sgl_prng *sgl_prng_init(struct sgl_prng *p, sgl_uint_t seed) {
  sgl_uint_t *s = p->state;
  s[0] = s[1] = seed;
  return p;
}


bool sgl_prng_eq(struct sgl_prng *p1, struct sgl_prng *p2) {
  return p1->state[0] == p2->state[0] && p1->state[1] == p2->state[1];
}

// Algo: xorshift128plus

sgl_uint_t sgl_prng_next(struct sgl_prng *p) {
  uint64_t *s = p->state;
  uint64_t x = s[0];
  const uint64_t y = s[1];
  s[0] = y;
  x ^= x << 23;
  s[1] = x ^ y ^ (x >> 17) ^ (y >> 26);
  return s[1] + y;
}

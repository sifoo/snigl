#include "snigl/pos.h"

struct sgl_pos SGL_NULL_POS;
struct sgl_pos SGL_START_POS;

struct sgl_pos *sgl_pos_init(struct sgl_pos *pos,
                             const char *load_path,
                             sgl_int_t row, sgl_int_t col) {
  pos->load_path = load_path;
  pos->row = row;
  pos->col = col;
  return pos;
}

void sgl_setup_pos() {
  sgl_pos_init(&SGL_NULL_POS, "n/a", -1, -1);
  sgl_pos_init(&SGL_START_POS, "n/a", 1, 0);
}

#ifndef SNIGL_STARCH_H
#define SNIGL_STARCH_H

#include "snigl/buf.h"
#include "snigl/int.h"
#include "snigl/iter.h"
#include "snigl/vset.h"

enum sgl_store_id {
  SGL_STORE_INVALID = -1,
  SGL_STORE_DUP, SGL_STORE_DUP_SHORT,
  SGL_STORE_CLONE, SGL_STORE_CLONE_SHORT,
  SGL_STORE_T, SGL_STORE_F,
  SGL_STORE_INT, SGL_STORE_INT_ZERO, SGL_STORE_INT_ONE, SGL_STORE_INT_SHORT,
  SGL_STORE_LIST, SGL_STORE_LIST_EMPTY, SGL_STORE_LIST_SINGLE,
  SGL_STORE_STR, SGL_STORE_STR_EMPTY, SGL_STORE_STR_SHORT,
  SGL_STORE_LEN
};

typedef uint8_t sgl_store_id_t;

enum sgl_store_id sgl_load_id(struct sgl_buf *in);
void sgl_store_id(enum sgl_store_id id, struct sgl_buf *out);
bool sgl_load_short(struct sgl_buf *in, uint8_t *out);
void sgl_store_short(sgl_int_t v, struct sgl_buf *out);

bool sgl_load_int(enum sgl_store_id id,
                  struct sgl_buf *in,
                  struct sgl *sgl,
                  sgl_int_t *out);

void sgl_store_int(sgl_int_t v, struct sgl_buf *out);

struct sgl_starch {
  struct sgl_buf buf;
  struct sgl_vec load_vals;
  struct sgl_vset store_vars;
};

struct sgl_starch *sgl_starch_new(struct sgl *sgl);
struct sgl_starch *sgl_starch_ref(struct sgl_starch *s);
void sgl_starch_deref(struct sgl_starch *s, struct sgl *sgl);

struct sgl_val *sgl_starch_load(struct sgl_starch *s,
                                struct sgl *sgl,
                                struct sgl_ls *root);

bool sgl_starch_store(struct sgl_starch *s,
                      struct sgl *sgl,
                      struct sgl_val *val);

struct sgl_store_var {
  sgl_int_t i;
  struct sgl_val *val;
};

struct sgl_starch_iter {
  struct sgl_iter iter;
  struct sgl_starch *src;
};

struct sgl_starch_iter *sgl_starch_iter_new(struct sgl_starch *src, struct sgl *sgl);

#endif

#include <assert.h>
#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/struct.h"
#include "snigl/iter.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/struct.h"
#include "snigl/user_type.h"
#include "snigl/val.h"

static bool clone_val(struct sgl_val *src,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *dst) {
  if (sgl_user_type_clone_val(src, sgl, pos, dst)) { return true; }
  struct sgl_struct_type *st = sgl_struct_type(src->type);
  struct sgl_field **f = sgl_vec_beg(&st->fields);
  struct sgl_struct *s = sgl_struct_new(st, sgl);
  struct sgl_val **sfs = src->as_struct->fields, **dfs = s->fields;

  for (struct sgl_val **sf = sfs, **df = dfs;
       sf < sfs + st->fields.len;
       f++, sf++, df++) {
    if (*sf) {
      *df = sgl_val_clone(*sf, sgl, pos, NULL);

      if (!*df) {
        sgl_error(sgl, pos, sgl_sprintf("Failed cloning field: %s", (*f)->id->id));
        sgl_struct_deref(s, sgl, st);
        return false;
      }
    }
  }
  
  dst->as_struct = s;
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *val,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {  
  struct sgl_struct_type *st = sgl_struct_type(val->type);
  struct sgl_field **f = sgl_vec_beg(&st->fields);
  struct sgl_val **vfs = val->as_struct->fields, **rfs = rhs->as_struct->fields;

  for (struct sgl_val **vf = vfs, **rf = rfs;
       vf < vfs + st->fields.len;
       f++, vf++, rf++) {
    enum sgl_cmp ok = sgl_val_cmp(*vf, sgl, *rf);
    if (ok != SGL_EQ) { return ok; }
  }

  return SGL_EQ;
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_struct_deref(val->as_struct, sgl, sgl_struct_type(val->type));
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_printf(out, "(%s", val->type->def.id->id);
  struct sgl_struct_type *st = sgl_struct_type(val->type);

  for (struct sgl_val **fs = val->as_struct->fields, **v = fs;
       v < fs + st->fields.len;
       v++) {
    if (*v) {
      sgl_buf_putc(out, ' ');
      sgl_val_dump(*v, out);
    } else {
      sgl_buf_putcs(out, " _");
    }
  }
  
  sgl_buf_putc(out, ')');
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_struct *s = out->as_struct = val->as_struct;
  s->nrefs++;
}

static bool eq_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  if (sgl->cfimp != sgl->eq_fimp &&
      sgl_func_call(sgl->eq_fimp->func, sgl, true, val, rhs)) {
    return sgl_pop(sgl)->as_bool;
  }
  
  struct sgl_struct_type *st = sgl_struct_type(val->type);
  struct sgl_field **f = sgl_vec_beg(&st->fields);
  struct sgl_val **vfs = val->as_struct->fields, **rfs = rhs->as_struct->fields;

  for (struct sgl_val **vf = vfs, **rf = rfs;
       vf < vfs + st->fields.len;
       f++, vf++, rf++) {
    struct sgl_val *vv = *vf, *rv = *rf;
    if (!vv && !rv) { continue; }
    if (!vv || !rv || !sgl_val_eq(vv, sgl, rv)) { return false; }
  }

  return true;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_struct == rhs->as_struct;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return (sgl_uint_t)v->as_struct; }

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  sgl_buf_printf(out, "(%s %p)", val->type->def.id->id, val->as_struct);
}

static enum sgl_cmp field_set_cmp(struct sgl_ls *lhs, const void *rhs, void *data) {
  struct sgl_sym *lsym = sgl_baseof(struct sgl_field, ls, lhs)->id;
  return sgl_sym_cmp(lsym, rhs);
}

static void free_imp(struct sgl_type *t, struct sgl *sgl) {
  struct sgl_struct_type *st = sgl_struct_type(t);
  sgl_vec_do(&st->fields, struct sgl_field *, f) { sgl_field_free(*f, sgl); } 
  sgl_vec_deinit(&st->fields);
  sgl_free(&sgl->struct_type_pool, st);
  sgl_ref_type_deinit(&st->ref_type);
}

struct sgl_struct_type *sgl_struct_type(struct sgl_type *t) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
  return sgl_baseof(struct sgl_struct_type, ref_type, rt);
}

struct sgl_type *sgl_struct_type_new(struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_lib *lib,
                                     struct sgl_sym *id,
                                     struct sgl_type *parents[]) {
  struct sgl_struct_type *st = sgl_malloc(&sgl->struct_type_pool);
  st->init_vals = false;
  
  sgl_lset_init(&st->field_set, field_set_cmp);
  sgl_vec_init(&st->fields, sizeof(struct sgl_field *));

  sgl_ref_type_init(&st->ref_type, sgl, pos, lib, id, parents,
                    sizeof(struct sgl_struct),
                    offsetof(struct sgl_struct, ls));

  for (struct sgl_type **p = parents; *p; p++) {
    if (*p != sgl->Struct && sgl_derived(*p, sgl->Struct)) {
      struct sgl_ref_type *prt = sgl_baseof(struct sgl_ref_type, type, *p);
      struct sgl_struct_type *pst = sgl_baseof(struct sgl_struct_type, ref_type, prt);
      sgl_vec_do(&pst->fields, struct sgl_field *, fp) {
        struct sgl_field *f = *fp;
        
        if (!sgl_struct_type_add(st, sgl, pos, f->id, f->type,
                                 f->init_val
                                 ? sgl_val_dup(f->init_val, sgl, NULL)
                                 : NULL)) {
          sgl_free(&sgl->struct_type_pool, st);
          return NULL;
        }
      }
    }
  }  

  struct sgl_type *t = &st->ref_type.type;
  t->call_val = sgl_user_type_call_val;
  t->clone_val = clone_val;
  t->cmp_val = cmp_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->iter_val = sgl_user_type_iter_val;
  t->print_val = print_val;
  t->free = free_imp;

  if (!sgl_derive(sgl_type_meta(t, sgl), sgl_type_meta(sgl->Struct, sgl))) {
    sgl_type_free(t, sgl);
    return NULL;
  }
  
  return t;
}

struct sgl_field *sgl_struct_type_find(struct sgl_struct_type *t,
                                       struct sgl_sym *id) {
  bool ok = false;
  struct sgl_ls *found = sgl_lset_find(&t->field_set, id, NULL, &ok);
  return ok ? sgl_baseof(struct sgl_field, ls, found) : NULL;
}

struct sgl_field *sgl_struct_type_add(struct sgl_struct_type *t,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_sym *id,
                                      struct sgl_type *type,
                                      struct sgl_val *init_val) {
  bool ok = false;
  struct sgl_ls *ls = sgl_lset_find(&t->field_set, id, NULL, &ok);
  
  if (ok) {
    sgl_error(sgl, pos, sgl_sprintf("Dup field: %s", id));
    return NULL;
  }

  assert(sgl_ls_null(&t->ref_type.pool.slabs));
  struct sgl_pool *p = &t->ref_type.pool;
  p->slot_size += sizeof(struct sgl_val *);
  p->slab_size = SGL_MIN_SLAB * p->slot_size;
  return sgl_field_new(sgl, t, id, type, init_val, ls);
}

struct sgl_field *sgl_field_new(struct sgl *sgl,
                                struct sgl_struct_type *struct_type,
                                struct sgl_sym *id,
                                struct sgl_type *type,
                                struct sgl_val *init_val,
                                struct sgl_ls *next) {
  struct sgl_field *f = sgl_malloc(&sgl->field_pool);
  f->i = struct_type->fields.len;
  f->id = id;
  f->type = type;
   
  sgl_ls_insert(next, &f->ls);
  *(struct sgl_field **)sgl_vec_push(&struct_type->fields) = f;
  if ((f->init_val = init_val)) { struct_type->init_vals = true; }
  return f;
}

void sgl_field_free(struct sgl_field *f, struct sgl *sgl) {
  if (f->init_val) { sgl_val_free(f->init_val, sgl); }
  sgl_free(&sgl->field_pool, f);
}
                    

#include <assert.h>

#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/iters/int.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/starch.h"
#include "snigl/type.h"
#include "snigl/types/int.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) { return val->as_int > 0; }

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->Int, %s);",
                 id, root ? root : "NULL");
  
  sgl_cemit_line(out, "%s->as_int = %" SGL_INT ";", id, val->as_int);
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *val,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {
  return sgl_int_cmp(val->as_int, rhs->as_int);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_put_int(out, val->as_int);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_int = val->as_int;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_int == rhs->as_int;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return v->as_int; }

static struct sgl_iter *iter_val(struct sgl_val *val,
                                 struct sgl *sgl,
                                 struct sgl_type **type) {
  if (type) { *type = sgl->IntIter; }
  return &sgl_int_iter_new(sgl, val->as_int)->iter;
}

static struct sgl_val *load_val(struct sgl_starch *in,
                                struct sgl *sgl,
                                enum sgl_store_id id,
                                struct sgl_ls *root) {
  sgl_int_t i = 0;
  if (!sgl_load_int(id, &in->buf, sgl, &i)) { return NULL; }
  struct sgl_val *v = sgl_val_new(sgl, sgl->Int, root);
  v->as_int = i;
  return v;
}

static bool store_val(struct sgl_val *v, struct sgl *sgl, struct sgl_starch *out) {
  sgl_store_int(v->as_int, &out->buf);
  return true;
}

struct sgl_type *sgl_int_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->iter_val = iter_val;
  t->load_val = load_val;
  t->store_val = store_val;  
  return t;
}

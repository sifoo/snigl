#ifndef SNIGL_TYPES_STRUCT_H
#define SNIGL_TYPES_STRUCT_H

#include "snigl/ref_type.h"
#include "snigl/lset.h"
#include "snigl/vec.h"

struct sgl_lib;
struct sgl_sym;
struct sgl_type;

struct sgl_struct_type {
  struct sgl_ref_type ref_type;
  struct sgl_lset field_set;
  struct sgl_vec fields;
  bool init_vals;
};

struct sgl_field {
  struct sgl_ls ls;
  sgl_int_t i;
  struct sgl_sym *id;
  struct sgl_type *type;
  struct sgl_val *init_val;
};

struct sgl_struct_type *sgl_struct_type(struct sgl_type *t);

struct sgl_type *sgl_struct_type_new(struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_lib *lib,
                                     struct sgl_sym *id,
                                     struct sgl_type *parents[]);

struct sgl_field *sgl_struct_type_add(struct sgl_struct_type *t,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_sym *id,
                                      struct sgl_type *type,
                                      struct sgl_val *init_val);

struct sgl_field *sgl_struct_type_find(struct sgl_struct_type *t, struct sgl_sym *id);

struct sgl_field *sgl_field_new(struct sgl *sgl,
                                struct sgl_struct_type *struct_type,
                                struct sgl_sym *id,
                                struct sgl_type *type,
                                struct sgl_val *init_val,
                                struct sgl_ls *next);

void sgl_field_free(struct sgl_field *f, struct sgl *sgl);

#endif

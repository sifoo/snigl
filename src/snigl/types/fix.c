#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/types/fix.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) { return val->as_fix.val; }

static enum sgl_cmp cmp_val(struct sgl_val *val,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {
  return sgl_fix_cmp(&val->as_fix, &rhs->as_fix);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_fix_dump(&val->as_fix, out);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_fix = val->as_fix;
}

static bool eq_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  return sgl_fix_eq(&val->as_fix, &rhs->as_fix);
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return sgl_fix_is(&val->as_fix, &rhs->as_fix);
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  struct sgl_fix *f = &v->as_fix;
  return f->val ^ f->scale;
}


static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  sgl_fix_print(&val->as_fix, out);
}

struct sgl_type *sgl_fix_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->print_val = print_val;
  return t;
}

#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/types/bool.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) { return val->as_bool; }

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_putc(out, val->as_bool ? 't' : 'f');
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_bool = val->as_bool;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_bool == rhs->as_bool;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return v->as_bool; }

static struct sgl_val *load_val(struct sgl_starch *in,
                                struct sgl *sgl,
                                enum sgl_store_id id,
                                struct sgl_ls *root) {
  switch (id) {
  case SGL_STORE_T:
    return sgl_val_dup(&sgl->t, sgl, root);
  case SGL_STORE_F:
    return sgl_val_dup(&sgl->f, sgl, root);
  default:
    break;
  }

  return NULL;
}

static bool store_val(struct sgl_val *v, struct sgl *sgl, struct sgl_starch *out) {
  sgl_store_id(v->as_bool ? SGL_STORE_T : SGL_STORE_F, &out->buf);
  return true;
}

struct sgl_type *sgl_bool_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->hash_val = hash_val;
  t->is_val = is_val;
  t->load_val = load_val;
  t->store_val = store_val;
  return t;
}

#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/list_set.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/list_set.h"
#include "snigl/val.h"

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  struct sgl_list_set *s = sgl_baseof(struct sgl_list_set, list, val->as_list);

  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->ListSet, %s);",
                 id, root ? root : "NULL");
  
  sgl_cemit_line(out, "%s->as_list = &sgl_list_set_new(sgl)->list;", id);
  struct sgl_ls *l = &s->list.root;

  if (l->next != l) {
    struct sgl_sym *lid = sgl_gsym(sgl), *vid = sgl_gsym(sgl);
    sgl_cemit_line(out, "struct sgl_ls *%s = &%s->as_list->root;", lid->id, id);
    sgl_cemit_line(out, "struct sgl_val *%s = NULL;", vid->id);

    sgl_ls_do(&val->as_list->root, struct sgl_val, ls, v) {
      if (!sgl_val_cemit(v, sgl, pos, vid->id, lid->id, out)) { return false; }
    }
  }
  
  return true;
}

static bool clone_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_list_set
    *src = sgl_baseof(struct sgl_list_set, list, val->as_list),
    *dst = sgl_list_set_new(sgl, sgl_val_dup(src->key, sgl, NULL));

  sgl_ls_do(&src->list.root, struct sgl_val, ls, v) {
    if (!sgl_val_clone(v, sgl, pos, &dst->list.root)) {
      sgl_list_set_deref(dst);
      return false;
    }
  }
  
  out->as_list = &dst->list;
  return true;
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  struct sgl_list_set *s = sgl_baseof(struct sgl_list_set, list, val->as_list);
  sgl_list_set_deref(s);
}

struct sgl_type *sgl_list_set_type_new(struct sgl *sgl,
                                       struct sgl_pos *pos,
                                       struct sgl_lib *lib,
                                       struct sgl_sym *id,
                                       struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_list_set),
                                        offsetof(struct sgl_list_set, list) +
                                        offsetof(struct sgl_list, ls));
  t->cemit_val = cemit_val;
  t->deinit_val = deinit_val;
  t->clone_val = clone_val;
  return t;
}

#include <ctype.h>

#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/types/char.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) {
  return val->as_char != 0;
}

static enum sgl_cmp cmp_val(struct sgl_val *val,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {
  return sgl_char_cmp(val->as_char, rhs->as_char);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  unsigned char c = val->as_char;
  
  switch(c) {
  case '\n':
    sgl_buf_putcs(out, "\\n");
    break;
  case ' ':
    sgl_buf_putcs(out, "\\s");
    break;
  default:
    if (isgraph(c)) {
      sgl_buf_printf(out, "#%c", c);
    } else {
      sgl_buf_printf(out, "\\%u", (unsigned int)c);
    }
  }
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_char = val->as_char;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_char == rhs->as_char;
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  return v->as_char;
}

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  sgl_buf_putc(out, val->as_char);
}

struct sgl_type *sgl_char_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->print_val = print_val;
  return t;
}

#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/ref.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/ref.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *v, struct sgl *sgl) {
  struct sgl_val *rv = v->as_ref->val;
  return rv && rv->type != sgl->Nil;
}

static bool cemit_val(struct sgl_val *v,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->Ref, %s);",
                 id, root ? root : "NULL");

  struct sgl_sym *vid = sgl_gsym(sgl);
  struct sgl_val *rv = v->as_ref->val;

  if (rv) {
    sgl_val_cemit(rv, sgl, pos, vid->id, NULL, out); 
    sgl_cemit_line(out, "%s->as_ref = sgl_ref_new(sgl, %s);", id, vid->id);
  } else {
    sgl_cemit_line(out, "%s->as_ref = sgl_ref_new(sgl, NULL);", id);
  }
  
  return true;
}

static bool clone_val(struct sgl_val *v,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_ref *src = v->as_ref;
  struct sgl_val *rv = src->val;
  out->as_ref = sgl_ref_new(sgl, rv ? sgl_val_clone(rv, sgl, pos, NULL) : NULL);
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *lhs,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {
  struct sgl_ref *lr = lhs->as_ref, *rr = rhs->as_ref;
  struct sgl_val *lv = lr->val, *rv = rr->val;
  if (!lv && !rv) { return SGL_EQ; }
  if (!lv) { return SGL_LT; }
  if (!rv) { return SGL_GT; }
  return sgl_ptr_cmp(lr, rr);
}

static void deinit_val(struct sgl_val *v, struct sgl *sgl) {
  sgl_ref_deref(v->as_ref, sgl);
}

static void dump_val(struct sgl_val *v, struct sgl_buf *out) {
  struct sgl_val *rv = v->as_ref->val;
  sgl_buf_putcs(out, "(Ref ");

  if (rv) {
    sgl_val_dump(rv, out);
  } else {
    sgl_buf_putcs(out, "(Ref nil)");
  }
          
  sgl_buf_putc(out, ')');
}

static void dup_val(struct sgl_val *v, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_ref *r = out->as_ref = v->as_ref;
  r->nrefs++;
}

static bool eq_val(struct sgl_val *lhs, struct sgl *sgl, struct sgl_val *rhs) {
  struct sgl_val *lv = lhs->as_ref->val, *rv = rhs->as_ref->val;
  if (!lv && !rv) { return true; }
  if (!lv || !rv) { return false; }
  return sgl_val_is(lv, rv);
}

static bool is_val(struct sgl_val *lhs, struct sgl_val *rhs) {
  return lhs->as_ref == rhs->as_ref;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return (sgl_uint_t)v->as_ref; }

static void print_val(struct sgl_val *v,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  struct sgl_val *rv = v->as_ref->val;
  if (rv) { sgl_val_print(rv, sgl, pos, out); }
}

struct sgl_type *sgl_vref_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_ref),
                                        offsetof(struct sgl_ref, ls));
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->clone_val = clone_val;
  t->cmp_val = cmp_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->print_val = print_val;
  return t;
}

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/str.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) {
  return val->as_str->len;
}

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  struct sgl_sym *vid = sgl_gsym(sgl);
  struct sgl_str *s = val->as_str;
  struct sgl_buf vbuf;
  sgl_buf_init(&vbuf);
  sgl_buf_putc(&vbuf, '{');
  
  for (char *beg = sgl_str_cs(s), *c = beg; c < beg + s->len; c++) {
    if (c > beg) { sgl_buf_putcs(&vbuf, ", "); }
    sgl_buf_put_int(&vbuf, *c);
  }

  sgl_buf_putc(&vbuf, '}');
  
  sgl_cemit_line(out,
                 "char %s[%" SGL_INT "] = %s;",
                 vid->id, s->len, sgl_buf_cs(&vbuf));

  sgl_buf_deinit(&vbuf);
  
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->Str, %s);",
                 id, root ? root : "NULL");

  sgl_cemit_line(out,
                 "%s->as_str = sgl_str_new(sgl, %s, %" SGL_INT ");",
                 id, vid->id, s->len);

  return true;
}

static bool clone_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_str *s = val->as_str;
  out->as_str = sgl_str_new(sgl, sgl_str_cs(s), s->len);
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *val,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {
  struct sgl_str *ls = val->as_str, *rs = rhs->as_str;
  int c = strcmp(sgl_str_cs(ls), sgl_str_cs(rs));
  if (c < 0) { return SGL_LT; }
  return (c > 0) ? SGL_GT : SGL_EQ;
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_str_deref(val->as_str, sgl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_putc(out, '"');
  struct sgl_str *s = val->as_str;
  char *c = sgl_str_cs(s);

  for (sgl_int_t i = 0; i < s->len; i++, c++) {
    switch (*c) {
    case '\\':
      sgl_buf_putcs(out, "\\\\");
      break;
    case '\n':
      sgl_buf_putcs(out, "\\n");
      break;
    default:
      sgl_buf_putc(out, *c);
    }
  }

  sgl_buf_putc(out, '"');
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_str *s = val->as_str;
  out->as_str = s;
  s->nrefs++;
}

static bool eq_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  struct sgl_str *ls = val->as_str, *rs = rhs->as_str;
  return strncmp(sgl_str_cs(ls), sgl_str_cs(rs), sgl_min(ls->len, rs->len)) == 0;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_str == rhs->as_str;
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  struct sgl_str *s = v->as_str;
  return sgl_hash_ncs(sgl_str_cs(s), s->len);
}

static struct sgl_iter *iter_val(struct sgl_val *val,
                                 struct sgl *sgl,
                                 struct sgl_type **type) {
  if (type) { *type = sgl->StrIter; }
  return &sgl_str_iter_new(sgl, val->as_str)->iter;
}

static struct sgl_val *load_val(struct sgl_starch *in,
                                struct sgl *sgl,
                                enum sgl_store_id id,
                                struct sgl_ls *root) {
  sgl_uint_t len = 0;
  struct sgl_buf *ib = &in->buf;
  
  switch (id) {
  case SGL_STORE_STR_EMPTY:
    return sgl_val_dup(&sgl->empty_str, sgl, root);
  case SGL_STORE_STR_SHORT:
    if (!sgl_load_short(ib, (uint8_t *)&len)) { return NULL; }
    break;
  case SGL_STORE_STR: {
    enum sgl_store_id len_id = sgl_load_id(ib);
    if (len_id == SGL_STORE_INVALID) { return NULL; }
    if (!sgl_load_int(len_id, ib, sgl, (sgl_int_t *)&len)) { return NULL; }
    break;
  }
  default:
    return NULL;
  }
  
  char *src = sgl_buf_get(ib, len);
  struct sgl_val *out = sgl_val_new(sgl, sgl->Str, root);
  out->as_str = sgl_str_new(sgl, src, len);
  return out;
}
  
static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  struct sgl_str *s = val->as_str;
  sgl_buf_nputcs(out, sgl_str_cs(s), s->len);
}

static bool store_val(struct sgl_val *v, struct sgl *sgl, struct sgl_starch *out) {
  struct sgl_str *s = v->as_str;
  sgl_uint_t sl = s->len;
  struct sgl_buf *ob = &out->buf;
  
  if (!sl) {
    sgl_store_id(SGL_STORE_STR_EMPTY, ob);
    return true;
  }

  if (sl < 256) {
    sgl_buf_grow(ob, ob->len + sl + 1);
    sgl_store_id(SGL_STORE_STR_SHORT, ob);
    sgl_store_short(sl, ob);
  } else {
    sgl_buf_grow(ob, ob->len + sl + 3);
    sgl_store_id(SGL_STORE_STR, ob);
    sgl_store_int(sl, ob);
  }

  memcpy(ob->data + ob->len, sgl_str_cs(s), sl);
  ob->len += sl;
  return true;
}

struct sgl_type *sgl_str_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_str),
                                        offsetof(struct sgl_str, ls));
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->clone_val = clone_val;
  t->cmp_val = cmp_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->iter_val = iter_val;
  t->load_val = load_val;
  t->print_val = print_val;
  t->store_val = store_val;
  return t;
}

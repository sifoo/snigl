#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/xy.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/types/xy.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *v, struct sgl *sgl) {
  struct sgl_xy *xy = &v->as_xy;
  return xy->x || xy->y;
}

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  struct sgl_xy *xy = &val->as_xy;
  
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->XY, %s);",
                 id, root ? root : "NULL");
  
  sgl_cemit_line(out,
                 "sgl_xy_init(&%s->as_xy, %" SGL_INT ", %" SGL_INT ");", 
                 id, xy->x, xy->y);
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *lhs,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {
  const struct sgl_xy *lxy = &lhs->as_xy, *rxy = &rhs->as_xy;
  enum sgl_cmp ok = sgl_int_cmp(lxy->x, rxy->x);
  if (ok != SGL_EQ) { return ok; }
  return sgl_int_cmp(lxy->y, rxy->y);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  struct sgl_xy *xy = &val->as_xy;
  sgl_buf_printf(out, "(XY %" SGL_INT " %" SGL_INT ")", xy->x, xy->y);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_xy = val->as_xy;
}

static bool is_val(struct sgl_val *lhs, struct sgl_val *rhs) {
  struct sgl_xy *lxy = &lhs->as_xy, *rxy = &rhs->as_xy;
  return lxy->x == rxy->x && lxy->y == rxy->y;
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  struct sgl_xy *xy = &v->as_xy;
  return xy->x ^ xy->y;
}

struct sgl_type *sgl_xy_type_new(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_lib *lib,
                                 struct sgl_sym *id,
                                 struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  return t;
}

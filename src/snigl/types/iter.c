#include <assert.h>

#include "snigl/buf.h"
#include "snigl/iter.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/iter.h"
#include "snigl/val.h"

static bool clone_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_iter *i = val->as_iter;

  if (!i->clone) {
    sgl_error(sgl, pos,
              sgl_sprintf("Clone not implemented: %s",
                          sgl_type_id(val->type)->id));
  }
  
  out->as_iter = i->clone(i, sgl);
  return true;
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_iter_deref(val->as_iter, sgl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_printf(out, "(%s %p)", val->type->def.id->id, val->as_iter);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_iter *i = out->as_iter = val->as_iter;
  i->nrefs++;
}

static bool eq_val(struct sgl_val *v, struct sgl *sgl, struct sgl_val *rhs) {
  struct sgl_iter *i = v->as_iter;
  
  if (!i->eq) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Eq not implemented: %s",
                          sgl_type_id(v->type)->id));
  }

  return i->eq(i, sgl, rhs->as_iter);
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_iter == rhs->as_iter;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return (sgl_uint_t)v->as_iter; }

static struct sgl_iter *iter_val(struct sgl_val *val,
                                 struct sgl *sgl,
                                 struct sgl_type **type) {
  if (type) { *type = val->type; }
  struct sgl_iter *i = val->as_iter;
  i->nrefs++;
  return i;
}

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  struct sgl_iter *i = val->as_iter;
  struct sgl_val *v = NULL;
  
  while ((v = sgl_iter_next_val(i, sgl, pos, NULL))) {
    sgl_val_print(v, sgl, pos, out);
    sgl_val_free(v, sgl);
  }
}

struct sgl_type *sgl_iter_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_malloc(&sgl->ref_type_pool);
  sgl_type_init(t, sgl, pos, lib, id, parents);
  t->clone_val = clone_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->iter_val = iter_val;
  t->print_val = print_val;
  return t;
}

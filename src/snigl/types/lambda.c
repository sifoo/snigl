#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/form.h"
#include "snigl/lambda.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/lambda.h"
#include "snigl/val.h"

static struct sgl_op *call_val(struct sgl_val *val,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_op *return_pc,
                               bool now) {
  struct sgl_lambda *l = val->as_lambda;
  struct sgl_imp *i = &l->imp;
  if (i->beg_pc == i->end_pc) { return return_pc->next; }
  if (i->flags & SGL_IMP_SCOPE) { sgl_beg_scope(sgl, l->parent_scope, l->var_tag); }
  sgl_beg_call(sgl, i, return_pc);
  
  if (now) {
    return sgl_run_task(sgl, sgl->task, i->beg_pc->next, return_pc->next)
      ? return_pc->next
      : sgl->end_pc;
  }

  return i->beg_pc->next;
}

static enum sgl_cmp cmp_val(struct sgl_val *val,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {
  return sgl_ptr_cmp(val->as_lambda->imp.body, rhs->as_lambda->imp.body);
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_lambda_deref(val->as_lambda, sgl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  struct sgl_lambda *l = val->as_lambda;
  struct sgl_pos *pos = &l->imp.body->pos;
  
  sgl_buf_printf(out,
                 "(%s %" SGL_INT " %" SGL_INT ")",
                 sgl_type_id(val->type)->id, pos->row, pos->col);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_lambda *l = out->as_lambda = val->as_lambda;
  l->nrefs++;
}

static bool eq_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  return val->as_lambda->imp.body == rhs->as_lambda->imp.body;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_lambda == rhs->as_lambda;
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  return (sgl_uint_t)v->as_lambda->imp.beg_pc;
}

struct sgl_type *sgl_lambda_type_new(struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_lib *lib,
                                     struct sgl_sym *id,
                                     struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_lambda),
                                        offsetof(struct sgl_lambda, ls));
  t->call_val = call_val;
  t->cmp_val = cmp_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  return t;
}

#include <string.h>

#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/starch.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/starch.h"
#include "snigl/val.h"

static bool clone_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_starch
    *src = sgl_baseof(struct sgl_starch, buf, val->as_buf),
    *dst = sgl_starch_new(sgl);

  struct sgl_buf *sb = &src->buf, *db = &dst->buf;
  sgl_buf_grow(db, sb->len);
  memcpy(db->data, sb->data, sb->len);
  out->as_buf = &dst->buf;
  return true;
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  struct sgl_starch *s = sgl_baseof(struct sgl_starch, buf, val->as_buf);
  sgl_starch_deref(s, sgl);
}

static struct sgl_iter *iter_val(struct sgl_val *val,
                                 struct sgl *sgl,
                                 struct sgl_type **type) {
  if (type) { *type = sgl->StarchIter; }
  struct sgl_starch *s = sgl_baseof(struct sgl_starch, buf, val->as_buf);
  return &sgl_starch_iter_new(s, sgl)->iter;
}

struct sgl_type *sgl_starch_type_new(struct sgl *sgl,
                                       struct sgl_pos *pos,
                                       struct sgl_lib *lib,
                                       struct sgl_sym *id,
                                       struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_starch),
                                        offsetof(struct sgl_starch, buf) +
                                        offsetof(struct sgl_buf, ls));
  t->clone_val = clone_val;
  t->iter_val = iter_val;
  t->deinit_val = deinit_val;
  return t;
}

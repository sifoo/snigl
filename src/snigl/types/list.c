#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/list.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/list.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) {
  struct sgl_ls *root = &val->as_list->root;
  return root->next != root;
}

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->List, %s);",
                 id, root ? root : "NULL");
  
  sgl_cemit_line(out, "%s->as_list = sgl_list_new(sgl);", id);
  struct sgl_ls *l = &val->as_list->root;

  if (l->next != l) {
    struct sgl_sym *lid = sgl_gsym(sgl), *vid = sgl_gsym(sgl);
    sgl_cemit_line(out, "struct sgl_ls *%s = &%s->as_list->root;", lid->id, id);
    sgl_cemit_line(out, "struct sgl_val *%s = NULL;", vid->id);

    sgl_ls_do(&val->as_list->root, struct sgl_val, ls, v) {
      if (!sgl_val_cemit(v, sgl, pos, vid->id, lid->id, out)) { return false; }
    }
  }
  
  return true;
}

static bool clone_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_list *src = val->as_list, *dst = sgl_list_new(sgl);

  sgl_ls_do(&src->root, struct sgl_val, ls, v) {
    if (!sgl_val_clone(v, sgl, pos, &dst->root)) {
      sgl_list_deref(dst, sgl);
      return false;
    }
  }
  
  out->as_list = dst;
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *val,
                            struct sgl *sgl,
                            const struct sgl_val *rhs) {
  struct sgl_ls
    *val_root = &val->as_list->root,
    *rhs_root = &rhs->as_list->root;

  struct sgl_ls *i = val_root->next, *j = rhs_root->next;
  
  for (; i != val_root && j != rhs_root; i = i->next, j = j->next) {
    enum sgl_cmp cmp = sgl_val_cmp(sgl_baseof(struct sgl_val, ls, i),
                                   sgl,
                                   sgl_baseof(struct sgl_val, ls, j));
    
    if (cmp != SGL_EQ) { return cmp; }
  }

  return (i == val_root) ? ((j == rhs_root) ? SGL_EQ : SGL_LT) : SGL_GT;
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_list_deref(val->as_list, sgl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_putc(out, '[');
  char sep = 0;
  
  sgl_ls_do(&val->as_list->root, struct sgl_val, ls, v) {
    if (sep) { sgl_buf_putc(out, sep); }
    sgl_val_dump(v, out);
    sep = ' ';
  }

  sgl_buf_putc(out, ']');
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_list *l = out->as_list = val->as_list;
  l->nrefs++;
}

static bool eq_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  struct sgl_ls
    *val_root = &val->as_list->root,
    *rhs_root = &rhs->as_list->root;

  struct sgl_ls *i = val_root->next, *j = rhs_root->next;
  
  while (i != val_root && j != rhs_root) {
    if (!sgl_val_eq(sgl_baseof(struct sgl_val, ls, i),
                    sgl,
                    sgl_baseof(struct sgl_val, ls, j))) { return false; }
    
    i = i->next;
    j = j->next;
  }

  return i == val_root && j == rhs_root;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_list == rhs->as_list;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return (sgl_uint_t)v->as_list; }

static struct sgl_iter *iter_val(struct sgl_val *val,
                                 struct sgl *sgl,
                                 struct sgl_type **type) {
  if (type) { *type = sgl->ListIter; }
  struct sgl_list *l = val->as_list;

  return
    &sgl_list_iter_new(sgl, sgl_val_dup(val, sgl, NULL), &l->root, &l->root)->iter;
}

static struct sgl_val *load_val(struct sgl_starch *in,
                                struct sgl *sgl,
                                enum sgl_store_id id,
                                struct sgl_ls *root) {
  sgl_uint_t len = 0;
  struct sgl_val *v = sgl_val_new(sgl, sgl->List, root);
  struct sgl_list *l = v->as_list = sgl_list_new(sgl);
  struct sgl_buf *ib = &in->buf;
  
  switch (id) {
  case SGL_STORE_LIST_EMPTY:
    return v;
  case SGL_STORE_LIST_SINGLE:
    len = 1;
    break;
  case SGL_STORE_LIST: {
    enum sgl_store_id len_id = sgl_load_id(ib);

    if (len_id != SGL_STORE_INVALID &&
        sgl_load_int(len_id, ib, sgl, (sgl_int_t *)&len)) { break; }
  }
  default:
    sgl_ls_delete(&v->ls);
    sgl_val_free(v, sgl);
    return NULL;
  }

  while (len--) {
    if (!sgl_starch_load(in, sgl, &l->root)) {
      sgl_ls_delete(&v->ls);
      sgl_val_free(v, sgl);
      return NULL;
    }
  }
  
  return v;
}

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  sgl_ls_do(&val->as_list->root, struct sgl_val, ls, v) {
    sgl_val_print(v, sgl, pos, out);
  }
}

static bool store_val(struct sgl_val *v, struct sgl *sgl, struct sgl_starch *out) {
  struct sgl_ls *l = &v->as_list->root;
  sgl_uint_t len = sgl_ls_len(l);
  struct sgl_buf *ob = &out->buf;
  
  switch (len) {
  case 0:
    sgl_store_id(SGL_STORE_LIST_EMPTY, ob);
    return true;
  case 1:
    sgl_store_id(SGL_STORE_LIST_SINGLE, ob);
    return sgl_starch_store(out, sgl, sgl_baseof(struct sgl_val, ls, l->next));
  default:
    sgl_store_id(SGL_STORE_LIST, ob);
    sgl_store_int(len, ob);

    sgl_ls_do(l, struct sgl_val, ls, v) {
      if (!sgl_starch_store(out, sgl, v)) { return false; }
    }
  }
  
  return true;
}

struct sgl_type *sgl_list_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_list),
                                        offsetof(struct sgl_list, ls));
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->clone_val = clone_val;
  t->cmp_val = cmp_val;
  t->eq_val = eq_val;
  t->hash_val = hash_val;
  t->is_val = is_val;
  t->iter_val = iter_val;
  t->load_val = load_val;
  t->print_val = print_val;
  t->store_val = store_val;
  return t;
}

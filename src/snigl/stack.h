#ifndef SNIGL_STACK_H
#define SNIGL_STACK_H

#include <stdbool.h>
#include <stdio.h>
#include "snigl/int.h"

struct sgl;
struct sgl_ls;

sgl_int_t sgl_stack_drop(struct sgl_ls *stack,
                         struct sgl *sgl,
                         sgl_int_t nvals,
                         bool silent);

struct sgl_val *sgl_stack_dup(struct sgl_ls *stack, struct sgl *sgl, bool silent);
struct sgl_val *sgl_stack_dupl(struct sgl_ls *stack, struct sgl *sgl, bool silent);
bool sgl_stack_rotl(struct sgl_ls *stack, struct sgl *sgl, bool silent);
bool sgl_stack_rotr(struct sgl_ls *stack, struct sgl *sgl, bool silent);
bool sgl_stack_swap(struct sgl_ls *stack, struct sgl *sgl, bool silent);
void sgl_stack_dump(struct sgl_ls *root, FILE *out);

#endif

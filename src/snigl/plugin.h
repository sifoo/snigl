#include "snigl/config.h"

#ifdef SGL_USE_DL
#ifndef SNIGL_PLUGIN_H
#define SNIGL_PLUGIN_H

#include <stdbool.h>

struct sgl;
struct sgl_pos;

bool sgl_link(struct sgl *sgl, struct sgl_pos *pos, const char *path);
void sgl_deinit_links(struct sgl *sgl);

#endif
#endif

#include <assert.h>

#include "snigl/enum.h"
#include "snigl/lib.h"
#include "snigl/libs/enum.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"

static bool int_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = v->as_enum.i;
  return true;
}

static bool sym_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_val_reuse(v, sgl, sgl->Sym, false)->as_sym = v->as_enum.id;
  return true;
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  if (!sgl_use(sgl, pos, sgl->abc_lib,
               sgl_sym(sgl, "Enum"),
               sgl_sym(sgl, "Int"),
               sgl_sym(sgl, "Sym"))) { return false; }

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "int"),
                    int_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Enum));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "sym"),
                    sym_imp,
                    sgl_rets(sgl->Sym),
                    sgl_arg(sgl->Enum));

  return sgl->errors.next == &sgl->errors;
}

struct sgl_lib *sgl_enum_lib_new(struct sgl *sgl, struct sgl_pos *pos) {
  struct sgl_lib *l = sgl_lib_new(sgl, pos, sgl_sym(sgl, "enum"));
  l->init = lib_init;
  return l;
}

#include <assert.h>

#include "snigl/iters/rand.h"
#include "snigl/lib.h"
#include "snigl/libs/math.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"

static bool new_rand_iter_imp(struct sgl_fimp *fimp,
                              struct sgl *sgl,
                              struct sgl_op **rpc) {
  struct sgl_val *max = sgl_pop(sgl), *seed = sgl_peek(sgl);

  struct sgl_rand_iter *ri =
    sgl_rand_iter_new(sgl, (max->type == sgl->Nil) ? 0 : max->as_int);
  
  sgl_prng_init(&ri->gen, (seed->type == sgl->Nil) ? sgl_prng_seed() : seed->as_int);
  sgl_val_reuse(seed, sgl, sgl->math_lib.RandIter, false)->as_iter = &ri->iter;
  sgl_val_free(max, sgl);
  return true;
}

static bool rand_imp(struct sgl_fimp *fimp,
                     struct sgl *sgl,
                     struct sgl_op **rpc) {
  struct sgl_val *max = sgl_peek(sgl);
  sgl_uint_t n = sgl_prng_next(&sgl->math_lib.prng);
  
  if (max->type == sgl->Nil) {
    sgl_val_reuse(max, sgl, sgl->Int, false)->as_int = sgl_abs(n);
  } else {
    max->as_int = sgl_abs(n % max->as_int);
  }
  
  return true;
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  if (!sgl_use(sgl, pos, sgl->abc_lib,
               sgl_sym(sgl, "Int"),
               sgl_sym(sgl, "Nil|Int"))) { return false; }

  struct sgl_math_lib *ml = sgl_baseof(struct sgl_math_lib, lib, l);  
  sgl_prng_init(&ml->prng, sgl_prng_seed());
  struct sgl_type *OptInt = sgl_type_opt(sgl->Int, sgl);

  ml->RandIter = sgl_ref_type_new(sgl, pos, l,
                                  sgl_sym(sgl, "RandIter"),
                                  sgl_types(sgl->Iter),
                                  sizeof(struct sgl_rand_iter),
                                  offsetof(struct sgl_rand_iter, iter) +
                                  offsetof(struct sgl_iter, ls));
  
  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "new-rand-iter"),
                    new_rand_iter_imp,
                    sgl_rets(ml->RandIter),
                    sgl_arg(OptInt), sgl_arg(OptInt));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "rand"),
                    rand_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int));

  return sgl->errors.next == &sgl->errors;
}

struct sgl_lib *sgl_math_lib_init(struct sgl_math_lib *ml, struct sgl *sgl) {
  struct sgl_lib *l = &ml->lib;
  sgl_lib_init(l, sgl, sgl_pos(sgl), sgl_sym(sgl, "math"));
  l->init = lib_init;
  return l;
}

#ifndef SNIGL_LIBS_LIST_H
#define SNIGL_LIBS_LIST_H

struct sgl_lib;

struct sgl_lib *sgl_list_lib_new(struct sgl *sgl, struct sgl_pos *pos);

#endif

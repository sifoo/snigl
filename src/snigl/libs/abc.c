#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "snigl/config.h"

#include "snigl/buf.h"
#include "snigl/error.h"
#include "snigl/file.h"
#include "snigl/form.h"
#include "snigl/io.h"
#include "snigl/iters/filter.h"
#include "snigl/iters/int.h"
#include "snigl/iters/map.h"
#include "snigl/lib.h"
#include "snigl/libs/abc.h"
#include "snigl/list_set.h"
#include "snigl/macro.h"
#include "snigl/parse.h"
#include "snigl/plugin.h"
#include "snigl/ref.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/struct.h"
#include "snigl/sym.h"
#include "snigl/time.h"
#include "snigl/types/bool.h"
#include "snigl/types/buf.h"
#include "snigl/types/char.h"
#include "snigl/types/curry.h"
#include "snigl/types/enum.h"
#include "snigl/types/error.h"
#include "snigl/types/file.h"
#include "snigl/types/fix.h"
#include "snigl/types/form.h"
#include "snigl/types/func.h"
#include "snigl/types/int.h"
#include "snigl/types/iter.h"
#include "snigl/types/lambda.h"
#include "snigl/types/list.h"
#include "snigl/types/list_set.h"
#include "snigl/types/meta.h"
#include "snigl/types/nil.h"
#include "snigl/types/ref.h"
#include "snigl/types/rgb.h"
#include "snigl/types/pair.h"
#include "snigl/types/starch.h"
#include "snigl/types/str.h"
#include "snigl/types/struct.h"
#include "snigl/types/sym.h"
#include "snigl/types/time.h"
#include "snigl/types/undef.h"
#include "snigl/types/xy.h"

static struct sgl_ls *lift_form(struct sgl_form *form,
                                struct sgl *sgl,
                                struct sgl_ls *root) {
  sgl_ls_do(&form->as_body.forms, struct sgl_form, ls, f) {
    struct sgl_val *v = sgl_val_new(sgl, sgl->Form, NULL);
    v->as_form = f;
    f->nrefs++;
    sgl_op_push_new(sgl, f, v);
  }
  
  return form->ls.next;
}

static const char *nop_imp(struct sgl_pmacro *m,
                           const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           char end_char,
                           struct sgl_ls *out) {
  sgl_form_nop_new(sgl, pos, out);
  return in;
}

static const char *trim_source(const char *in, int64_t *len) {
  const char *i = in, *j = in + *len-1;
  
  while (i < j) {
    if (*i == ' ' || *i == '(' || *i == ',') {
      i++;
      continue;
    }

    if (*j == ' ' || *j == ')') {
      j--;
      continue;
    }

    break;
  }

  *len = j - i + 1;
  return i;
}

static const char *source_imp(struct sgl_pmacro *m,
                              const char *in,
                              struct sgl *sgl,
                              struct sgl_pos *pos,
                              char end_char,
                              struct sgl_ls *out) {
  struct sgl_pos start_pos = *pos;
  struct sgl_ls tmp;
  sgl_ls_init(&tmp);
  
  const char *end = sgl_parse_form(in, sgl, pos, end_char, &tmp);
  if (!end) { return NULL; }
  
  if (tmp.next == &tmp) {
    sgl_error(sgl, &start_pos, sgl_strdup("Missing arg"));
    return NULL;
  }
    
  struct sgl_form *f = sgl_baseof(struct sgl_form, ls, sgl_ls_pop(&tmp));
  sgl_int_t len = end - in;
  in = trim_source(in, &len);
  sgl_form_source_new(sgl, &start_pos, out, f, sgl_strndup(in, len));
  return end;
}

static const char *nop_source_imp(struct sgl_pmacro *m,
                                  const char *in,
                                  struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  char end_char,
                                  struct sgl_ls *out) {
  struct sgl_pos start_pos = *pos;
  struct sgl_ls tmp;
  sgl_ls_init(&tmp);

  const char *end = sgl_parse_form(in, sgl, pos, end_char, &tmp);
  if (!end) { return NULL; }
  
  if (tmp.next == &tmp) {
    sgl_error(sgl, &start_pos, sgl_strdup("Missing arg"));
    return NULL;
  }
    
  sgl_form_deref(sgl_baseof(struct sgl_form, ls, sgl_ls_pop(&tmp)), sgl);
  sgl_int_t len = end - in;
  in = trim_source(in, &len);
  struct sgl_val *v = sgl_val_new(sgl, sgl->Str, NULL);
  v->as_str = sgl_str_new(sgl, in, len);
  sgl_form_lit_new(sgl, &start_pos, out, v);
  return end;
}

static struct sgl_ls *pair_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {  
  struct sgl_form *last = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(last, sgl)) { return NULL; }
  sgl_op_pair_new(sgl, form);
  return last->ls.next;
}

static struct sgl_ls *bench_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_op *op = sgl_op_bench_new(sgl, form);
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, arg);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  op->as_bench.end_pc = sgl_op_count_new(sgl, form, op);
  return arg->next;
}

static struct sgl_ls *curry_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_form *expr = sgl_baseof(struct sgl_form, ls, form->ls.next);
  sgl_op_list_beg_new(sgl, form);
  struct sgl_list *args = NULL; 
  sgl_lift(sgl, lift_form) { args = sgl_form_eval(expr, sgl); }  
  if (!args) { return NULL; }

  sgl_ls_do(&args->root, struct sgl_val, ls, a) {
    if (a->type == sgl->Form) {
      sgl_op_stack_switch_new(sgl, form, -1, false);
      if (!sgl_form_compile(a->as_form, sgl)) { return NULL; }
      sgl_op_stack_switch_new(sgl, form, 1, true);
    } else {
      sgl_op_push_new(sgl, form, a);
      sgl_ls_delete(&a->ls);
    }
  }

  sgl_list_deref(args, sgl);
  sgl_op_list_end_new(sgl, form);
  sgl_op_curry_new(sgl, form);
  return expr->ls.next;
}

static struct sgl_ls *debug_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl->debug = true;
  return form->ls.next;
}

static struct sgl_ls *defer_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_op *op = sgl_op_defer_new(sgl, form);
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  op->as_defer.end_pc = sgl->end_pc->prev;
  return body->ls.next;
}

static struct sgl_ls *doc_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_form_type *idt = id_form->type;
  struct sgl_lib *lib = sgl_lib(sgl);
  struct sgl_def *def = NULL;

  if (idt == &SGL_FORM_ID) {
    struct sgl_sym *id = id_form->as_id.val;
    def = sgl_lib_find(lib, sgl, pos, id);
    
    if (!def) {
      sgl_error(sgl, pos, sgl_sprintf("Unknown def: %s", id->id));
      return NULL;        
    }
  } else if (idt != &SGL_FORM_NOP) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid doc id: %s", idt->id));
    return NULL;
  }
  
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, id_form->ls.next);
  struct sgl_list *vals = sgl_form_eval(body, sgl);
  if (!vals) { return NULL; }
  struct sgl_buf *buf = def ? &def->docs : &lib->docs;
  if (buf->len) { sgl_buf_putc(buf, '\n'); }
  sgl_ls_do(&vals->root, struct sgl_val, ls, v) { sgl_val_print(v, sgl, pos, buf); }
  return body->ls.next;
}

static struct sgl_ls *drop_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_drop_new(sgl, form, 1);
  return form->ls.next;
}

static struct sgl_ls *drop2_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl_op_drop_new(sgl, form, 2);
  return form->ls.next;
}

static struct sgl_ls *dup_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  sgl_op_dup_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *dupl_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_dupl_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *else_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_op *skip = sgl_op_if_new(sgl, form, NULL);
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  skip->as_if.pc = sgl->end_pc->prev;
  return body->ls.next;
}

static struct sgl_ls *enum_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_lib *lib = sgl_lib(sgl);
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid enum id: %s", id_form->type->id));
    return NULL;    
  }

  struct sgl_form *p_form = sgl_baseof(struct sgl_form, ls, id_form->ls.next);
  struct sgl_list *p_list = sgl_form_eval(p_form, sgl);
  if (!p_list) { return NULL; }
  sgl_int_t nps = sgl_list_len(p_list);
  struct sgl_type *ps[nps + 2];
  ps[0] = sgl->T;
  ps[nps+1] = NULL;
  struct sgl_type **pst = ps+1;
  
  sgl_ls_do(&p_list->root, struct sgl_val, ls, p) {
    if (!sgl_derived(p->type, sgl->Meta)) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid enum parent: %s", sgl_type_id(p->type)->id));
      
      sgl_list_deref(p_list, sgl);
      return NULL;
    }

    if (!p->as_type->is_trait) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid enum parent: %s", sgl_type_id(p->as_type)->id));
      
      sgl_list_deref(p_list, sgl);
      return NULL;
    }

    *pst++ = p->as_type;
  }

  sgl_list_deref(p_list, sgl);
  struct sgl_form *alt_form = sgl_baseof(struct sgl_form, ls, p_form->ls.next);
  struct sgl_list *alt_list = sgl_form_eval(alt_form, sgl);
  if (!alt_list) { return NULL; }
  sgl_int_t nalts = sgl_list_len(alt_list);
  struct sgl_sym *alts[nalts + 1];
  alts[nalts] = NULL;
  struct sgl_sym **alt_dst = alts;

  sgl_ls_do(&alt_list->root, struct sgl_val, ls, alt) {
    if (alt->type != sgl->Sym) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid enum alt: %s", sgl_type_id(alt->type)->id));
      
      sgl_list_deref(alt_list, sgl);
      return NULL;
    }

    *alt_dst++ = alt->as_sym;
  }

  sgl_list_deref(alt_list, sgl);  
  struct sgl_sym *id = id_form->as_id.val;
  struct sgl_type *t = _sgl_enum_type_new(sgl, pos, lib, id, ps, alts);
  if (!t) { return NULL; }
  return alt_form->ls.next;
}

static struct sgl_ls *for_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_op *op = sgl_op_for_new(sgl, form, NULL);
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_op *beg_pc = sgl->end_pc->prev;
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_iter_new(sgl, form, beg_pc);
  op->as_for.end_pc = sgl->end_pc->prev;
  return body->ls.next;
}

static struct sgl_ls *func_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_form *args_form = sgl_baseof(struct sgl_form, ls, id_form->ls.next);
  
  struct sgl_list *arg_list = sgl_form_eval(args_form, sgl);
  if (!arg_list) { return NULL; }
  sgl_int_t nargs = sgl_list_len(arg_list);
  struct sgl_arg args[SGL_MAX_ARGS];
  struct sgl_ls *al = arg_list->root.next;
  
  for (struct sgl_arg *a = args;
       a < args + nargs && al != &arg_list->root;
       a++, al = al->next) {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, al);

    if (sgl_derived(v->type, sgl->Meta)) {
      sgl_arg_init(a, v->as_type, NULL);
    } else {
      sgl_arg_init(a, NULL, v);
    }
  }

  sgl_ls_init(&arg_list->root);
  sgl_list_deref(arg_list, sgl);

  struct sgl_form *rets_form = sgl_baseof(struct sgl_form, ls, args_form->ls.next);  
  sgl_int_t nrets = -1;
  struct sgl_type **rets = NULL;

  if (rets_form->type != &SGL_FORM_NOP) {
    struct sgl_list *ret_list = sgl_form_eval(rets_form, sgl);
    if (!ret_list) { return NULL; }
    nrets = sgl_list_len(ret_list);
    rets = malloc(sizeof(struct sgl_type *) * (nrets + 1));
    rets[nrets] = NULL;
    struct sgl_ls *rl = ret_list->root.next;
    
    for (struct sgl_type **r = rets;
         r < rets + nrets && rl != &ret_list->root;
         r++, rl = rl->next) {
      struct sgl_val *v = sgl_baseof(struct sgl_val, ls, rl);
      
      if (!sgl_derived(v->type, sgl->Meta)) {
        sgl_error(sgl, &form->pos,
                  sgl_sprintf("Invalid func ret: %s", sgl_type_id(v->type)->id));
        
        sgl_list_deref(ret_list, sgl);
        free(rets);
        return NULL;
      }

      *r = v->as_type;
    }

    sgl_list_deref(ret_list, sgl);
  }

  struct sgl_fimp *fimp = _sgl_lib_add_func(sgl_lib(sgl), sgl, &form->pos,
                                            id_form->as_id.val,
                                            nargs, args, rets);

  if (rets) { free(rets); }
  if (!fimp) { return NULL; }
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, rets_form->ls.next);  
  sgl_imp_init(&fimp->imp, body, 0);
  if (!sgl_fimp_compile(fimp, sgl, &form->pos)) { return NULL; }
  return body->ls.next;
}

static struct sgl_ls *get_set_imp(struct sgl_macro *macro,
                                  struct sgl_form *form,
                                  struct sgl_ls *root,
                                  struct sgl *sgl) {
  sgl_op_deref_beg_new(sgl, form);
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_deref_end_new(sgl, form);
  return body->ls.next;
}

static struct sgl_ls *idle_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_op *op = sgl_op_idle_new(sgl, form);
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_yield_new(sgl, form);
  op->as_idle.end_pc = sgl_op_jump_new(sgl, form, op->prev);
  return body->ls.next;
}

static struct sgl_ls *if_imp(struct sgl_macro *macro,
                             struct sgl_form *form,
                             struct sgl_ls *root,
                             struct sgl *sgl) {
  struct sgl_op *skip = sgl_op_if_new(sgl, form, NULL);
  skip->as_if.neg = true;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  skip->as_if.pc = sgl->end_pc->prev;
  return body->ls.next;
}

static struct sgl_ls *if_else_imp(struct sgl_macro *macro,
                                  struct sgl_form *form,
                                  struct sgl_ls *root,
                                  struct sgl *sgl) {
  struct sgl_op *else_skip = sgl_op_if_new(sgl, form, NULL);
  else_skip->as_if.neg = true;
  struct sgl_form *lhs = sgl_baseof(struct sgl_form, ls, form->ls.next);
  if (!sgl_form_compile(lhs, sgl)) { return NULL; }
  struct sgl_op *if_skip = sgl_op_jump_new(sgl, form, NULL);
  struct sgl_form *rhs = sgl_baseof(struct sgl_form, ls, lhs->ls.next);
  else_skip->as_if.pc = sgl->end_pc->prev;
  if (!sgl_form_compile(rhs, sgl)) { return NULL; }
  if_skip->as_jump.pc = sgl->end_pc->prev;
  return rhs->ls.next;
}

static struct sgl_ls *include_imp(struct sgl_macro *macro,
                                  struct sgl_form *form,
                                  struct sgl_ls *root,
                                  struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *path_form = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *paths = sgl_form_eval(path_form, sgl);
  if (!paths) { return NULL; }
  bool ok = false;
  
  sgl_ls_do(&paths->root, struct sgl_val, ls, p) {
    if (p->type != sgl->Str) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid include argument: %s",
                            sgl_type_id(p->type)->id));
      

      goto exit;
    }

    char *lp = sgl_load_path(sgl, sgl_str_cs(p->as_str));
    if (!sgl_load(sgl, pos, lp)) { goto exit; }
  }

  ok = true;
 exit:
  sgl_list_deref(paths, sgl);
  return ok ? path_form->ls.next : NULL;
}

static struct sgl_ls *let_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_form *place = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *ids = NULL;
  
  sgl_lift(sgl, lift_form) { ids = sgl_form_eval(place, sgl); }
  if (!ids) { return NULL; }

  sgl_ls_do(&ids->root, struct sgl_val, ls, id) {
    struct sgl_val *val = NULL;
    
    if (id->type == sgl->Pair) {
      struct sgl_val *v = id->as_pair.last;

      if (v->type == sgl->Form) {
        if (!sgl_form_compile(v->as_form, sgl)) { return NULL; }
      } else {
        val = sgl_val_dup(v, sgl, NULL);
      }

      id = id->as_pair.first;
    } else if (id->type == sgl->Form) {
      if (!sgl_form_compile(id->as_form, sgl)) { return NULL; }
      continue;
    }
    
    if (id->type == sgl->Sym) {
      sgl_op_let_var_new(sgl, form, id->as_sym)->as_let_var.val = val;
    } else {
      sgl_error(sgl, &form->pos,
                sgl_sprintf("Invalid let place: %s", id->type->def.id->id));
      
      return NULL;
    }
  }
  
  sgl_list_deref(ids, sgl);
  return place->ls.next;
}

static struct sgl_ls *lib_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid lib id: %s", id_form->type->id));
    return NULL;
  }

  struct sgl_lib *lib = sgl_lib_new(sgl,pos, id_form->as_id.val);
  sgl_add_lib(sgl, pos, lib);
  struct sgl_op *beg_pc = sgl_op_lib_beg_new(sgl, form, lib);
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, id_form->ls.next);
  sgl_beg_lib(sgl, lib);
  if (!sgl_use(sgl, pos, sgl->abc_lib, sgl_sym(sgl, "use:"))) { return NULL; }
  bool ok = sgl_form_compile(body, sgl);
  sgl_end_lib(sgl);
  if (!ok) { return NULL; }
  beg_pc->as_lib_beg.end_pc = sgl_op_lib_end_new(sgl, form, beg_pc);
  return body->ls.next;
}

static struct sgl_ls *nop_expr_imp(struct sgl_macro *macro,
                                   struct sgl_form *form,
                                   struct sgl_ls *root,
                                   struct sgl *sgl) {
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  return body->ls.next;
}

#ifdef SGL_USE_DL

static struct sgl_ls *link_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *out = sgl_form_eval(body, sgl);
  if (!out) { return NULL; }
  
  sgl_ls_do(&out->root, struct sgl_val, ls, v) {
    if (v->type != sgl->Str) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid link argument: %s", sgl_type_id(v->type)->id));

      sgl_list_deref(out, sgl);
      return NULL;
    }
    
    if (!sgl_link(sgl, pos, sgl_str_cs(v->as_str))) {
      sgl_list_deref(out, sgl);
      return NULL;
    }
  }
  
  sgl_list_deref(out, sgl);
  return body->ls.next;
}

#endif

static struct sgl_ls *mem_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *fids = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *ids = sgl_form_eval(fids, sgl);
  if (!ids) { return NULL; }

  if (sgl_list_len(ids) != 1) {
    sgl_error(sgl, pos, sgl_strdup("Invalid mem id"));
    return NULL;
  }

  struct sgl_val *idv = sgl_baseof(struct sgl_val, ls, sgl_ls_pop(&ids->root));

  if (idv->type != sgl->Func) {
    sgl_error(sgl, pos,
              sgl_sprintf("Invalid mem func: %s", sgl_type_id(idv->type)->id));
    
    return NULL;
  }

  struct sgl_func *f = idv->as_func;

  if (!f->nargs || f->nrets <= 0) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid mem func: %s", f->def.id->id));
    return NULL;
  }

  struct sgl_op_mem_beg *beg = &sgl_op_mem_beg_new(sgl, form, f)->as_mem_beg;
  sgl_op_dispatch_new(sgl, form, f, NULL);
  beg->end_pc = sgl_op_mem_end_new(sgl, form, beg);
  return fids->ls.next;
}

static struct sgl_ls *new_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  sgl_op_new_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *or_imp(struct sgl_macro *macro,
                             struct sgl_form *form,
                             struct sgl_ls *root,
                             struct sgl *sgl) {
  struct sgl_form *rhs = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_op *skip = sgl_op_if_new(sgl, form, NULL);
  skip->as_if.pop = false;
  if (!sgl_form_compile(rhs, sgl)) { return NULL; }
  skip->as_if.pc = sgl->end_pc->prev;
  return rhs->ls.next;
}

static struct sgl_ls *and_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_form *rhs = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_op *if_false = sgl_op_if_new(sgl, form, NULL);
  if_false->as_if.neg = true;
  if_false->as_if.val = sgl_val_dup(&sgl->nil, sgl, NULL);
  if (!sgl_form_compile(rhs, sgl)) { return NULL; }
  if_false->as_if.pc = sgl->end_pc->prev;
  return rhs->ls.next;
}

static struct sgl_ls *pre_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *out = NULL;
  sgl_lift(sgl, lift_form) { out = sgl_form_eval(body, sgl); }
  if (!out) { return NULL; }
  
  sgl_ls_do(&out->root, struct sgl_val, ls, v) {
    if (v->type == sgl->Form) {
      if (!sgl_form_compile(v->as_form, sgl)) { return NULL; }
    } else {
      sgl_op_push_new(sgl, form, v);
    }
  }
  
  sgl_ls_init(&out->root);
  sgl_list_deref(out, sgl);
  return body->ls.next;
}

static struct sgl_ls *put_field(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl,
                                bool drop) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *place = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_list *ids = NULL;
  
  sgl_lift(sgl, lift_form) { ids = sgl_form_eval(place, sgl); }
  if (!ids) { return NULL; }

  sgl_int_t nvars = 0;
  sgl_ls_do(&ids->root, struct sgl_val, ls, v) {
    if (v->type == sgl->Sym || v->type == sgl->Pair) { nvars++; }
  }

  bool use_reg = nvars > 1;

  if (use_reg) {
    sgl_op_push_reg_new(sgl, form);
  } else if (!drop) {
    sgl_op_dup_new(sgl, form);
  }
  
  sgl_ls_do(&ids->root, struct sgl_val, ls, id) {
    struct sgl_val *val = NULL;
    
    if (id->type == sgl->Pair) {
      struct sgl_val *v = id->as_pair.last;

      if (v->type == sgl->Form) {
        if (!sgl_form_compile(v->as_form, sgl)) { return NULL; }
      } else {
        val = sgl_val_dup(v, sgl, NULL);
      }

      id = id->as_pair.first;
    } else if (id->type == sgl->Form) {
      if (!sgl_form_compile(id->as_form, sgl)) { return NULL; }
      continue;
    }

    if (id->type == sgl->Sym && id->as_sym->id[0] == '.') {
      if (!use_reg && !drop && !val) { sgl_op_rotl_new(sgl, form); }
      struct sgl_sym *fid = sgl_sym(sgl, id->as_sym->id+1);
      struct sgl_op *op = sgl_op_put_field_new(sgl, form, fid, val);
      if (!op) { return NULL; }
      op->as_field.use_reg = use_reg;
    } else {
      if (use_reg) { sgl_op_peek_reg_new(sgl, form); }
      sgl_op_push_new(sgl, form, sgl_val_dup(id, sgl, NULL));

      if (val) {
        sgl_op_push_new(sgl, form, sgl_val_dup(val, sgl, NULL));
      } else {
        sgl_op_rotr_new(sgl, form);
      }
      
      struct sgl_func *func = sgl_find_func(sgl, pos, sgl_sym(sgl, "put"));
      assert(func);
      sgl_op_dispatch_new(sgl, form, func, NULL);
    }
  }
  
  if (use_reg) { sgl_op_pop_reg_new(sgl, form, drop); }
  sgl_list_deref(ids, sgl);
  return place->ls.next;
}

static struct sgl_ls *put_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  return put_field(macro, form, root, sgl, true);
}

static struct sgl_ls *dup_put_imp(struct sgl_macro *macro,
                                  struct sgl_form *form,
                                  struct sgl_ls *root,
                                  struct sgl *sgl) {
  return put_field(macro, form, root, sgl, false);
}

static struct sgl_ls *recall_arg_imp(struct sgl_macro *macro,
                                     struct sgl_form *form,
                                     struct sgl_ls *root,
                                     struct sgl *sgl) {
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, arg);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, &form->pos,
              sgl_sprintf("Invalid recall arg: %s", id_form->type->id));
    
    return NULL;
  }
  
  struct sgl_sym *id = id_form->as_id.val;
  struct sgl_func *func = sgl_find_func(sgl, &form->pos, id);

  if (!func) {
    sgl_error(sgl, &form->pos, sgl_sprintf("Unknown func: %s", id->id));
    return NULL;    
  }
  
  sgl_op_recall_new(sgl, form, func);
  return form->ls.next;
}

static struct sgl_ls *recall_imp(struct sgl_macro *macro,
                                 struct sgl_form *form,
                                 struct sgl_ls *root,
                                 struct sgl *sgl) {
  sgl_op_recall_new(sgl, form, NULL);
  return form->ls.next;
}

static struct sgl_ls *ref_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  sgl_op_ref_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *rotl_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_rotl_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *rotr_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_rotr_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *stdout_imp(struct sgl_macro *macro,
                                 struct sgl_form *form,
                                 struct sgl_ls *root,
                                 struct sgl *sgl) {
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  sgl_op_stdout_beg_new(sgl, form);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_stdout_end_new(sgl, form);
  return body->ls.next;
}

static struct sgl_ls *struct_imp(struct sgl_macro *macro,
                                 struct sgl_form *form,
                                 struct sgl_ls *root,
                                 struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  
  struct sgl_form
    *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next),
    *parent_form = sgl_baseof(struct sgl_form, ls, id_form->ls.next),
    *field_form = sgl_baseof(struct sgl_form, ls, parent_form->ls.next);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid struct id: %s", id_form->type->id));
    return NULL;
  }

  struct sgl_list *parent_list = sgl_form_eval(parent_form, sgl);
  if (!parent_list) { return NULL; }
  sgl_int_t nparents = sgl_list_len(parent_list);
  struct sgl_type *parents[nparents + 2];
  parents[0] = sgl->Struct;
  parents[nparents + 1] = NULL;
  struct sgl_ls *p = parent_list->root.next;
  
  for (sgl_int_t i = 1; i < nparents+1; i++, p = p->next) {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, p);
    struct sgl_type *vt = v->as_type;
    
    if (!vt->is_trait && !sgl_derived(vt, sgl->Struct)) {
      sgl_error(sgl, pos, sgl_sprintf("Invalid struct parent: %s", vt->def.id->id));
      sgl_list_deref(parent_list, sgl);
      return NULL;
    }
    
    parents[i] = vt;
  }

  sgl_list_deref(parent_list, sgl);
  struct sgl_list *field_list = sgl_form_eval(field_form, sgl);
  if (!field_list) { return NULL; }  

  struct sgl_type *t = sgl_struct_type_new(sgl, pos,
                                           sgl_lib(sgl),
                                           id_form->as_id.val,
                                           parents);
  if (!t) { return NULL; }
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
  struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
  
  struct sgl_vec fids;
  sgl_vec_init(&fids, sizeof(struct sgl_sym *));
  struct sgl_ls *fr = &field_list->root;
  bool ok = false;
  
  for (struct sgl_ls *f = fr->next; f != fr; f = f->next) {
    struct sgl_val *fv = sgl_baseof(struct sgl_val, ls, f);
    
    if (fv->type == sgl->Sym) {
      *(struct sgl_sym **)sgl_vec_push(&fids) = fv->as_sym;
    } else {
      struct sgl_val *ft = fv, *iv = NULL;

      if (ft->type == sgl->Pair) {
        struct sgl_pair *p = &fv->as_pair;
        ft = p->first;
        iv = p->last;
        p->last = NULL;
      }
      
      if (sgl_derived(ft->type, sgl->Meta)) {
        if (!fids.len) {
          sgl_error(sgl, pos, sgl_strdup("Missing field name"));
          goto exit;
        }
        
        sgl_vec_do(&fids, struct sgl_sym *, fid) {
          if (!sgl_struct_type_add(st, sgl, pos, *fid, ft->as_type, iv)) {
            goto exit;
          }
        }
        
        fids.len = 0;
      } else {
        sgl_error(sgl, pos,
                  sgl_sprintf("Invalid struct field: %s",
                              sgl_type_id(fv->type)->id));
          
        goto exit;
      }
    }
  }

  sgl_vec_do(&fids, struct sgl_sym *, fid) {
    if (!sgl_struct_type_add(st, sgl, pos, *fid, sgl->A, NULL)) { goto exit; }
  }

  ok = true;
 exit:
  sgl_vec_deinit(&fids);
  sgl_list_deref(field_list, sgl);
  return ok ? field_form->ls.next : NULL;
}

static struct sgl_ls *switch_imp(struct sgl_macro *macro,
                                 struct sgl_form *form,
                                 struct sgl_ls *root,
                                 struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  
  struct sgl_form
    *fexpr = sgl_baseof(struct sgl_form, ls, form->ls.next),    
    *fbody = sgl_baseof(struct sgl_form, ls, fexpr->ls.next),
    *fif = sgl_baseof(struct sgl_form, ls, fbody->ls.next);
  
  if (fbody->type != &SGL_FORM_NOP && fbody->type != &SGL_FORM_EXPR) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid switch body: %s", fbody->type->id));
    return NULL;
  }

  bool has_expr = fexpr->type != &SGL_FORM_NOP;

  if (has_expr) {
    struct sgl_list *expr = NULL;
    sgl_lift(sgl, lift_form) { expr = sgl_form_eval(fexpr, sgl); }
    if (!expr) { return NULL; }
    
    sgl_ls_do(&expr->root, struct sgl_val, ls, v) {
      if (v->type == sgl->Form) {
        if (!sgl_form_compile(v->as_form, sgl)) { return NULL; }
        sgl_val_free(v, sgl);
      } else {
        sgl_op_push_new(sgl, form, v);
      }
    }
    
    sgl_ls_init(&expr->root);
    sgl_list_deref(expr, sgl);
    sgl_op_push_reg_new(sgl, form);
  }
  
  struct sgl_ls *body_forms = &fbody->as_body.forms;
  struct sgl_vec exits;

  if (fbody->type != &SGL_FORM_NOP) {  
    sgl_vec_init(&exits, sizeof(struct sgl_op *));

    for (struct sgl_ls *i = body_forms->next; i != body_forms; i = i->next) {
      if (has_expr) { sgl_op_peek_reg_new(sgl, form); }
      struct sgl_form *f = sgl_baseof(struct sgl_form, ls, i);
      if (!sgl_form_compile(f, sgl)) { return NULL; }
      if ((i = i->next) == body_forms) { break; }
      struct sgl_op *skip = sgl_op_if_new(sgl, form, NULL);
      skip->as_if.neg = true;
      if (!sgl_form_compile(sgl_baseof(struct sgl_form, ls, i), sgl)) { return NULL; }
      struct sgl_op *exit = sgl_op_jump_new(sgl, form, NULL);
      *(struct sgl_op **)sgl_vec_push(&exits) = exit;
      skip->as_if.pc = sgl->end_pc->prev;
    }
  }

  struct sgl_op *skip_if = NULL;

  if (fif->type != &SGL_FORM_NOP) {
    skip_if = sgl_op_jump_new(sgl, form, NULL);
  }
  
  if (fbody->type != &SGL_FORM_NOP) {
    struct sgl_op *exit_pc = sgl->end_pc->prev; 
    sgl_vec_do(&exits, struct sgl_op *, e) { (*e)->as_jump.pc = exit_pc; }
    sgl_vec_deinit(&exits);
  }

  if (fif->type != &SGL_FORM_NOP) {
    if (has_expr) { sgl_op_peek_reg_new(sgl, form); }
    if (!sgl_form_compile(fif, sgl)) { return NULL; }
    skip_if->as_jump.pc = sgl->end_pc->prev;
  }

  if (has_expr) { sgl_op_pop_reg_new(sgl, form, true); }
  sgl_op_fence_new(sgl, form);
  return fif->ls.next;
}

static struct sgl_ls *sync_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  sgl_op_sync_new(sgl, form, 1);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_sync_new(sgl, form, -1);
  return body->ls.next;
}

static struct sgl_ls *task_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, arg);
  struct sgl_op *op = sgl_op_task_beg_new(sgl, form);
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  op->as_task_beg.end_pc = sgl_op_task_end_new(sgl, form, op);
  return arg->next;
}

static struct sgl_ls *throw_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl_op_throw_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *times_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_op *op = sgl_op_times_new(sgl, form, NULL);
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, arg);
  struct sgl_op *beg_pc = sgl->end_pc->prev;
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  op->as_times.end_pc = sgl_op_count_new(sgl, form, beg_pc);
  return arg->next;
}

static struct sgl_ls *try_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_op *op = sgl_op_try_beg_new(sgl, form);
  struct sgl_ls *arg = form->ls.next;
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, arg);
  if (!sgl_form_compile(body, sgl)) { return NULL; }  
  sgl_op_push_new(sgl, form, sgl_val_dup(&sgl->nil, sgl, NULL));
  op->as_try_beg.end_pc = sgl_op_try_end_new(sgl, form);
  return arg->next;
}

static struct sgl_ls *swap_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  sgl_op_swap_new(sgl, form);
  return form->ls.next;
}

static struct sgl_ls *trait_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_lib *lib = sgl_lib(sgl);
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid trait id: %s", id_form->type->id));
    return NULL;
  }

  struct sgl_form *p_form = sgl_baseof(struct sgl_form, ls, id_form->ls.next);
  struct sgl_list *p_list = sgl_form_eval(p_form, sgl);
  if (!p_list) { return NULL; }
  sgl_int_t nps = sgl_list_len(p_list);
  struct sgl_type *ps[nps+1];
  ps[nps] = NULL;
  struct sgl_type **pst = ps;
  
  sgl_ls_do(&p_list->root, struct sgl_val, ls, p) {
    if (!sgl_derived(p->type, sgl->Meta)) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid trait parent: %s", sgl_type_id(p->type)->id));
      
      return NULL;
    }

    if (!p->as_type->is_trait) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid trait parent: %s", sgl_type_id(p->as_type)->id));
      
      return NULL;
    }

    *pst++ = p->as_type;
  }

  sgl_list_deref(p_list, sgl);
  struct sgl_sym *id = id_form->as_id.val;
  struct sgl_type *t = sgl_trait_new(sgl, pos, lib, id, ps);
  if (!t) { return NULL; }
  return p_form->ls.next;
}

static struct sgl_ls *type_get_imp(struct sgl_macro *macro,
                                   struct sgl_form *form,
                                   struct sgl_ls *root,
                                   struct sgl *sgl) {
  sgl_op_type_new(sgl, form);
  return form->ls.next;
}

bool type_conv_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **return_pc) {
  sgl_val_reuse(sgl_peek(sgl), sgl, fimp->rets[0], false);
  return true;
}

static struct sgl_ls *type_imp(struct sgl_macro *macro,
                               struct sgl_form *form,
                               struct sgl_ls *root,
                               struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_lib *lib = sgl_lib(sgl);
  struct sgl_form *id_form = sgl_baseof(struct sgl_form, ls, form->ls.next);

  if (id_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid type id: %s", id_form->type->id));
    return NULL;    
  }

  struct sgl_form *p_form = sgl_baseof(struct sgl_form, ls, id_form->ls.next);
  struct sgl_list *p_list = sgl_form_eval(p_form, sgl);
  if (!p_list) { return NULL; }
  sgl_int_t nps = sgl_list_len(p_list)+1;
  struct sgl_type *ps[nps+1];
  ps[nps] = NULL;
  struct sgl_type **pst = ps;
  
  sgl_ls_do(&p_list->root, struct sgl_val, ls, p) {
    if (!sgl_derived(p->type, sgl->Meta)) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid type parent: %s", sgl_type_id(p->type)->id));
      
      sgl_list_deref(p_list, sgl);
      return NULL;
    }

    if (!p->as_type->is_trait) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid type parent: %s", sgl_type_id(p->as_type)->id));
      
      sgl_list_deref(p_list, sgl);
      return NULL;
    }

    *pst++ = p->as_type;
  }

  sgl_list_deref(p_list, sgl);
  struct sgl_form *imp_form = sgl_baseof(struct sgl_form, ls, p_form->ls.next);
  struct sgl_list *imp_stack = sgl_form_eval(imp_form, sgl);
  if (!imp_stack) { return NULL; }

  if (imp_stack->root.prev == &imp_stack->root) {
    sgl_error(sgl, pos, sgl_strdup("Missing type imp"));
    return NULL;
  }

  if (imp_stack->root.prev->prev != &imp_stack->root) {
    sgl_error(sgl, pos, sgl_strdup("Invalid type imp"));
    return NULL;
  }
  
  struct sgl_val *imp_val =
    sgl_baseof(struct sgl_val, ls, sgl_ls_pop(&imp_stack->root));
  
  if (!sgl_derived(imp_val->type, sgl->Meta) ||
      imp_val->as_type->is_union ||
      imp_val->as_type->is_trait) {
    sgl_error(sgl, pos,
              sgl_sprintf("Invalid type imp: %s", imp_val->type->def.id->id));
    
    return NULL;
  }
  
  struct sgl_type *imp = ps[nps-1] = imp_val->as_type;
  sgl_list_deref(imp_stack, sgl);  
  struct sgl_sym *id = id_form->as_id.val;
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, ps);
  if (!t) { return NULL; }
  
  struct sgl_fimp *f = sgl_lib_add_cfunc(lib, sgl, pos,
                                         sgl_sym_lcase(id, sgl),
                                         type_conv_imp,
                                         sgl_rets(t),
                                         sgl_arg(imp));
  
  if (!f) { return NULL; }
  return imp_form->ls.next;
}

static struct sgl_ls *use_imp(struct sgl_macro *macro,
                              struct sgl_form *form,
                              struct sgl_ls *root,
                              struct sgl *sgl) {
  struct sgl_pos *pos = &form->pos;
  struct sgl_form *src_form = sgl_baseof(struct sgl_form, ls, form->ls.next);

  if (src_form->type != &SGL_FORM_ID) {
    sgl_error(sgl, pos, sgl_sprintf("Invalid lib id: %s", src_form->type->id));
    return NULL;
  }
  
  struct sgl_sym *src_id = src_form->as_id.val;
  struct sgl_lib *src = sgl_find_lib(sgl, pos, src_id);

  if (!src) {
    sgl_error(sgl, pos, sgl_sprintf("Unknown lib: %s", src_id->id));
    return NULL;
  }
                                    
  struct sgl_form *dids_form = sgl_baseof(struct sgl_form, ls, src_form->ls.next);
  struct sgl_list *dids = sgl_form_eval(dids_form, sgl);
  if (!dids) { return NULL; }
  sgl_int_t ndids = sgl_list_len(dids);
  struct sgl_sym *dids_arg[ndids + 1];
  dids_arg[ndids] = NULL;
  struct sgl_sym **arg = dids_arg;
  
  sgl_ls_do(&dids->root, struct sgl_val, ls, v) {
    if (v->type != sgl->Sym) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid def id: %s", sgl_type_id(v->type)->id));

      sgl_list_deref(dids, sgl);
      return NULL;
    }
    
    *arg++ = v->as_sym;
  }

  sgl_list_deref(dids, sgl);
  if (!_sgl_use(sgl, pos, src, dids_arg)) { return NULL; }
  return dids_form->ls.next;
}

static struct sgl_ls *while_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_form *body = sgl_baseof(struct sgl_form, ls, form->ls.next);
  struct sgl_op *beg_pc = sgl->end_pc->prev;
  if (!sgl_form_compile(body, sgl)) { return NULL; }
  sgl_op_if_new(sgl, form, beg_pc);
  return body->ls.next;
}

static struct sgl_ls *yield_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  sgl_op_yield_new(sgl, form);
  return form->ls.next;
}

static bool argv_imp(struct sgl_fimp *fimp,
                     struct sgl *sgl,
                     struct sgl_op **rpc) {
  sgl_val_clone(&sgl->argv, sgl, sgl_pos(sgl), sgl_stack(sgl));
  return true;
}

static bool dump_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_buf *buf = sgl_stdout(sgl);
  sgl_val_dump(v, buf);
  sgl_buf_putc(buf, '\n');
  sgl_val_free(v, sgl);
  return true;
}

static bool docs_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *idv = sgl_peek(sgl);
  struct sgl_sym *id = idv->as_sym;
  struct sgl_def *def = sgl_lib_find(sgl_lib(sgl), sgl, sgl_pos(sgl), id);

  if (def) {
    sgl_val_reuse(idv, sgl, sgl->Str, false)->as_str = sgl_buf_str(&def->docs, sgl);
  } else {
    sgl_val_reuse(idv, sgl, sgl->Nil, false);
  }
  
  return true;
}

static bool eval_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *e = sgl_pop(sgl);
  bool clear = true, ok = false;
  
  struct sgl_ls fs;
  struct sgl_op *beg_pc = sgl->end_pc->prev;
  
  if (!sgl_parse(sgl_str_cs(e->as_str), sgl, sgl_ls_init(&fs)) ||
      !sgl_compile(sgl, &fs) ||
      !sgl_run_task(sgl, sgl->task, beg_pc->next, sgl->end_pc)) {
    struct sgl_op *pc = sgl_throw_errors(sgl);

    if (pc) {
      *rpc = pc;
      goto exit;
    }
  }

  for (struct sgl_op *op = beg_pc->next; op != sgl->end_pc; op = op->next) {
    if (op->type == &SGL_OP_FIMP ||
        op->type == &SGL_OP_LAMBDA ||
        op->type == &SGL_OP_TASK_BEG) {
      clear = false;
      break;
    }
  }

  if (clear) {
    for (struct sgl_op *op = sgl->end_pc->prev, *pop = op->prev;
         op != beg_pc;
         op = pop, pop = op->prev) {
      sgl_ls_delete(&op->ls);
      sgl_op_free(op, sgl);
      sgl->end_pc->pc--;
    }

    beg_pc->next = sgl->end_pc;
    sgl->end_pc->prev = beg_pc;
  }

  ok = true;
 exit:
  sgl_forms_deref(&fs, sgl);
  sgl_val_free(e, sgl);
  return ok;
}

static bool type_sub_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *t = sgl_pop(sgl), *v = sgl_peek(sgl);
  struct sgl_type *vt = v->type;
  sgl_val_reuse(v, sgl, sgl->Bool, true)->as_bool = sgl_derived(vt, t->as_type);
  sgl_val_free(t, sgl);
  return true;
}

static bool sub_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *y = sgl_pop(sgl), *x = sgl_peek(sgl);
  struct sgl_type *xt = x->as_type;
  sgl_val_reuse(x, sgl, sgl->Bool, false)->as_bool = sgl_derived(xt, y->as_type);
  return true;
}

static bool dup_clone_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  return sgl_val_clone(v, sgl, sgl_pos(sgl), v->ls.next);
}

static bool clone_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  bool ok = sgl_val_clone(v, sgl, sgl_pos(sgl), sgl_stack(sgl));
  sgl_val_free(v, sgl);
  return ok;
}

static bool ref_get_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rv = sgl_pop(sgl);
  sgl_val_dup(sgl_ref_get(rv->as_ref, sgl), sgl, sgl_stack(sgl));
  sgl_val_free(rv, sgl);
  return true;
}

static bool ref_set_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl), *rv = sgl_pop(sgl);
  bool ok = sgl_ref_set(rv->as_ref, sgl, v);
  sgl_val_free(rv, sgl);
  return ok;
}

static bool ref_clear_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *r = sgl_pop(sgl);
  bool ok = sgl_ref_set(r->as_ref, sgl, NULL);
  sgl_val_free(r, sgl);
  return ok;
}

static bool call_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  *rpc = sgl_val_call(v, sgl, sgl_pos(sgl), sgl->pc, true);
  sgl_val_free(v, sgl);
  return sgl->errors.next == &sgl->errors;
}

static bool dup_call_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl->cfimp = sgl->call_fimp;
  *rpc = sgl_val_call(v, sgl, sgl_pos(sgl), sgl->pc, true);
  return sgl->errors.next == &sgl->errors;
}

static bool catch_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_error *e = v->as_error;
  
  if (e->val) {
    sgl_val_dup(e->val, sgl, sgl_stack(sgl));
  } else {
    sgl_push(sgl, sgl->Nil);
  }
          
  sgl_val_free(v, sgl);
  return true;
}

static bool bool_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  sgl_push(sgl, sgl->Bool)->as_bool = sgl_val_bool(v, sgl);
  sgl_val_free(v, sgl);
  return true;
}

static bool not_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  sgl_push(sgl, sgl->Bool)->as_bool = !sgl_val_bool(v, sgl);
  sgl_val_free(v, sgl);
  return true;
}

static bool is_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {  
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_pop(sgl);
  sgl_push(sgl, sgl->Bool)->as_bool = sgl_val_is(lhs, rhs);
  return true;
}

static bool nis_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {  
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_pop(sgl);
  sgl_push(sgl, sgl->Bool)->as_bool = !sgl_val_is(lhs, rhs);
  return true;
}

static bool eq_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_eq(lhs, sgl, rhs);
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool ne_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = !sgl_val_eq(lhs, sgl, rhs);
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool lt_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_cmp(lhs, sgl, rhs) == SGL_LT;
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool lte_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_cmp(lhs, sgl, rhs) < SGL_GT;
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool gt_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_cmp(lhs, sgl, rhs) == SGL_GT;
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool gte_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_peek(sgl);

  bool ok = sgl_val_cmp(lhs, sgl, rhs) > SGL_LT;
  sgl_val_reuse(lhs, sgl, sgl->Bool, true)->as_bool = ok;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool int_inc_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  sgl_peek(sgl)->as_int++;
  return true;
}

static bool int_dec_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  sgl_peek(sgl)->as_int--;
  return true;
}

static bool int_add_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int += rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_sub_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int -= rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_mul_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int *= rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_div_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int /= rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_mod_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_peek(sgl)->as_int %= rhs->as_int;
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool int_fix_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *scale = sgl_pop(sgl);
  struct sgl_val *v = sgl_peek(sgl);
  int64_t iv = v->as_int;
  sgl_val_reuse(v, sgl, sgl->Fix, false);
  sgl_fix_init(&v->as_fix, iv, 0, scale->as_int, iv < 0);
  sgl_free(&sgl->val_pool, scale);
  return true;
}

static bool fix_add_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_add(&sgl_peek(sgl)->as_fix, &rhs->as_fix);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_sub_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_sub(&sgl_peek(sgl)->as_fix, &rhs->as_fix);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_mul_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_mul(&sgl_peek(sgl)->as_fix, &rhs->as_fix);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_int_mul_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_int_mul(&sgl_peek(sgl)->as_fix, rhs->as_int);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_div_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl);
  sgl_fix_div(&sgl_peek(sgl)->as_fix, &rhs->as_fix);
  sgl_free(&sgl->val_pool, rhs);
  return true;
}

static bool fix_int_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_int_t iv = sgl_fix_int(&v->as_fix);
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = iv;
  return true;
}

static bool min_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_peek(sgl);

  if (rhs->type != lhs->type) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Expected type %s: %s",
                          lhs->type->def.id->id, rhs->type->def.id->id));

    return false;
  }
  
  if (sgl_val_cmp(lhs, sgl, rhs) == SGL_GT) {
    sgl_pop(sgl);
    sgl_ls_push(sgl_stack(sgl), &rhs->ls);
    sgl_val_free(lhs, sgl);
  } else {
    sgl_val_free(rhs, sgl);
  }
  
  return true;
}

static bool max_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_peek(sgl);

  if (rhs->type != lhs->type) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Expected type %s: %s",
                          lhs->type->def.id->id, rhs->type->def.id->id));

    return false;
  }
  
  if (sgl_val_cmp(lhs, sgl, rhs) == SGL_LT) {
    sgl_pop(sgl);
    sgl_ls_push(sgl_stack(sgl), &rhs->ls);
    sgl_val_free(lhs, sgl);
  } else {
    sgl_val_free(rhs, sgl);
  }
  
  return true;
}

static bool seq_iter_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *in = sgl_pop(sgl);
  struct sgl_type *t = NULL;
  struct sgl_iter *i = sgl_val_iter(in, sgl, &t);
  bool ok = false;
  if (!i) { goto exit; }
  sgl_push(sgl, t)->as_iter = i;
  ok = true;
 exit:
  sgl_val_free(in, sgl);
  return ok;
}

static bool seq_splat_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *in = sgl_pop(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);
  sgl_val_free(in, sgl);
  if (!i) { return false; }
  struct sgl_ls *s = sgl_stack(sgl);
  while (sgl_iter_next_val(i, sgl, sgl_pos(sgl), s));
  sgl_iter_deref(i, sgl);
  return true;
}

static bool iter_next_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *i = sgl_pop(sgl);
  bool ok = false;
  
  struct sgl_val *v =
    sgl_iter_next_val(i->as_iter, sgl, sgl_pos(sgl), sgl_stack(sgl));

  if (sgl->errors.next != &sgl->errors) { goto exit; }
  if (!v) { sgl_push(sgl, sgl->Nil); }
  ok = true;
 exit:
  sgl_val_free(i, sgl);
  return ok;
}

static bool iter_next_drop_imp(struct sgl_fimp *fimp,
                               struct sgl *sgl,
                               struct sgl_op **rpc) {
  struct sgl_val *i = sgl_pop(sgl);
  sgl_iter_skip_vals(i->as_iter, sgl, sgl_pos(sgl), 1);
  sgl_val_free(i, sgl);
  return true;
}

static bool iter_skip_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *n = sgl_pop(sgl), *i = sgl_pop(sgl);
  bool ok = sgl_iter_skip_vals(i->as_iter, sgl, sgl_pos(sgl), n->as_int);
  sgl_val_free(n, sgl);
  sgl_val_free(i, sgl);
  return ok;
}

static bool iter_map_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *fn = sgl_pop(sgl), *in = sgl_pop(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);
  bool ok = false;
  
  if (!i) {
    sgl_val_free(fn, sgl);
    goto exit;
  }
  
  sgl_push(sgl, sgl->MapIter)->as_iter = &sgl_map_iter_new(sgl, i, fn)->iter;
  ok = true;
 exit:
  sgl_val_free(in, sgl);
  return ok;
}

static bool iter_filter_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *fn = sgl_pop(sgl), *in = sgl_pop(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);
  bool ok = false;
  
  if (!i) {
    sgl_val_free(fn, sgl);
    goto exit;
  }

  sgl_push(sgl, sgl->Iter)->as_iter = &sgl_filter_iter_new(sgl, i, fn)->iter;
  ok = true;
 exit:
  sgl_val_free(in, sgl);
  return ok;
}

static bool buf_new_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  sgl_push(sgl, sgl->Buf)->as_buf = sgl_buf_new(sgl);
  return true;
}

static bool buf_pop_drop_imp(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_op **rpc) {
  struct sgl_val *bv = sgl_pop(sgl);
  struct sgl_buf *b = bv->as_buf;
  bool ok = false;
  
  if (!b->len) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to pop"));
    goto exit;
  }

  b->len--;
  ok = true;
 exit:
  sgl_val_free(bv, sgl);
  return ok;
}

static bool buf_len_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *bv = sgl_peek(sgl);
  struct sgl_buf *b = bv->as_buf;
  sgl_val_reuse(bv, sgl, sgl->Int, false)->as_int = b->len;
  sgl_buf_deref(b, sgl);
  return true;
}

static bool buf_clear_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *b = sgl_pop(sgl);
  b->as_buf->len = 0;
  sgl_val_free(b, sgl);
  return true;
}

static bool int_char_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_int_t iv = v->as_int;
  
  if (iv < 0 || iv > SGL_MAX_CHAR) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Invalid char: %" SGL_INT, iv));
    sgl_val_free(sgl_pop(sgl), sgl);
    return false;
  }

  sgl_val_reuse(v, sgl, sgl->Char, false)->as_char = iv;
  return true;
}

static bool char_int_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_int_t iv = v->as_char;
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = iv;
  return true;
}

static bool str_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  struct sgl_buf out;
  sgl_buf_init(&out);
  sgl_val_print(v, sgl, sgl_pos(sgl), &out);
  bool is_short = out.len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(&out) : NULL, out.len);

  if (!is_short) {
    char *d = (char *)out.data;
    d[out.len] = 0;
    s->as_long = d;
    out.data = NULL;
    out.cap = 0;
  }

  out.len = 0;
  sgl_val_reuse(v, sgl, sgl->Str, true)->as_str = s;
  sgl_buf_deinit(&out);
  return true;
}

static bool str_pop_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;
  if (!s->len) { sgl_error(sgl, sgl_pos(sgl), "Nothing to pop"); }
  s->len--;
  sgl_push(sgl, sgl->Char)->as_char = sgl_str_cs(s)[s->len];
  sgl_val_free(v, sgl);
  return true;
}

static bool str_pop_drop_imp(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;
  if (!s->len) { sgl_error(sgl, sgl_pos(sgl), "Nothing to pop"); }
  s->len--;
  sgl_val_free(v, sgl);
  return true;
}

static bool str_first_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;

  if (s->len) {
    sgl_push(sgl, sgl->Char)->as_char = *sgl_str_cs(s);
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(v, sgl);
  return true;
}

static bool str_last_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;

  if (s->len) {
    sgl_push(sgl, sgl->Char)->as_char = sgl_str_cs(s)[s->len-1];
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(v, sgl);
  return true;
}

static bool str_len_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str *s = v->as_str;
  sgl_push(sgl, sgl->Int)->as_int = s->len;
  sgl_val_free(v, sgl);
  return true;
}

static bool seq_join_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *sepv = sgl_pop(sgl), *in = sgl_pop(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);
  sgl_val_free(in, sgl);

  if (!i) {
    sgl_val_free(sepv, sgl);
    return false;
  }
  
  struct sgl_buf out;
  sgl_buf_init(&out);
  struct sgl_val *v = NULL, *sep = NULL;
  struct sgl_pos *pos = sgl_pos(sgl);
  
  while ((v = sgl_iter_next_val(i, sgl, pos, NULL))) {
    if (sep) { sgl_val_print(sep, sgl, pos, &out); }
    sgl_val_print(v, sgl, pos, &out);
    sgl_val_free(v, sgl);
    sep = sepv;
  }

  sgl_iter_deref(i, sgl);
  sgl_val_free(sep, sgl);
  bool is_short = out.len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(&out) : NULL, out.len);

  if (!is_short) {
    char *d = (char *)out.data;
    d[out.len] = 0;
    s->as_long = d;
    out.data = NULL;
    out.cap = 0;
  }
  
  out.len = 0;
  sgl_buf_deinit(&out);
  sgl_push(sgl, sgl->Str)->as_str = s;
  return true;
}

static bool list_join_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *sep = sgl_pop(sgl), *in = sgl_pop(sgl);
  struct sgl_list *l = in->as_list;

  struct sgl_buf out;
  sgl_buf_init(&out);
  struct sgl_pos *pos = sgl_pos(sgl);
  sgl_int_t len = 0, sep_len = (sep->type == sgl->Str) ? sep->as_str->len : 1;
  struct sgl_ls *fst = l->root.next;

  sgl_ls_do(&l->root, struct sgl_val, ls, v) {
    if (&v->ls != fst) { len += sep_len; }

    if (v->type == sgl->Str) {
      len += v->as_str->len;
    } else {
      len++;
    }
  }

  sgl_buf_grow(&out, len);
  
  sgl_ls_do(&l->root, struct sgl_val, ls, v) {
    if (&v->ls != fst) { sgl_val_print(sep, sgl, pos, &out); }
    sgl_val_print(v, sgl, pos, &out);
  }

  sgl_val_free(sep, sgl);
  sgl_val_free(in, sgl);
  
  bool is_short = out.len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(&out) : NULL, out.len);

  if (!is_short) {
    char *d = (char *)out.data;
    d[out.len] = 0;
    s->as_long = d;
    out.data = NULL;
    out.cap = 0;
  }

  out.len = 0;
  sgl_buf_deinit(&out);
  sgl_push(sgl, sgl->Str)->as_str = s;
  return true;
}

static bool str_int_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  struct sgl_str *s = v->as_str;
  char *cs = sgl_str_cs(s), *p = NULL;  
  sgl_int_t i = strtoimax(cs, &p, 10);

  if (p == cs) {
    v->type = sgl->Nil;
  } else {
    v->type = sgl->Int;
    v->as_int = i;
  }
  
  sgl_str_deref(s, sgl);
  return true;
}

static bool str_iter_int_imp(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_str_iter *i = sgl_baseof(struct sgl_str_iter, iter, v->as_iter);
  char *s = i->pos+1, *end = NULL;
  sgl_push(sgl, sgl->Int)->as_int = strtoimax(s, &end, 10);
  if (end > i->pos) { i->pos = end-1; }
  sgl_val_free(v, sgl);
  return true;
}

static bool pair_first_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_pair *p = &v->as_pair;
  sgl_val_dup(p->first, sgl, sgl_stack(sgl));
  sgl_val_free(v, sgl);
  return true;
}

static bool pair_last_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_pair *p = &v->as_pair;
  sgl_val_dup(p->last, sgl, sgl_stack(sgl));
  sgl_val_free(v, sgl);
  return true;
}

static bool pair_splat_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_pair *p = &v->as_pair;
  struct sgl_ls *s = sgl_stack(sgl);
  sgl_ls_push(s, &p->first->ls);
  sgl_ls_push(s, &p->last->ls);
  sgl_free(&sgl->val_pool, v);
  return true;
}

static bool list_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *in = sgl_peek(sgl);
  struct sgl_iter *i = sgl_val_iter(in, sgl, NULL);

  if (!i) {
    sgl_ls_delete(&in->ls);
    sgl_val_free(in, sgl);
    return false;
  }
  
  struct sgl_list *l = sgl_list_new(sgl);
  struct sgl_pos *pos = sgl_pos(sgl);
  while (sgl_iter_next_val(i, sgl, pos, &l->root));
  sgl_iter_deref(i, sgl);
  sgl_val_reuse(in, sgl, sgl->List, true)->as_list = l;
  return true;
}

static bool push_list(struct sgl_fimp *fimp,
                      struct sgl *sgl,
                      struct sgl_op **rpc,
                      bool drop) {
  struct sgl_val *v = sgl_pop(sgl), *l = drop ? sgl_pop(sgl) : sgl_peek(sgl);
  sgl_ls_push(&l->as_list->root, &v->ls);
  if (drop) { sgl_val_free(l, sgl); }
  return true;
}

static bool list_push_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  return push_list(fimp, sgl, rpc, false);
}

static bool list_push_drop_imp(struct sgl_fimp *fimp,
                               struct sgl *sgl,
                               struct sgl_op **rpc) {
  return push_list(fimp, sgl, rpc, true);
}

static bool list_peek_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *l = sgl_pop(sgl);
  struct sgl_ls *v = sgl_ls_peek(&l->as_list->root);

  if (v) {
    sgl_val_dup(sgl_baseof(struct sgl_val, ls, v), sgl, sgl_stack(sgl));
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(l, sgl);
  return true;
}

static bool list_pop_drop_imp(struct sgl_fimp *fimp,
                              struct sgl *sgl,
                              struct sgl_op **rpc) {
  struct sgl_val *l = sgl_pop(sgl);
  struct sgl_ls *v = sgl_ls_pop(&l->as_list->root);
  
  if (v) {
    sgl_val_free(sgl_baseof(struct sgl_val, ls, v), sgl);
    sgl_val_free(l, sgl);
    return true;
  }
  
  sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to pop"));
  sgl_val_free(l, sgl);
  return false;
}

static bool list_pop_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *l = sgl_pop(sgl);
  struct sgl_ls *v = sgl_ls_pop(&l->as_list->root);
  
  if (v) {
    sgl_ls_push(sgl_stack(sgl), v);
    sgl_val_free(l, sgl);
    return true;
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(l, sgl);
  return false;
}

static bool list_len_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  struct sgl_list *l = v->as_list;
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = sgl_list_len(l);
  sgl_list_deref(l, sgl);
  return true;
}

static bool list_delete_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *i = sgl_pop(sgl);
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i->as_iter);

  if (li->pos == li->root) {
    sgl_push(sgl, sgl->Nil);
  } else {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, li->pos);
    li->pos = li->pos->next;
    sgl_ls_delete(&v->ls);
    sgl_ls_push(sgl_stack(sgl), &v->ls);
  }

  sgl_val_free(i, sgl);
  return true;
}

static bool list_delete_drop_imp(struct sgl_fimp *fimp,
                                 struct sgl *sgl,
                                 struct sgl_op **rpc) {
  struct sgl_val *i = sgl_pop(sgl);
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i->as_iter);
  bool ok = false;
  
  if (li->pos == li->root) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Iter eof"));
    goto exit;
  }

  struct sgl_val *v = sgl_baseof(struct sgl_val, ls, li->pos);
  li->pos = li->pos->next;
  sgl_ls_delete(&v->ls);
  sgl_val_free(v, sgl);
  ok = true;
 exit:
  sgl_val_free(i, sgl);
  return ok;
}

static bool list_insert_imp(struct sgl_fimp *fimp,
                            struct sgl *sgl,
                            struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl), *i = sgl_pop(sgl);
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i->as_iter);
  sgl_ls_insert(li->pos, &v->ls);
  sgl_val_free(i, sgl);
  return true;
}

static bool list_set_new_imp(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_op **rpc) {
  struct sgl_val *k = sgl_pop(sgl);

  if (k->type == sgl->Nil) {
    sgl_val_free(k,sgl);
    k = NULL;
  }

  sgl_push(sgl, sgl->ListSet)->as_list = &sgl_list_set_new(sgl, k)->list;
  return true;
}

static bool list_set_try_add_imp(struct sgl_fimp *fimp,
                                 struct sgl *sgl,
                                 struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl), *sv = sgl_pop(sgl);
  struct sgl_list_set *s = sgl_baseof(struct sgl_list_set, list, sv->as_list);

  bool ok = false;
  struct sgl_val *k = s->key ? sgl_list_set_key(s, v) : v;
  struct sgl_ls *i = sgl_list_set_find(s, k, &ok);
  if (s->key) { sgl_val_free(k, sgl); }

  if (!i) {
    sgl_val_free(v, sgl);
    sgl_val_free(sv, sgl);
    return false;
  }
  
  if (ok) {
    sgl_val_free(v, sgl);
  } else {
    sgl_ls_insert(i, &v->ls);
  }
  
  sgl_val_free(sv, sgl);
  return true;
}

static bool list_set_remove_drop_imp(struct sgl_fimp *fimp,
                                     struct sgl *sgl,
                                     struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl), *sv = sgl_pop(sgl);
  struct sgl_list_set *s = sgl_baseof(struct sgl_list_set, list, sv->as_list);

  bool ok = false;
  struct sgl_ls *i = sgl_list_set_find(s, v, &ok); 

  if (!i) {
    sgl_val_free(v, sgl);
    sgl_val_free(sv, sgl);
    return false;
  }

  if (ok) {
    sgl_ls_delete(i);
    sgl_val_free(sgl_baseof(struct sgl_val, ls, i), sgl);
  } else {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Key not found"));
  }

  sgl_val_free(v, sgl);
  sgl_val_free(sv, sgl);
  return ok;
}

static bool list_set_find_imp(struct sgl_fimp *fimp,
                              struct sgl *sgl,
                              struct sgl_op **rpc) {
  struct sgl_val *k = sgl_pop(sgl), *sv = sgl_pop(sgl);
  struct sgl_list_set *s = sgl_baseof(struct sgl_list_set, list, sv->as_list);

  bool ok = false;
  struct sgl_ls *i = sgl_list_set_find(s, k, &ok); 

  if (!i) {
    sgl_val_free(k, sgl);
    sgl_val_free(sv, sgl);
    return false;
  }

  if (ok) {
    sgl_val_dup(sgl_baseof(struct sgl_val, ls, i), sgl, sgl_stack(sgl));
  } else {
    sgl_push(sgl, sgl->Nil);
  }
  
  sgl_val_free(k, sgl);
  sgl_val_free(sv, sgl);
  return true;
}

static bool list_set_find_iter_imp(struct sgl_fimp *fimp,
                                   struct sgl *sgl,
                                   struct sgl_op **rpc) {
  struct sgl_val *k = sgl_pop(sgl), *sv = sgl_pop(sgl);
  struct sgl_list_set *s = sgl_baseof(struct sgl_list_set, list, sv->as_list);

  bool ok = false;
  struct sgl_ls *ip = sgl_list_set_find(s, k, &ok);

  if (!ip) {
    sgl_val_free(k, sgl);
    sgl_val_free(sv, sgl);
    return false;
  }

  struct sgl_iter *i = &sgl_list_iter_new(sgl, sv, &s->list.root, ip)->iter;
  sgl_push(sgl, sgl->ListIter)->as_iter = i;
  sgl_push(sgl, sgl->Bool)->as_bool = ok;
  sgl_val_free(k, sgl);
  return true;
}

static bool put_field_sym(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc,
                          bool drop) {
  struct sgl_val
    *val = sgl_pop(sgl),
    *id = sgl_pop(sgl),
    *s = drop ? sgl_pop(sgl) : sgl_peek(sgl);
  
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, s->type);
  struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
  struct sgl_sym *fid = id->as_sym;
  struct sgl_field *f = sgl_struct_type_find(st, fid);
  bool ok = false;
  
  if (!f) {
    sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Unknown field: %s", fid->id));
    sgl_val_free(val, sgl);
    goto exit;
  }

  if (!sgl_derived(val->type, f->type)) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Expected %s, was: %s",
                          sgl_type_id(f->type)->id, sgl_type_id(val->type)->id));
    
    sgl_val_free(val, sgl);
    goto exit;
  }
  
  s->as_struct->fields[f->i] = val;
  ok = true;
 exit:
  sgl_val_free(id, sgl);
  if (drop) { sgl_val_free(s, sgl); }
  return ok;
}

static bool field_dup_put_sym_imp(struct sgl_fimp *fimp,
                                  struct sgl *sgl,
                                  struct sgl_op **rpc) {
  return put_field_sym(fimp, sgl, rpc, false);
}

static bool field_put_sym_imp(struct sgl_fimp *fimp,
                              struct sgl *sgl,
                              struct sgl_op **rpc) {
  return put_field_sym(fimp, sgl, rpc, true);
}

static bool now_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  sgl_push(sgl, sgl->Time)->as_time = sgl_now();
  return true;
}

static bool new_xy_imp(struct sgl_fimp *fimp,
                       struct sgl *sgl,
                       struct sgl_op **rpc) {
  struct sgl_val *y = sgl_pop(sgl), *x = sgl_peek(sgl);
  sgl_xy_init(&sgl_val_reuse(x, sgl, sgl->XY, false)->as_xy, x->as_int, y->as_int);
  return true;
}

static bool x_imp(struct sgl_fimp *fimp,
                  struct sgl *sgl,
                  struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = v->as_xy.x;
  return true;
}

static bool y_imp(struct sgl_fimp *fimp,
                  struct sgl *sgl,
                  struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = v->as_xy.y;
  return true;
}

static bool xy_add_imp(struct sgl_fimp *fimp,
                       struct sgl *sgl,
                       struct sgl_op **rpc) {
  struct sgl_val *rhs = sgl_pop(sgl), *lhs = sgl_peek(sgl);
  struct sgl_xy *rxy = &rhs->as_xy, *lxy = &lhs->as_xy;
  lxy->x += rxy->x;
  lxy->y += rxy->y;
  sgl_val_free(rhs, sgl);
  return true;
}

static bool xy_splat_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  struct sgl_xy xy = v->as_xy;
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = xy.x;
  sgl_push(sgl, sgl->Int)->as_int = xy.y;
  return true;
}

static bool new_rgb_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *b = sgl_pop(sgl), *g = sgl_pop(sgl), *r = sgl_peek(sgl);
  
  sgl_rgb_init(&sgl_val_reuse(r, sgl, sgl->RGB, false)->as_rgb,
               r->as_int, g->as_int, b->as_int);
  
  return true;
}

static bool rgb_splat_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  struct sgl_rgb rgb = v->as_rgb;
  sgl_val_reuse(v, sgl, sgl->Int, false)->as_int = rgb.r;
  sgl_push(sgl, sgl->Int)->as_int = rgb.g;
  sgl_push(sgl, sgl->Int)->as_int = rgb.b;
  return true;
}

static bool print_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_buf *buf = sgl_stdout(sgl);
  sgl_val_print(v, sgl, sgl_pos(sgl), buf);
  sgl_val_free(v, sgl);
  return true;
}

static bool say_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  struct sgl_buf *buf = sgl_stdout(sgl);
  sgl_val_print(v, sgl, sgl_pos(sgl), buf);
  sgl_buf_putc(buf, '\n');
  sgl_val_free(v, sgl);
  return true;
}

static bool ask_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *out = sgl_peek(sgl);
  struct sgl_buf *buf = sgl_stdout(sgl);
  sgl_val_print(out, sgl, sgl_pos(sgl), buf);

  if (!sgl_flush_stdout(sgl)) {
    sgl_val_free(out, sgl);
    return false;
  }
  
  int64_t len = 0;
  char *in = sgl_readln(stdin, &len);
  struct sgl_str *s = sgl_str_new(sgl, NULL, len);
  sgl_str_assign(s, in, len);
  sgl_val_reuse(out, sgl, sgl->Str, true)->as_str = s;  
  return true;
}

static bool get_stdout_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_buf *b = sgl_push(sgl, sgl->Buf)->as_buf = sgl_stdout(sgl);
  b->nrefs++;
  return true;
}

static bool stdout_str_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_buf *b = sgl_stdout(sgl);
  bool is_short = b->len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(b) : NULL, b->len);

  if (!is_short) {
    char *d = (char *)b->data;
    d[b->len] = 0;
    s->as_long = d;
    b->data = NULL;
    b->cap = 0;
  }
  
  b->len = 0;
  sgl_push(sgl, sgl->Str)->as_str = s;
  return true;
}

static bool flush_stdout_imp(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_op **rpc) {
  return sgl_flush_stdout(sgl);
}

static char *fopen_io_run_imp(struct sgl *sgl,
                              const char *p,
                              struct sgl_sym *m,
                              struct sgl_file *out) {
  FILE *f = out->imp = fopen(p, m->id);
  return f ? NULL : sgl_sprintf("Failed opening file '%s': %" SGL_INT, p, errno);
}

#ifdef SGL_USE_ASYNC

static char *fopen_io_run(struct sgl_io *io,
                          struct sgl *sgl,
                          struct sgl_io_thread *thread) {
  struct sgl_val *p = io->args[0];
  return fopen_io_run_imp(sgl, sgl_str_cs(p->as_str), io->args[1], io->args[2]);
}

static void fopen_io_free(struct sgl_io *io, struct sgl *sgl) {
  struct sgl_val *p = io->args[0];
  struct sgl_file *f = io->args[2];
  
  if (io->error) {
    sgl_file_deref(f, sgl);    
    sgl_error(sgl, &io->pos, io->error);
  } else {
    sgl_push(sgl, sgl->File)->as_file = f;
  }
  
  sgl_val_free(p, sgl);
  sgl_io_free(io, sgl);
}

#endif

static bool fopen_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *m = sgl_pop(sgl), *p = sgl_pop(sgl);
  struct sgl_sym *ms = m->as_enum.id;
  sgl_val_free(m, sgl);
  char *ps = sgl_str_cs(p->as_str);
  struct sgl_file *file = sgl_file_new(sgl, NULL);
  
#ifdef SGL_USE_ASYNC
  if (sgl_is_async(sgl)) {
    struct sgl_io *io = sgl_io_new(sgl, -1, p, ms, file);
    io->run = fopen_io_run;
    io->free = fopen_io_free;
    *rpc = sgl_io_yield(io, sgl, sgl_io_thread(sgl))->next;
    return sgl->errors.next == &sgl->errors;
  }
#endif
  
  char *error = fopen_io_run_imp(sgl, ps, ms, file);
    
  if (error) {
    sgl_file_free(file, sgl);
    sgl_error(sgl, sgl_pos(sgl), error);
  } else {
    sgl_push(sgl, sgl->File)->as_file = file;
  }
  
  sgl_val_free(p, sgl);
  return !error;
}

static bool file_lines_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  struct sgl_val *f = sgl_peek(sgl);
  struct sgl_iter *i = &sgl_file_line_iter_new(sgl, f->as_file)->iter;
  sgl_val_reuse(f, sgl, sgl->Iter, true)->as_iter = i;
  return true;
}

static bool starch_new_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  sgl_push(sgl, sgl->Starch)->as_buf = &sgl_starch_new(sgl)->buf;
  return true;
}

static bool load_val(struct sgl_fimp *fimp,
                     struct sgl *sgl,
                     struct sgl_op **rpc,
                     bool drop) {
  struct sgl_val *in = drop ? sgl_pop(sgl) : sgl_peek(sgl);
  struct sgl_starch *s = sgl_baseof(struct sgl_starch, buf, in->as_buf);
  struct sgl_val *v = sgl_starch_load(s, sgl, sgl_stack(sgl));
  if (drop) { sgl_val_free(in, sgl); }

  if (!v) {
    sgl_push(sgl, sgl->Nil);
    return false;
  }

  return true;
}

static bool load_imp(struct sgl_fimp *fimp,
                     struct sgl *sgl,
                     struct sgl_op **rpc) {
  return load_val(fimp, sgl, rpc, false);
}

static bool load_drop_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  return load_val(fimp, sgl, rpc, true);
}

static bool store_val(struct sgl_fimp *fimp,
                      struct sgl *sgl,
                      struct sgl_op **rpc,
                      bool drop) {
  struct sgl_val *v = sgl_pop(sgl), *out = drop ? sgl_pop(sgl) : sgl_peek(sgl);
  struct sgl_starch *s = sgl_baseof(struct sgl_starch, buf, out->as_buf);
  bool ok = sgl_starch_store(s, sgl, v);
  if (drop) { sgl_val_free(out, sgl); }
  return ok;
}

static bool store_imp(struct sgl_fimp *fimp,
                      struct sgl *sgl,
                      struct sgl_op **rpc) {
  return store_val(fimp, sgl, rpc, false);
}

static bool store_drop_imp(struct sgl_fimp *fimp,
                           struct sgl *sgl,
                           struct sgl_op **rpc) {
  return store_val(fimp, sgl, rpc, true);
}

static bool fail_imp(struct sgl_fimp *fimp,
                     struct sgl *sgl,
                     struct sgl_op **rpc) {
  sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
  return false;
}

static bool test_imp(struct sgl_fimp *fimp,
                     struct sgl *sgl,
                     struct sgl_op **rpc) {
  struct sgl_val *v = sgl_pop(sgl);
  bool ok = sgl_val_bool(v, sgl);
  sgl_val_free(v, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static bool test_is_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_pop(sgl);

  bool ok = sgl_val_is(lhs, rhs);
  sgl_val_free(lhs, sgl);
  sgl_val_free(rhs, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static bool test_nis_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_pop(sgl);

  bool ok = !sgl_val_is(lhs, rhs);
  sgl_val_free(lhs, sgl);
  sgl_val_free(rhs, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static bool test_eq_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_pop(sgl);

  bool ok = sgl_val_eq(lhs, sgl, rhs);
  sgl_val_free(lhs, sgl);
  sgl_val_free(rhs, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static bool test_neq_imp(struct sgl_fimp *fimp,
                         struct sgl *sgl,
                         struct sgl_op **rpc) {
  struct sgl_val
    *rhs = sgl_pop(sgl),
    *lhs = sgl_pop(sgl);

  bool ok = !sgl_val_eq(lhs, sgl, rhs);
  sgl_val_free(lhs, sgl);
  sgl_val_free(rhs, sgl);

  if (!ok) {
    sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Test failed"));
    return false;
  }
  
  fputc('.', stdout);
  return true;
}

static struct sgl_val *load_var(struct sgl_starch *in,
                                struct sgl *sgl,
                                enum sgl_store_id id,
                                struct sgl_ls *root) {
  struct sgl_buf *ib = &in->buf;
  sgl_int_t i = 0;

  if (id == SGL_STORE_DUP_SHORT || id == SGL_STORE_CLONE_SHORT) {
    if (!sgl_load_short(ib, (uint8_t *)&i)) { return NULL; }
  } else {
    enum sgl_store_id i_id = sgl_load_id(ib);
    if (i_id == SGL_STORE_INVALID) { return NULL; }
    if (!sgl_load_int(i_id, ib, sgl, &i)) { return NULL; }
  }

  assert(i < in->load_vals.len);
  struct sgl_val *v = *(struct sgl_val **)sgl_vec_get(&in->load_vals, i);

  return (id == SGL_STORE_CLONE || id == SGL_STORE_CLONE_SHORT)
    ? sgl_val_clone(v, sgl, sgl_pos(sgl), root)
    : sgl_val_dup(v, sgl, root);
}

static void init_types(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  sgl->A = sgl_type_new(sgl, pos, l, sgl_sym(sgl, "A"),
                        sgl_types(NULL));

  sgl->Nil = sgl_nil_type_new(sgl, pos, l, sgl_sym(sgl, "Nil"),
                              sgl_types(sgl->A));

  sgl->T = sgl_type_new(sgl, pos, l, sgl_sym(sgl, "T"),
                        sgl_types(sgl->A));

  sgl->Meta = sgl_meta_type_new(sgl, pos, l, sgl_sym(sgl, "Meta"),
                                sgl_types(sgl->T));

  sgl->Num = sgl_trait_new(sgl, pos, l, sgl_sym(sgl, "Num"),
                           sgl_types(sgl->T));

  sgl->Seq = sgl_trait_new(sgl, pos, l, sgl_sym(sgl, "Seq"),
                           sgl_types(sgl->T));

  sgl->Iter = sgl_iter_type_new(sgl, pos, l, sgl_sym(sgl, "Iter"),
                                sgl_types(sgl->T, sgl->Seq));

  sgl->Bool = sgl_bool_type_new(sgl, pos, l, sgl_sym(sgl, "Bool"),
                                sgl_types(sgl->T));

  sgl->Buf = sgl_buf_type_new(sgl, pos, l, sgl_sym(sgl, "Buf"),
                              sgl_types(sgl->T));

  sgl->Char = sgl_char_type_new(sgl, pos, l, sgl_sym(sgl, "Char"),
                                sgl_types(sgl->T));

  sgl->Curry = sgl_curry_type_new(sgl, pos, l, sgl_sym(sgl, "Curry"),
                                  sgl_types(sgl->T));

  sgl->Enum = sgl_trait_new(sgl, pos, l, sgl_sym(sgl, "Enum"),
                            sgl_types(sgl->T));

  sgl->EnumIter = sgl_ref_type_new(sgl, pos, l,
                                   sgl_sym(sgl, "EnumIter"),
                                   sgl_types(sgl->Iter),
                                   sizeof(struct sgl_enum_iter),
                                   offsetof(struct sgl_enum_iter, iter) +
                                   offsetof(struct sgl_iter, ls));

  sgl_derive(sgl_type_meta(sgl->Enum, sgl), sgl->Seq);
    
  sgl->Error = sgl_error_type_new(sgl, pos, l, sgl_sym(sgl, "Error"),
                                  sgl_types(sgl->T));

  sgl->File = sgl_file_type_new(sgl, pos, l, sgl_sym(sgl, "File"),
                                sgl_types(sgl->T));

  sgl->FileLineIter = sgl_ref_type_new(sgl, pos, l,
                                       sgl_sym(sgl, "FileLineIter"),
                                       sgl_types(sgl->Iter),
                                       sizeof(struct sgl_file_line_iter),
                                       offsetof(struct sgl_file_line_iter, iter) +
                                       offsetof(struct sgl_iter, ls));

  sgl->FileMode = sgl_enum_type_new(sgl, pos, l, sgl_sym(sgl, "FileMode"),
                                    sgl_types(NULL),
                                    sgl_sym(sgl, "r"),
                                    sgl_sym(sgl, "w"),
                                    sgl_sym(sgl, "r+"),
                                    sgl_sym(sgl, "w+"));

  sgl->FilterIter = sgl_ref_type_new(sgl, pos, l,
                                     sgl_sym(sgl, "FilterIter"),
                                     sgl_types(sgl->Iter),
                                     sizeof(struct sgl_filter_iter),
                                     offsetof(struct sgl_filter_iter, iter) +
                                     offsetof(struct sgl_iter, ls));

  sgl->Fix = sgl_fix_type_new(sgl, pos, l, sgl_sym(sgl, "Fix"),
                              sgl_types(sgl->T, sgl->Num));

  sgl->Func = sgl_func_type_new(sgl, pos, l, sgl_sym(sgl, "Func"),
                                sgl_types(sgl->T));

  sgl->Form = sgl_form_type_new(sgl, pos, l, sgl_sym(sgl, "Form"),
                                sgl_types(sgl->T));

  sgl->Int = sgl_int_type_new(sgl, pos, l, sgl_sym(sgl, "Int"),
                              sgl_types(sgl->T, sgl->Num, sgl->Seq));

  sgl->IntIter = sgl_ref_type_new(sgl, pos, l,
                                  sgl_sym(sgl, "IntIter"),
                                  sgl_types(sgl->Iter),
                                  sizeof(struct sgl_int_iter),
                                  offsetof(struct sgl_int_iter, iter) +
                                  offsetof(struct sgl_iter, ls));
                                  
  sgl->Lambda = sgl_lambda_type_new(sgl, pos, l, sgl_sym(sgl, "Lambda"),
                                    sgl_types(sgl->T));

  sgl->List = sgl_list_type_new(sgl, pos, l, sgl_sym(sgl, "List"),
                                sgl_types(sgl->T, sgl->Seq));

  sgl->ListIter = sgl_ref_type_new(sgl, pos, l,
                                   sgl_sym(sgl, "ListIter"),
                                   sgl_types(sgl->Iter),
                                   sizeof(struct sgl_list_iter),
                                   offsetof(struct sgl_list_iter, iter) +
                                   offsetof(struct sgl_iter, ls));

  sgl->ListSet = sgl_list_set_type_new(sgl, pos, l, sgl_sym(sgl, "ListSet"),
                                       sgl_types(sgl->List));

  sgl->Macro = sgl_lambda_type_new(sgl, pos, l, sgl_sym(sgl, "Macro"),
                                   sgl_types(sgl->T));

  sgl->MapIter = sgl_ref_type_new(sgl, pos, l,
                                  sgl_sym(sgl, "MapIter"),
                                  sgl_types(sgl->Iter),
                                  sizeof(struct sgl_map_iter),
                                  offsetof(struct sgl_map_iter, iter) +
                                  offsetof(struct sgl_iter, ls));

  sgl->RGB = sgl_rgb_type_new(sgl, pos, l, sgl_sym(sgl, "RGB"),
                              sgl_types(sgl->T));

  sgl->Ref = sgl_vref_type_new(sgl, pos, l, sgl_sym(sgl, "Ref"),
                               sgl_types(sgl->T));
  
  sgl->Pair = sgl_pair_type_new(sgl, pos, l, sgl_sym(sgl, "Pair"),
                                sgl_types(sgl->T));

  sgl->Starch = sgl_starch_type_new(sgl, pos, l, sgl_sym(sgl, "Starch"),
                                    sgl_types(sgl->Buf, sgl->Seq));

  sgl->StarchIter = sgl_ref_type_new(sgl, pos, l,
                                     sgl_sym(sgl, "StarchIter"),
                                     sgl_types(sgl->Iter),
                                     sizeof(struct sgl_starch_iter),
                                     offsetof(struct sgl_starch_iter, iter) +
                                     offsetof(struct sgl_iter, ls));

  sgl->Str = sgl_str_type_new(sgl, pos, l, sgl_sym(sgl, "Str"),
                              sgl_types(sgl->T, sgl->Seq));

  sgl->StrIter = sgl_ref_type_new(sgl, pos, l,
                                  sgl_sym(sgl, "StrIter"),
                                  sgl_types(sgl->Iter),
                                  sizeof(struct sgl_str_iter),
                                  offsetof(struct sgl_str_iter, iter) +
                                  offsetof(struct sgl_iter, ls));

  sgl->Struct = sgl_trait_new(sgl, pos, l, sgl_sym(sgl, "Struct"),
                              sgl_types(sgl->T));

  sgl->StructMeta = sgl_type_meta(sgl->Struct, sgl);
  
  sgl->Sym = sgl_sym_type_new(sgl, pos, l, sgl_sym(sgl, "Sym"),
                              sgl_types(sgl->T));

  sgl->Time = sgl_time_type_new(sgl, pos, l, sgl_sym(sgl, "Time"),
                                sgl_types(sgl->T));
    
  sgl->Undef = sgl_undef_type_new(sgl, pos, l, sgl_sym(sgl, "Undef"),
                                  sgl_types(NULL));

  sgl->XY = sgl_xy_type_new(sgl, pos, l, sgl_sym(sgl, "XY"),
                            sgl_types(sgl->T));

  sgl->loaders[SGL_STORE_DUP] = sgl->loaders[SGL_STORE_DUP_SHORT] =
    sgl->loaders[SGL_STORE_CLONE] = sgl->loaders[SGL_STORE_CLONE_SHORT] = load_var;

  sgl->loaders[SGL_STORE_T] = sgl->loaders[SGL_STORE_F] = sgl->Bool->load_val;

  sgl->loaders[SGL_STORE_INT] =
    sgl->loaders[SGL_STORE_INT_ZERO] =
    sgl->loaders[SGL_STORE_INT_ONE] =
    sgl->loaders[SGL_STORE_INT_SHORT] = sgl->Int->load_val;

  sgl->loaders[SGL_STORE_LIST] =
    sgl->loaders[SGL_STORE_LIST_EMPTY] =
    sgl->loaders[SGL_STORE_LIST_SINGLE] = sgl->List->load_val;

  sgl->loaders[SGL_STORE_STR] =
    sgl->loaders[SGL_STORE_STR_EMPTY] = 
    sgl->loaders[SGL_STORE_STR_SHORT] = sgl->Str->load_val;
}

static void init_vals(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  sgl_val_init(&sgl->argv, sgl, sgl->List, NULL)->as_list = sgl_list_new(sgl);
  sgl_val_init(&sgl->empty_str, sgl, sgl->Str, NULL)->as_str =
    sgl_str_new(sgl, "", 0);
  sgl_val_init(&sgl->nil, sgl, sgl->Nil, NULL);
  sgl_val_init(&sgl->t, sgl, sgl->Bool, NULL)->as_bool = true;
  sgl_val_init(&sgl->f, sgl, sgl->Bool, NULL)->as_bool = false;
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  init_types(l, sgl, pos);
  init_vals(l, sgl, pos);

  sgl_lib_add_pmacro(l, sgl, pos, sgl_sym(sgl, "_"), nop_imp);
  sgl_lib_add_pmacro(l, sgl, pos, sgl_sym(sgl, "source:"), source_imp);
  sgl_lib_add_pmacro(l, sgl, pos, sgl_sym(sgl, "_source:"), nop_source_imp);
  
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "_:"), 1, nop_expr_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "::"), 0, pair_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "and:"), 1, and_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "else:"), 1, else_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "bench:"), 1, bench_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "curry:"), 1, curry_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "debug"), 0, debug_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "defer:"), 1, defer_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "doc:"), 1, doc_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "drop"), 0, drop_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "drop2"), 0, drop2_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "dup"), 0, dup_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "dupl"), 0, dupl_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "enum:"), 3, enum_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "for:"), 1, for_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "func:"), 4, func_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "get-set:"), 1, get_set_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "idle:"), 1, idle_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "if:"), 1, if_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "if-else:"), 2, if_else_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "include:"), 1, include_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "let:"), 1, let_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "lib:"), 1, lib_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "mem:"), 1, mem_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "new"), 0, new_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "or:"), 1, or_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "pre:"), 1, pre_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "dup-put:"), 1, dup_put_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "put:"), 1, put_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "recall"), 0, recall_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "recall:"), 1, recall_arg_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "ref"), 0, ref_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "rotl"), 0, rotl_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "rotr"), 0, rotr_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "stdout:"), 1, stdout_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "struct:"), 3, struct_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "swap"), 0, swap_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "switch:"), 3, switch_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "task:"), 1, task_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "throw"), 0, throw_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "times:"), 1, times_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "try:"), 1, try_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "trait:"), 2, trait_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "type"), 0, type_get_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "type:"), 3, type_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "use:"), 2, use_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "while:"), 1, while_imp);
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "yield"), 0, yield_imp);

#ifdef SGL_USE_ASYNC
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "sync:"), 1, sync_imp);
#endif

#ifdef SGL_USE_DL
  sgl_lib_add_macro(l, sgl, pos, sgl_sym(sgl, "link:"), 1, link_imp);
#endif

  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "nil"), &sgl->nil);
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "t"), &sgl->t);
  sgl_lib_add_const(l, sgl, pos, sgl_sym(sgl, "f"), &sgl->f);

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "argv"),
                    argv_imp,
                    sgl_rets(sgl->List)); 

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "dump"),
                    dump_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "docs"),
                    docs_imp,
                    sgl_rets(sgl_type_union(sgl, pos, sgl->Str, sgl->Nil)),
                    sgl_arg(sgl->Sym)); 

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "eval"),
                    eval_imp,
                    NULL,
                    sgl_arg(sgl->Str)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "type-sub?"),
                    type_sub_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A), sgl_arg(sgl->Meta)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "sub?"),
                    sub_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->Meta), sgl_arg(sgl->Meta)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "dup-clone"),
                    dup_clone_imp,
                    sgl_rets(sgl->T, sgl->T),
                    sgl_arg(sgl->T));

  sgl->clone_fimp = sgl_lib_add_cfunc(l, sgl, pos, 
                                      sgl_sym(sgl, "clone"),
                                      clone_imp,
                                      sgl_rets(sgl->T),
                                      sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "get"),
                    ref_get_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->Ref)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "set"),
                    ref_set_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Ref), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "clear"),
                    ref_clear_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Ref)); 

  sgl->call_fimp = sgl_lib_add_cfunc(l, sgl, pos, 
                                     sgl_sym(sgl, "call"),
                                     call_imp,
                                     NULL,
                                     sgl_arg(sgl->A));

  sgl->dup_call_fimp = sgl_lib_add_cfunc(l, sgl, pos, 
                                         sgl_sym(sgl, "dup-call"),
                                         dup_call_imp,
                                         NULL,
                                         sgl_arg(sgl->A));

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "catch"),
                    catch_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->Error)); 

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "bool"),
                    bool_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "not"),
                    not_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "=="),
                    is_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "!=="),
                    nis_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl->eq_fimp = sgl_lib_add_cfunc(l, sgl, pos, 
                                   sgl_sym(sgl, "="),
                                   eq_imp,
                                   sgl_rets(sgl->Bool),
                                   sgl_arg(sgl->A), sgl_arg(sgl->A));

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "!="),
                    ne_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "<"),
                    lt_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "<="),
                    lte_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, ">"),
                    gt_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, ">="),
                    gte_imp,
                    sgl_rets(sgl->Bool),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "++"),
                    int_inc_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "--"),
                    int_dec_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "+"),
                    int_add_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "-"),
                    int_sub_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "*"),
                    int_mul_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "div"),
                    int_div_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "mod"),
                    int_mod_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "fix"),
                    int_fix_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "+"),
                    fix_add_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "-"),
                    fix_sub_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "*"),
                    fix_mul_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "*"),
                    fix_int_mul_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "/"),
                    fix_div_imp,
                    sgl_rets(sgl->Fix),
                    sgl_arg(sgl->Fix), sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "int"),
                    fix_int_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Fix)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "min"),
                    min_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "max"),
                    max_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->T), sgl_arg(sgl->T)); 

  sgl->iter_fimp = sgl_lib_add_cfunc(l, sgl, pos,
                                     sgl_sym(sgl, "iter"),
                                     seq_iter_imp,
                                     sgl_rets(sgl->Iter),
                                     sgl_arg(sgl->Seq));
  
  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, ".."),
                    seq_splat_imp,
                    NULL,
                    sgl_arg(sgl->Seq));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "next"),
                    iter_next_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->Iter));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "_next"),
                    iter_next_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Iter));
  
  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "skip"),
                    iter_skip_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Iter), sgl_arg(sgl->Int));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "map"),
                    iter_map_imp,
                    sgl_rets(sgl->MapIter),
                    sgl_arg(sgl->Seq), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "filter"),
                    iter_filter_imp,
                    sgl_rets(sgl->FilterIter),
                    sgl_arg(sgl->Seq), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "new-buf"),
                    buf_new_imp,
                    sgl_rets(sgl->Buf));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop-drop"),
                    buf_pop_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Buf));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "len"),
                    buf_len_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Buf));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "clear"),
                    buf_clear_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Buf));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "char"),
                    int_char_imp,
                    sgl_rets(sgl->Char),
                    sgl_arg(sgl->Int));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "int"),
                    char_int_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Char));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "str"),
                    str_imp,
                    sgl_rets(sgl->Str),
                    sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop"),
                    str_pop_imp,
                    sgl_rets(sgl_type_union(sgl, pos, sgl->Char, sgl->Nil)),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop-drop"),
                    str_pop_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "first"),
                    str_first_imp,
                    sgl_rets(sgl_type_union(sgl, pos, sgl->Char, sgl->Nil)),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "last"),
                    str_last_imp,
                    sgl_rets(sgl_type_union(sgl, pos, sgl->Char, sgl->Nil)),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "len"),
                    str_len_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "join"),
                    seq_join_imp,
                    sgl_rets(sgl->Str),
                    sgl_arg(sgl->Seq), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "join"),
                    list_join_imp,
                    sgl_rets(sgl->Str),
                    sgl_arg(sgl->List), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "int"),
                    str_int_imp,
                    sgl_rets(sgl_type_opt(sgl->Int, sgl)),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "int"),
                    str_iter_int_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->StrIter));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "first"),
                    pair_first_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->Pair));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "last"),
                    pair_last_imp,
                    sgl_rets(sgl->T),
                    sgl_arg(sgl->Pair));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, ".."),
                    pair_splat_imp,
                    sgl_rets(sgl->T, sgl->T),
                    sgl_arg(sgl->Pair));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "list"),
                    list_imp,
                    sgl_rets(sgl->List),
                    sgl_arg(sgl->Seq));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "push"),
                    list_push_imp,
                    sgl_rets(sgl->List),
                    sgl_arg(sgl->List), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "_push"),
                    list_push_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->List), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "peek"),
                    list_peek_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->List));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop-drop"),
                    list_pop_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->List));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop"),
                    list_pop_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->List));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "len"),
                    list_len_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->List));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "delete"),
                    list_delete_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->ListIter));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "_delete"),
                    list_delete_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->ListIter));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "insert"),
                    list_insert_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->ListIter), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "new-list-set"),
                    list_set_new_imp,
                    sgl_rets(sgl->ListSet),
                    sgl_arg(sgl->A));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "_add"),
                    list_set_try_add_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->ListSet), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "_remove"),
                    list_set_remove_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->ListSet), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "find"),
                    list_set_find_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->ListSet), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "find-iter"),
                    list_set_find_iter_imp,
                    sgl_rets(sgl->ListIter, sgl->Bool),
                    sgl_arg(sgl->ListSet), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "put"),
                    field_put_sym_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Struct), sgl_arg(sgl->Sym), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "dup-put"),
                    field_dup_put_sym_imp,
                    sgl_rets(sgl->Struct),
                    sgl_arg(sgl->Struct), sgl_arg(sgl->Sym), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "new-xy"),
                    new_xy_imp,
                    sgl_rets(sgl->XY),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "x"),
                    x_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->XY)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "y"),
                    y_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->XY)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "+"),
                    xy_add_imp,
                    sgl_rets(sgl->XY),
                    sgl_arg(sgl->XY), sgl_arg(sgl->XY)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, ".."),
                    xy_splat_imp,
                    sgl_rets(sgl->Int, sgl->Int),
                    sgl_arg(sgl->XY)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "new-rgb"),
                    new_rgb_imp,
                    sgl_rets(sgl->RGB),
                    sgl_arg(sgl->Int), sgl_arg(sgl->Int), sgl_arg(sgl->Int)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, ".."),
                    rgb_splat_imp,
                    sgl_rets(sgl->Int, sgl->Int, sgl->Int),
                    sgl_arg(sgl->RGB)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "now"),
                    now_imp,
                    sgl_rets(sgl->Time)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "print"),
                    print_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A));

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "say"),
                    say_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "ask"),
                    ask_imp,
                    sgl_rets(sgl->Str),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "stdout"),
                    get_stdout_imp,
                    sgl_rets(sgl->Buf)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "stdout-str"),
                    stdout_str_imp,
                    sgl_rets(sgl->Str)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "flush-stdout"),
                    flush_stdout_imp,
                    sgl_rets(NULL)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "fopen"),
                    fopen_imp,
                    sgl_rets(sgl->File),
                    sgl_arg(sgl->Str), sgl_arg(sgl->FileMode)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "lines"),
                    file_lines_imp,
                    sgl_rets(sgl->FileLineIter),
                    sgl_arg(sgl->File)); 

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "new-starch"),
                    starch_new_imp,
                    sgl_rets(sgl->Starch));

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "load"),
                    load_imp,
                    sgl_rets(sgl->Starch, sgl->A),
                    sgl_arg(sgl->Starch)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "_load"),
                    load_drop_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->Starch)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "store"),
                    store_imp,
                    sgl_rets(sgl->Starch),
                    sgl_arg(sgl->Starch), sgl_arg(sgl->T)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "_store"),
                    store_drop_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->Starch), sgl_arg(sgl->T));
  
  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "fail"),
                    fail_imp,
                    sgl_rets(NULL)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test"),
                    test_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test=="),
                    test_is_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test!=="),
                    test_nis_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test="),
                    test_eq_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "test!="),
                    test_neq_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->A), sgl_arg(sgl->A)); 

  return sgl->errors.next == &sgl->errors;
}

struct sgl_lib *sgl_abc_lib_new(struct sgl *sgl, struct sgl_pos *pos) {
  struct sgl_lib *l = sgl_lib_new(sgl, pos, sgl_sym(sgl, "abc"));
  l->init = lib_init;
  return l;
}

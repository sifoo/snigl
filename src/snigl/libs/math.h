#ifndef SNIGL_LIBS_MATH_H
#define SNIGL_LIBS_MATH_H

#include "snigl/lib.h"
#include "snigl/prng.h"

struct sgl_math_lib {
  struct sgl_lib lib;
  struct sgl_prng prng;
  struct sgl_type *RandIter;
};

struct sgl_lib *sgl_math_lib_init(struct sgl_math_lib *l, struct sgl *sgl);

#endif

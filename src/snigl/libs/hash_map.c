#include <assert.h>

#include "snigl/hash_map.h"
#include "snigl/lib.h"
#include "snigl/libs/hash_map.h"
#include "snigl/ref_type.h"
#include "snigl/types/hash_map.h"
#include "snigl/sgl.h"

static bool at_imp(struct sgl_fimp *fimp,
                   struct sgl *sgl,
                   struct sgl_op **rpc) {
  struct sgl_val *key = sgl_pop(sgl), *hm = sgl_pop(sgl);
  struct sgl_val *v = sgl_hash_map_at(hm->as_hash_map, sgl, key);

  if (v) {
    sgl_val_dup(v, sgl, sgl_stack(sgl));
  } else {
    sgl_push(sgl, sgl->Nil);
  }

  sgl_val_free(key, sgl);
  sgl_val_free(hm, sgl);
  return true;
}

static bool new_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  sgl_push(sgl, sgl->HashMap)->as_hash_map = sgl_hash_map_new(sgl);
  return true;
}

static bool len_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  sgl_int_t len = v->as_hash_map->hash.len;
  sgl_val_reuse(v, sgl, sgl->Int, true)->as_int = len;
  return true;
}

static bool put_hash(struct sgl_fimp *fimp,
                     struct sgl *sgl,
                     struct sgl_op **rpc,
                     bool drop) {
  struct sgl_val
    *val = sgl_pop(sgl),
    *key = sgl_pop(sgl),
    *mv = drop ? sgl_pop(sgl) : sgl_peek(sgl);
  
  struct sgl_hash_map *m = mv->as_hash_map;
  sgl_hash_map_put(m, sgl, key, val);
  if (drop) { sgl_val_free(mv, sgl); }
  return true;
}

static bool dup_put_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  return put_hash(fimp, sgl, rpc, false);
}

static bool put_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  return put_hash(fimp, sgl, rpc, true);  
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  if (!sgl_use(sgl, pos, sgl->abc_lib,
               sgl_sym(sgl, "A"),
               sgl_sym(sgl, "Int"),
               sgl_sym(sgl, "T"))) { return false; }

  sgl->HashMap = sgl_hash_map_type_new(sgl, pos, l, sgl_sym(sgl, "HashMap"),
                                       sgl_types(sgl->Seq));

  sgl->HashMapIter = sgl_ref_type_new(sgl, pos, l,
                                      sgl_sym(sgl, "HashMapIter"),
                                      sgl_types(sgl->Iter),
                                      sizeof(struct sgl_hash_map_iter),
                                      offsetof(struct sgl_hash_map_iter, iter) +
                                      offsetof(struct sgl_iter, ls));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "at"),
                    at_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->HashMap), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "new-hash-map"),
                    new_imp,
                    sgl_rets(sgl->HashMap));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "len"),
                    len_imp,
                    sgl_rets(sgl->Int),
                    sgl_arg(sgl->HashMap));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "dup-put"),
                    dup_put_imp,
                    sgl_rets(sgl->HashMap),
                    sgl_arg(sgl->HashMap), sgl_arg(sgl->T), sgl_arg(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "put"),
                    put_imp,
                    sgl_rets(NULL),
                    sgl_arg(sgl->HashMap), sgl_arg(sgl->T), sgl_arg(sgl->T));
  
  return sgl->errors.next == &sgl->errors;
}

struct sgl_lib *sgl_hash_map_lib_new(struct sgl *sgl, struct sgl_pos *pos) {
  struct sgl_lib *l = sgl_lib_new(sgl, pos, sgl_sym(sgl, "hash-map"));
  l->init = lib_init;
  return l;
}

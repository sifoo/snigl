#include <assert.h>

#include "snigl/list.h"
#include "snigl/lib.h"
#include "snigl/libs/list.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"

static bool pop_front_imp(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_op **rpc) {
  struct sgl_val *l = sgl_pop(sgl);
  struct sgl_ls *v = sgl_ls_pop(&l->as_list->root);
  
  if (v) {
    sgl_val_dup(sgl_baseof(struct sgl_val, ls, v), sgl, sgl_stack(sgl));
  } else {
    sgl_push(sgl, sgl->Nil);
  }
  
  sgl_val_free(l, sgl);
  return true;
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  if (!sgl_use(sgl, pos, sgl->abc_lib,
               sgl_sym(sgl, "A"),
               sgl_sym(sgl, "List"))) { return false; }


  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "pop-first"),
                    pop_front_imp,
                    sgl_rets(sgl->A),
                    sgl_arg(sgl->List));

  return sgl->errors.next == &sgl->errors;
}

struct sgl_lib *sgl_list_lib_new(struct sgl *sgl, struct sgl_pos *pos) {
  struct sgl_lib *l = sgl_lib_new(sgl, pos, sgl_sym(sgl, "list"));
  l->init = lib_init;
  return l;
}

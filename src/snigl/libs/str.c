#include <assert.h>
#include <ctype.h>

#include "snigl/iters/phrase.h"
#include "snigl/iters/word.h"
#include "snigl/lib.h"
#include "snigl/libs/str.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/str.h"

static bool lower_imp(struct sgl_fimp *fimp,
                      struct sgl *sgl,
                      struct sgl_op **rpc) {
  struct sgl_str *s = sgl_peek(sgl)->as_str;
  for (char *cs = sgl_str_cs(s), *c = cs; c < cs + s->len; c++) { *c = tolower(*c); }
  return true;
}

static bool phrases_imp(struct sgl_fimp *fimp,
                        struct sgl *sgl,
                        struct sgl_op **rpc) {
  struct sgl_val *in = sgl_peek(sgl);
  struct sgl_phrase_iter *i = sgl_phrase_iter_new(sgl, in->as_str);
  sgl_val_reuse(in, sgl, sgl->PhraseIter, true)->as_iter = &i->iter;
  return true;
}

static bool sym_imp(struct sgl_fimp *fimp,
                    struct sgl *sgl,
                    struct sgl_op **rpc) {
  struct sgl_val *v = sgl_peek(sgl);
  struct sgl_str *s = v->as_str;
  sgl_val_reuse(v, sgl, sgl->Sym, false)->as_sym = sgl_sym(sgl, sgl_str_cs(s));
  sgl_str_deref(s, sgl);
  return true;
}

static bool words_imp(struct sgl_fimp *fimp,
                      struct sgl *sgl,
                      struct sgl_op **rpc) {
  struct sgl_val *in = sgl_peek(sgl);
  struct sgl_word_iter *i = sgl_word_iter_new(sgl, in->as_str);
  sgl_val_reuse(in, sgl, sgl->WordIter, true)->as_iter = &i->iter;
  return true;
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  if (!sgl_use(sgl, pos, sgl->abc_lib,
               sgl_sym(sgl, "Str"))) { return false; }

  sgl->PhraseIter = sgl_ref_type_new(sgl, pos, l,
                                     sgl_sym(sgl, "PhraseIter"),
                                     sgl_types(sgl->Iter),
                                     sizeof(struct sgl_phrase_iter),
                                     offsetof(struct sgl_phrase_iter, iter) +
                                     offsetof(struct sgl_iter, ls));
  
  sgl->WordIter = sgl_ref_type_new(sgl, pos, l,
                                   sgl_sym(sgl, "WordIter"),
                                   sgl_types(sgl->Iter),
                                   sizeof(struct sgl_word_iter),
                                   offsetof(struct sgl_word_iter, iter) +
                                   offsetof(struct sgl_iter, ls));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "lower"),
                    lower_imp,
                    sgl_rets(sgl->Str),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "phrases"),
                    phrases_imp,
                    sgl_rets(sgl->PhraseIter),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "sym"),
                    sym_imp,
                    sgl_rets(sgl->Sym),
                    sgl_arg(sgl->Str));

  sgl_lib_add_cfunc(l, sgl, pos,
                    sgl_sym(sgl, "words"),
                    words_imp,
                    sgl_rets(sgl->WordIter),
                    sgl_arg(sgl->Str));

  return sgl->errors.next == &sgl->errors;
}

struct sgl_lib *sgl_str_lib_new(struct sgl *sgl, struct sgl_pos *pos) {
  struct sgl_lib *l = sgl_lib_new(sgl, pos, sgl_sym(sgl, "str"));
  l->init = lib_init;
  return l;
}

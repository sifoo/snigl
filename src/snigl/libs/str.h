#ifndef SNIGL_LIBS_STR_H
#define SNIGL_LIBS_STR_H

struct sgl_lib;

struct sgl_lib *sgl_str_lib_new(struct sgl *sgl, struct sgl_pos *pos);

#endif

#ifndef SNIGL_LIBS_HASH_MAP_H
#define SNIGL_LIBS_HASH_MAP_H

struct sgl_lib;

struct sgl_lib *sgl_hash_map_lib_new(struct sgl *sgl, struct sgl_pos *pos);

#endif

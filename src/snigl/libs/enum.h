#ifndef SNIGL_LIBS_ENUM_H
#define SNIGL_LIBS_ENUM_H

struct sgl_lib;

struct sgl_lib *sgl_enum_lib_new(struct sgl *sgl, struct sgl_pos *pos);

#endif

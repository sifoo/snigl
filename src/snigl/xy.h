#ifndef SNIGL_XY_H
#define SNIGL_XY_H

#include "snigl/int.h"

struct sgl_xy {
  sgl_int_t x, y;
};

struct sgl_xy *sgl_xy_init(struct sgl_xy *xy, sgl_int_t x, sgl_int_t y);

#endif

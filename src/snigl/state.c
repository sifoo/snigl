#include <assert.h>

#include "snigl/call.h"
#include "snigl/sgl.h"
#include "snigl/state.h"
#include "snigl/sym.h"
#include "snigl/task.h"
#include "snigl/try.h"

struct sgl_state *sgl_state_init(struct sgl_state *state, struct sgl *sgl) {
  struct sgl_task *task = sgl->task;
  state->calls_len = task->calls.len;
  state->lib = task->lib;
  state->reg_stack_len = task->reg_stack.len;
  state->scope = task->scopes.prev;
  state->tries_len = task->tries.len;
  state->vars_len = sgl->vars.len;
  return state;
}

static void scope_deinit(struct sgl_ls *ls, struct sgl *sgl) {
  struct sgl_scope *s = sgl_baseof(struct sgl_scope, ls, ls);
  sgl_scope_end(s, sgl);
  sgl_scope_deref(s, sgl);  
}

static void restore_ls(struct sgl *sgl,
                         struct sgl_task *task,
                         struct sgl_ls *root,
                         struct sgl_ls *last,
                         void (*deinit)(struct sgl_ls *, struct sgl *)) {
  for (struct sgl_ls *i = root->prev; i != last;) {
    assert(i != root);
    struct sgl_ls *tmp = i;
    i = i->prev;
    sgl_ls_delete(tmp);
    deinit(tmp, sgl);
  }
}

void sgl_state_restore(struct sgl_state *state, struct sgl *sgl) {
  struct sgl_task *task = sgl->task;
  sgl->task->lib = state->lib;

  struct sgl_vec *rs = &task->reg_stack;
  assert(rs->len >= state->reg_stack_len);
  rs->len = state->reg_stack_len;

  restore_ls(sgl, task, &task->scopes, state->scope, scope_deinit);

  struct sgl_vec *ts = &task->tries;
  assert(ts->len >= state->tries_len);
  ts->len = state->tries_len;

  struct sgl_vec *vs = &sgl->vars;

  sgl_vec_rdo(vs, struct sgl_var, v) {
    if (vs->len-- <= state->vars_len) { break; }
    assert(v->tag == sgl->var_tag);
    if (v->id) { sgl_val_free(v->val, sgl); }
    sgl->var_tag--;
  }
}

void sgl_state_restore_calls(struct sgl_state *state, struct sgl *sgl) {
  struct sgl_task *task = sgl->task;
  struct sgl_vec *cs = &task->calls;
  assert(cs->len >= state->calls_len);
  cs->len = state->calls_len;
}

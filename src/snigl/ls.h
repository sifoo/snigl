#ifndef SNIGL_LS_H
#define SNIGL_LS_H

#include <stdbool.h>

#include "snigl/cmp.h"
#include "snigl/int.h"

#define _sgl_ls_do(mroot, mtype, mfield, mvar, mls, mnext)  \
  struct sgl_ls *mls = mroot->next, *mnext = mls->next;     \
                                                            \
  for (mtype *mvar = sgl_baseof(mtype, mfield, mls);        \
       mls != mroot;                                        \
       mls = mnext,                                         \
         mnext = mls->next,                                 \
         mvar = sgl_baseof(mtype, mfield, mls))             \

#define sgl_ls_do(mroot, mtype, mfield, mvar)   \
  _sgl_ls_do((mroot), mtype, mfield, mvar,      \
             SGL_CUID(mls), SGL_CUID(mnext))    \

#define _sgl_ls_rdo(mroot, mtype, mfield, mvar, mls, mprev) \
  struct sgl_ls *mls = mroot->prev, *mprev = mls->prev;     \
                                                            \
  for (mtype *mvar = sgl_baseof(mtype, mfield, mls);        \
       mls != mroot;                                        \
       mls = mprev,                                         \
         mprev = mls->prev,                                 \
         mvar = sgl_baseof(mtype, mfield, mls))             \

#define sgl_ls_rdo(mroot, mtype, mfield, mvar)  \
  _sgl_ls_rdo((mroot), mtype, mfield, mvar,     \
              SGL_CUID(mls), SGL_CUID(mprev))   \


#define sgl_ls_push sgl_ls_insert

struct sgl_ls {
  struct sgl_ls *prev, *next;
};

typedef enum sgl_cmp (*sgl_ls_cmp_t)(struct sgl_ls *lhs,
                                     const void *rhs,
                                     void *data);

struct sgl_ls *sgl_ls_init(struct sgl_ls *ls);
bool sgl_ls_null(struct sgl_ls *ls);
sgl_int_t sgl_ls_len(struct sgl_ls *ls);
void sgl_ls_assign(struct sgl_ls *ls, struct sgl_ls *src);

void sgl_ls_append(struct sgl_ls *ls, struct sgl_ls *next);
void sgl_ls_insert(struct sgl_ls *ls, struct sgl_ls *prev);
void sgl_ls_delete(struct sgl_ls *ls);
void sgl_ls_move(struct sgl_ls *dst, struct sgl_ls *src);
void sgl_ls_swap(struct sgl_ls *ls);

struct sgl_ls *sgl_ls_peek(struct sgl_ls *root);
struct sgl_ls *sgl_ls_pop(struct sgl_ls *root);

struct sgl_ls *sgl_ls_find(struct sgl_ls *root,
                           sgl_ls_cmp_t cmp,
                           const void *key,
                           void *data,
                           bool *ok);

#endif

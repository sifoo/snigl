#include <stdio.h>

#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/ref_type.h"

static void free_type(struct sgl_type *t, struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
  sgl_ref_type_deinit(rt);
  sgl_free(&sgl->ref_type_pool, rt);
}

struct sgl_ref_type *sgl_ref_type_init(struct sgl_ref_type *rt,
                                       struct sgl *sgl,
                                       struct sgl_pos *pos,
                                       struct sgl_lib *lib,
                                       struct sgl_sym *id,
                                       struct sgl_type *parents[],
                                       sgl_int_t val_size,
                                       sgl_int_t ls_offs) {
  sgl_type_init(&rt->type, sgl, pos, lib, id, parents);
  sgl_pool_init(&rt->pool, val_size, ls_offs);
  struct sgl_type *t = &rt->type;
  t->free = free_type;
  t->is_ref = true;
  return rt;
}

struct sgl_ref_type *sgl_ref_type_deinit(struct sgl_ref_type *t) {
  sgl_pool_deinit(&t->pool);
  return t;
}

struct sgl_type *sgl_ref_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[],
                                  sgl_int_t val_size,
                                  sgl_int_t ls_offs) {
  return &sgl_ref_type_init(sgl_malloc(&sgl->ref_type_pool),
                            sgl, pos, lib, id, parents, val_size, ls_offs)->type;    
}

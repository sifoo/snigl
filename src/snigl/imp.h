#ifndef SNIGL_IMP_H
#define SNIGL_IMP_H

struct sgl;
struct sgl_form;
struct sgl_ls;

enum sgl_imp_flags {SGL_IMP_RECALL = 1, SGL_IMP_SCOPE = 2};
  
struct sgl_imp {
  struct sgl_form *body;
  struct sgl_op *beg_pc, *end_pc, *rec_pc;
  enum sgl_imp_flags flags;
};

struct sgl_imp *sgl_imp_init(struct sgl_imp *imp,
                             struct sgl_form *body,
                             enum sgl_imp_flags flags);

struct sgl_imp *sgl_imp_deinit(struct sgl_imp *imp, struct sgl *sgl);

#endif

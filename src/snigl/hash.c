#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/pool.h"
#include "snigl/sgl.h"
#include "snigl/hash.h"
#include "snigl/util.h"

struct sgl_hash *sgl_hash_init(struct sgl_hash *h) {
  h->nslots = h->len = 0;
  h->slots = NULL;
  h->data = NULL;
  h->full = false;
  sgl_ls_init(&h->root);
  
  h->fn = NULL;
  h->is = NULL;
  return h;
}

struct sgl_hash *sgl_hash_deinit(struct sgl_hash *h, struct sgl *sgl) {
  if (!h->slots) { return h; }
  
  sgl_ls_do(&h->root, struct sgl_hash_node, ls, n) {
    sgl_free(&sgl->hash_node_pool, n);
  }
  
  free(h->slots);
  return h;
}

bool sgl_hash_init_slots(struct sgl_hash *h, struct sgl *sgl) {
  struct sgl_ls **ss = h->slots;
  if (ss && !h->full) { return false; }
  sgl_int_t ns = h->nslots;
  ns = h->nslots = ns ? ns * SGL_MIN_HASH_SLOTS : SGL_MIN_HASH_SLOTS;
  ss = h->slots = realloc(ss, ns * sizeof(struct sgl_ls *));
  memset(ss, 0, ns * sizeof(struct sgl_ls *));
  if (!h->len) { return true; }
  struct sgl_ls prev, *root = &h->root;
  sgl_ls_assign(&prev, root);
  sgl_ls_init(root);
  
  sgl_ls_do(&prev, struct sgl_hash_node, ls, n) {
    struct sgl_ls **s = ss + n->hkey % ns;
    sgl_ls_insert(*s ? *s : root, &n->ls);
    *s = &n->ls;
  }
  
  h->full = false;
  return false;
}

sgl_uint_t sgl_hash_key(struct sgl_hash *h, void *key) {
  return h->fn ? h->fn(key, h->data) : (sgl_uint_t)key;
}

struct sgl_ls **sgl_hash_slot(struct sgl_hash *h, sgl_uint_t hkey) {
  struct sgl_ls **ss = h->slots;
  return ss ? ss + hkey % h->nslots : NULL;
}

struct sgl_hash_node *sgl_hash_find(struct sgl_hash *h,
                                    struct sgl_ls **slot,
                                    sgl_uint_t hkey,
                                    void *key) {
  assert(slot);
  struct sgl_ls *s = *slot;
  if (!s) { return NULL; }
  struct sgl_hash_node *node = NULL;
  sgl_int_t niter = 1, ns = h->nslots, hi = hkey % ns;

  for (struct sgl_ls *i = s; i != &h->root; i = i->next) {
    struct sgl_hash_node *n = sgl_baseof(struct sgl_hash_node, ls, i);
    if (n->hkey % ns != hi) { break; }
    if (h->is ? (n->hkey == hkey && h->is(n->key, key, h->data)) : key == n->key) {
      node = n;
      break;
    }

    niter++;
  }

  if (niter > SGL_MAX_HASH_ITER) { h->full = true; }
  return node;
}

struct sgl_hash_node *sgl_hash_add(struct sgl_hash *h,
                                   struct sgl *sgl,
                                   struct sgl_ls **slot,
                                   sgl_uint_t hkey,
                                   void *key,
                                   void *val) {
  assert(slot);
  struct sgl_hash_node *n = sgl_malloc(&sgl->hash_node_pool);
  sgl_ls_insert(*slot ? *slot : &h->root, &n->ls);
  *slot = &n->ls;
  n->hkey = hkey;
  n->key = key;
  n->val = val;
  h->len++;
  return n;
}

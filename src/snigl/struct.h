#ifndef SNIGL_STRUCT_H
#define SNIGL_STRUCT_H

#include "snigl/int.h"
#include "snigl/ls.h"

struct sgl;
struct sgl_struct_type;

struct sgl_struct {
  struct sgl_ls ls;
  sgl_int_t nrefs;
  struct sgl_val *fields[];
};

struct sgl_struct *sgl_struct_new(struct sgl_struct_type *type, struct sgl *sgl);


struct sgl_struct *sgl_struct_init(struct sgl_struct *s,
                                   struct sgl *sgl,
                                   struct sgl_struct_type *type);

struct sgl_struct *sgl_struct_deinit(struct sgl_struct *s,
                                     struct sgl *sgl,
                                     struct sgl_struct_type *type);

void sgl_struct_deref(struct sgl_struct *s,
                      struct sgl *sgl,
                      struct sgl_struct_type *type);

#endif

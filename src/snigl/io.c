#include "snigl/config.h"

#ifdef SGL_USE_ASYNC

#include <stdio.h>
#include <stdlib.h>

#include "snigl/io.h"
#include "snigl/io_thread.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"

struct sgl_io *_sgl_io_new(struct sgl *sgl, sgl_int_t depth, void *args[]) {
  struct sgl_io *io = sgl_malloc(&sgl->io_pool);

  io->pos = *sgl_pos(sgl);
  io->task = NULL;
  io->depth = depth;
  io->error = NULL;
  io->run = NULL;
  io->free = NULL;
  
  for (void **a = io->args; *args; args++, a++) { *a = *args; }
  return io;
}

void sgl_io_free(struct sgl_io *io, struct sgl *sgl) { sgl_free(&sgl->io_pool, io); }

struct sgl_op *sgl_io_yield(struct sgl_io *io,
                            struct sgl *sgl,
                            struct sgl_io_thread *thread) {
  struct sgl_task *t = io->task = sgl->task;
  sgl_ls_delete(&t->ls);
  sgl_io_thread_push(thread, io);
  return sgl_yield(sgl);
}

#endif

#include <assert.h>

#include "snigl/list_set.h"
#include "snigl/pool.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/val.h"
#include "snigl/util.h"

static enum sgl_cmp val_cmp(struct sgl_ls *lhs, const void *rhs, void *data) {
  struct sgl_list_set *s = data;
  struct sgl *sgl = s->sgl;
  struct sgl_val *k = sgl_baseof(struct sgl_val, ls, lhs);

  if (s->key) {
    k = sgl_list_set_key(s, k);
    if (!k) { return SGL_CMP_FAIL; }
  }
    
  enum sgl_cmp cmp = sgl_val_cmp(k, sgl, rhs);
  if (s->key) { sgl_val_free(k, sgl); }
  return cmp;
}

struct sgl_list_set *sgl_list_set_new(struct sgl *sgl, struct sgl_val *key) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->ListSet);
  struct sgl_list_set *s = sgl_malloc(&rt->pool);
  sgl_list_init(&s->list);
  s->sgl = sgl;
  s->key = key;
  s->cmp = val_cmp;
  return s;
}

void sgl_list_set_deref(struct sgl_list_set *s) {
  assert(s->list.nrefs > 0);

  if (!--s->list.nrefs) {
    struct sgl *sgl = s->sgl;
    sgl_list_deinit(&s->list, sgl);
    if (s->key) { sgl_val_free(s->key, sgl); }
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->ListSet);
    sgl_free(&rt->pool, s);
  }
}

struct sgl_val *sgl_list_set_key(struct sgl_list_set *s, struct sgl_val *val) {
  struct sgl *sgl = s->sgl;
  sgl_val_dup(val, sgl, sgl_stack(sgl));
  if (!sgl_val_call(s->key, sgl, sgl_pos(sgl), sgl->pc, true)) { return NULL; }
  val = sgl_pop(sgl);
  if (!val) { return NULL; }
  return val;
}

struct sgl_ls *sgl_list_set_find(struct sgl_list_set *s,
                                 struct sgl_val *key,
                                 bool *ok) {
  return sgl_ls_find(&s->list.root, s->cmp, key, s, ok);
}

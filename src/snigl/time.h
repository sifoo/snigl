#ifndef SNIGL_TIME_H
#define SNIGL_TIME_H

#include <time.h>

#include "snigl/cmp.h"

enum sgl_cmp sgl_time_cmp(time_t lhs, time_t rhs);
time_t sgl_now();

#endif

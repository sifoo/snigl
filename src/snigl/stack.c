#include "snigl/buf.h"
#include "snigl/ls.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"

sgl_int_t sgl_stack_drop(struct sgl_ls *stack,
                         struct sgl *sgl,
                         sgl_int_t nvals,
                         bool silent) {
  sgl_int_t i = 0;
  
  while (i < nvals) {
    struct sgl_ls *v = sgl_ls_pop(stack);
    
    if (!v) {
      if (!silent) { sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to drop")); }
      break;
    }
    
    sgl_val_free(sgl_baseof(struct sgl_val, ls, v), sgl);
    i++;
  }
  
  return i;
}

struct sgl_val *sgl_stack_dup(struct sgl_ls *stack, struct sgl *sgl, bool silent) {
  struct sgl_ls *v = sgl_ls_peek(stack);
  
  if (!v) {
    if (!silent) { sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to dup")); }
    return NULL;
  }
  
  return sgl_val_dup(sgl_baseof(struct sgl_val, ls, v), sgl, stack);
}

struct sgl_val *sgl_stack_dupl(struct sgl_ls *stack, struct sgl *sgl, bool silent) {
  struct sgl_ls *v = stack->prev, *sp = v->prev;
  
  if (v == stack || sp == stack) {
    if (!silent) { sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to dupl")); }
    return NULL;
  }
  
  return sgl_val_dup(sgl_baseof(struct sgl_val, ls, v), sgl, sp);
}

bool sgl_stack_rotl(struct sgl_ls *stack, struct sgl *sgl, bool silent) {
  struct sgl_ls *sp = stack->prev->prev->prev;

  if (sp == stack || sp->next == stack || sp->next->next == stack) {
    if (!silent) { sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to rotl")); }
    return false;
  }

  sgl_ls_move(sp, stack->prev);
  return true;
}

bool sgl_stack_rotr(struct sgl_ls *stack, struct sgl *sgl, bool silent) {
  struct sgl_ls *sp = stack->prev->prev->prev;

  if (sp == stack || sp->next == stack || sp->next->next == stack) {
    if (!silent) { sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to rotr")); }
    return false;
  }

  sgl_ls_move(stack, sp);            
  return true;
}

bool sgl_stack_swap(struct sgl_ls *stack, struct sgl *sgl, bool silent) {
  struct sgl_ls *x = stack->prev;
  
  if (x == stack || x->prev == stack) {
    if (!silent) { sgl_error(sgl, sgl_pos(sgl), sgl_strdup("Nothing to swap")); }
    return false;
  }

  sgl_ls_swap(x);
  return true;
}

void sgl_stack_dump(struct sgl_ls *root, FILE *out) {
  fputc('[', out);
  char sep = 0;
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  
  sgl_ls_do(root, struct sgl_val, ls, v) {
    if (sep) { sgl_buf_putc(&buf, sep); }
    sgl_val_dump(v, &buf);
    sep = ' ';
  }

  fputs(sgl_buf_cs(&buf), out);
  fputc(']', out);
  sgl_buf_deinit(&buf);
}

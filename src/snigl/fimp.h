#ifndef SNIGL_FIMP_H
#define SNIGL_FIMP_H

#include "snigl/arg.h"
#include "snigl/func.h"
#include "snigl/config.h"
#include "snigl/imp.h"
#include "snigl/ls.h"

struct sgl_cemit;
struct sgl_fimp;
struct sgl_op;

typedef bool (*sgl_cimp_t)(struct sgl_fimp *, struct sgl *, struct sgl_op **);

struct sgl_fimp {
  struct sgl_ls ls;
  struct sgl_func *func;
  struct sgl_sym *id;
  sgl_int_t cemit_id, weight, nrets;
  
  struct sgl_arg args[SGL_MAX_ARGS];
  struct sgl_type *rets[SGL_MAX_RETS];

  struct sgl_imp imp;
  sgl_cimp_t cimp;
  sgl_int_t trace_depth;
  void *data;
};

sgl_int_t sgl_fimp_weight(struct sgl_arg args[], sgl_int_t nargs);
struct sgl_sym *sgl_fimp_id(struct sgl *sgl, struct sgl_arg args[], sgl_int_t nargs);

struct sgl_fimp *sgl_fimp_init(struct sgl_fimp *fimp,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_func *func,
                               struct sgl_arg args[],
                               struct sgl_type *rets[]);

struct sgl_fimp *sgl_fimp_deinit(struct sgl_fimp *fimp, struct sgl *sgl);

struct sgl_fimp *sgl_fimp_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_func *func,
                              struct sgl_arg args[],
                              struct sgl_type *rets[]);

sgl_int_t sgl_fimp_cemit_id(struct sgl_fimp *f,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_cemit *out); 

bool sgl_fimp_match(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_ls *stack);

bool sgl_fimp_match_types(struct sgl_fimp *fimp,
                          struct sgl *sgl,
                          struct sgl_ls *stack);

bool sgl_fimp_compile(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_pos *pos);

struct sgl_op *sgl_fimp_call(struct sgl_fimp *fimp,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_op *return_pc);

#endif

#include <assert.h>
#include <stddef.h>

#include "snigl/arg.h"
#include "snigl/buf.h"
#include "snigl/cemit.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/val.h"

struct sgl_arg *sgl_arg_init(struct sgl_arg *a,
                             struct sgl_type *type,
                             struct sgl_val *val) {
  a->type = type;
  a->val = val;
  return a;
}

struct sgl_arg *sgl_arg_deinit(struct sgl_arg *a, struct sgl *sgl) {
  if (a->val) { sgl_val_free(a->val, sgl); }
  return a;
}

sgl_int_t sgl_arg_weight(struct sgl_arg *a) {
  return a->val ? SGL_MAX_TYPES : a->type->tag;
}

void sgl_arg_dump(struct sgl_arg *a, struct sgl_buf *out) {
  if (a->val) {
    sgl_val_dump(a->val, out);
  } else {
    assert(a->type);
    sgl_buf_putcs(out, a->type->def.id->id);
  }
}

bool sgl_arg_cemit(struct sgl_arg *a,
                   struct sgl *sgl,
                   struct sgl_pos *pos,
                   sgl_int_t id, sgl_int_t i,
                   struct sgl_cemit *out) {
  if (a->val) {
    if (!sgl_val_cemit(a->val, sgl, pos, "v", NULL, out)) { return false; }
    
    sgl_cemit_line(out,
                   "sgl_arg_init(args%" SGL_INT " + %" SGL_INT ", NULL, v);",
                   id, i);
  } else {
    sgl_int_t type_id = sgl_type_cemit_id(a->type, sgl, pos, out);
    if (type_id == -1) { return false; }
    
    sgl_cemit_line(out,
                   "sgl_arg_init(args%" SGL_INT " + %" SGL_INT ", "
                   "type%" SGL_INT ", "
                   "NULL);",
                   id, i, type_id);
  }

  return true;
}


struct sgl_arg sgl_arg(struct sgl_type *type) {
  struct sgl_arg a;
  sgl_arg_init(&a, type, NULL);
  return a;
}

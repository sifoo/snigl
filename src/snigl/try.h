#ifndef SNIGL_TRY_H
#define SNIGL_TRY_H

#include "snigl/state.h"

struct sgl;

struct sgl_try {
  struct sgl_state state;
  struct sgl_op *end_pc;
};

struct sgl_try *sgl_try_init(struct sgl_try *t,
                             struct sgl *sgl,
                             struct sgl_op *end_pc);

#endif

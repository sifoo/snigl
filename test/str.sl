use: str _

\n type Char test=
#a type Char test=

"foo" len 3 test=
"foo" dup pop-drop len 2 test=
"foobar" dup pop-drop dup last #a test=
["foo" 42 'bar] #, join "foo,42,bar" test=
"foo" list "bar" push str "foobar" test=
"42abc" iter dup _next dup int 2 test= next #a test= 
"abc" iter dup _next str "bc" test=

"abcabcabc" &(int ++ char) map str "bcdbcdbcd" test=
"abcabcabc" &(#b !=) filter str "acacac" test=
"abcabcabc" &(dup #b = if: (drop nil)) map str "acacac" test=

["abc"..] [#a #b #c] test=
['foo 42 "bar" [..]] [['foo 42 "bar"]] test=

{'bar 42 let: 'foo "foo %@foo %_ baz"} "foo 42 bar baz" test=

"foo bar .! baz _ -" words list ["foo" "bar" "baz"] test=
"FoobAr!" lower "foobar!" test=

"hello, how are you? i am fine, thank you." phrases list
["hello", "how are you" "i am fine" "thank you"]
test=

1 2 abc/+ 3 test=

lib: foo (
  use: abc 'func:
  func: bar _ _ 42
)

foo/bar 42 test=

lib: my-math (
  use: abc _
  
  func: fib Int Int (
    0 1 rotr -- times: (dup rotl +)
    swap drop
  )
)

20 my-math/fib 6765 test=

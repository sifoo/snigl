use: enum _

42 Int|Sym type-sub? test
'a Int|Sym type-sub? test
nil Int|Sym type-sub? not test
nil Int|Nil type-sub? test
Int|Fix Num sub? test

type: IntList _ List
func: sum IntList Int (0 swap for: +)
[1 2 3] int-list sum 6 test=

trait: TFoo _
trait: TBar _
trait: TBaz (TFoo TBar)

TBaz TFoo sub? test
TBaz TBar sub? test

enum: EFoo TFoo ('e-foo 'e-bar 'e-baz)
e-foo TFoo type-sub? test
e-foo e-bar < test
e-foo sym 'e-foo test=
e-baz int 2 test=
EFoo list [e-foo e-bar e-baz] test=

func: call(e-foo) _ 'ok
e-foo call 'ok test=